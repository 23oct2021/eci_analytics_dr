USE [ECIApp]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 6/18/2018 1:48:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[companyName] [varchar](512) NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL,
	[AccOwner] [varchar](250) NULL,
	[Industry] [varchar](250) NULL,
	[Employees] [int] NULL,
	[Annual_Revenue] [decimal](10, 2) NULL,
	[Telephone] [varchar](250) NULL,
	[Mailing_Street] [varchar](250) NULL,
	[Mailing_City] [varchar](250) NULL,
	[Mailing_State] [varchar](250) NULL,
	[Mailing_Postal_Code] [int] NULL,
	[Mailing_Country] [varchar](250) NULL,
	[Website] [varchar](250) NULL,
	[Description] [varchar](530) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 6/18/2018 1:48:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[categoryid] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](512) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryResponseMaster]    Script Date: 6/18/2018 1:48:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryResponseMaster](
	[CatResId] [int] IDENTITY(1,1) NOT NULL,
	[categoryid] [int] NULL,
	[ResponseId] [int] NULL,
	[ResponseValue] [varchar](150) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DataLabelMaster]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataLabelMaster](
	[DataLabelID] [int] IDENTITY(1,1) NOT NULL,
	[DataLabelName] [varchar](512) NULL,
	[categoryid] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ECIData]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ECIData](
	[companyname] [varchar](250) NULL,
	[Category] [varchar](512) NULL,
	[DataLabel] [varchar](150) NULL,
	[SurveyYear] [int] NULL,
	[ResponseID] [int] NULL,
	[ResponseCount] [int] NULL,
	[ReponsePercent] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuOption]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuOption](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[menuname] [varchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsHasChild] [bit] NOT NULL,
	[ParentId] [int] NULL,
	[URLPath] [varchar](512) NULL,
	[menutype] [char](1) NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL,
	[Orderid] [int] NULL,
	[class] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProfileDtls]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProfileDtls](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [varchar](150) NOT NULL,
	[lastname] [varchar](150) NULL,
	[email] [varchar](150) NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL,
	[Middle_Name] [varchar](250) NULL,
	[Tittle] [varchar](250) NULL,
	[Division] [varchar](250) NULL,
	[Brithday] [datetime] NULL,
	[Mobile] [varchar](20) NULL,
	[Contact_Owner] [varchar](250) NULL,
	[Telephone] [varchar](250) NULL,
	[Mailing_Street] [varchar](250) NULL,
	[Mailing_City] [varchar](250) NULL,
	[Mailing_State] [varchar](250) NULL,
	[Mailing_Postal_Code] [int] NULL,
	[Mailing_Country] [varchar](250) NULL,
	[Website] [varchar](250) NULL,
	[Description] [varchar](530) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleMaster]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rolename] [varchar](150) NOT NULL,
	[isactive] [bit] NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rolemenurel]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rolemenurel](
	[roleid] [int] NOT NULL,
	[menuid] [int] NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserHistory]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserHistory](
	[usernum] [int] IDENTITY(1,1) NOT NULL,
	[logintime] [datetime] NULL,
	[logouttime] [datetime] NULL,
	[isconnected] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserOTP]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserOTP](
	[usernum] [int] NOT NULL,
	[opt] [varchar](10) NULL,
	[starttime] [datetime] NULL,
	[endtime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[usernum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserId] [int] NOT NULL,
	[roleid] [int] NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Accountid] [int] NULL,
	[profileid] [int] NULL,
	[FirstName] [varchar](150) NOT NULL,
	[LastName] [varchar](150) NOT NULL,
	[EmailId] [varchar](150) NOT NULL,
	[Pwd] [varchar](max) NOT NULL,
	[UserType] [int] NULL,
	[UserStatus] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[IpAddress] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 
GO
INSERT [dbo].[Account] ([id], [companyName], [createdon], [createdby], [modifiedon], [modifiedby], [AccOwner], [Industry], [Employees], [Annual_Revenue], [Telephone], [Mailing_Street], [Mailing_City], [Mailing_State], [Mailing_Postal_Code], [Mailing_Country], [Website], [Description]) VALUES (1, N'Company1', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Account] ([id], [companyName], [createdon], [createdby], [modifiedon], [modifiedby], [AccOwner], [Industry], [Employees], [Annual_Revenue], [Telephone], [Mailing_Street], [Mailing_City], [Mailing_State], [Mailing_Postal_Code], [Mailing_Country], [Website], [Description]) VALUES (2, N'Company3', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Account] ([id], [companyName], [createdon], [createdby], [modifiedon], [modifiedby], [AccOwner], [Industry], [Employees], [Annual_Revenue], [Telephone], [Mailing_Street], [Mailing_City], [Mailing_State], [Mailing_Postal_Code], [Mailing_Country], [Website], [Description]) VALUES (3, N'Company4', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Account] ([id], [companyName], [createdon], [createdby], [modifiedon], [modifiedby], [AccOwner], [Industry], [Employees], [Annual_Revenue], [Telephone], [Mailing_Street], [Mailing_City], [Mailing_State], [Mailing_Postal_Code], [Mailing_Country], [Website], [Description]) VALUES (4, N'Company2', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Account] ([id], [companyName], [createdon], [createdby], [modifiedon], [modifiedby], [AccOwner], [Industry], [Employees], [Annual_Revenue], [Telephone], [Mailing_Street], [Mailing_City], [Mailing_State], [Mailing_Postal_Code], [Mailing_Country], [Website], [Description]) VALUES (5, N'Embraer', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Account] ([id], [companyName], [createdon], [createdby], [modifiedon], [modifiedby], [AccOwner], [Industry], [Employees], [Annual_Revenue], [Telephone], [Mailing_Street], [Mailing_City], [Mailing_State], [Mailing_Postal_Code], [Mailing_Country], [Website], [Description]) VALUES (6, N'Schenker', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Account] ([id], [companyName], [createdon], [createdby], [modifiedon], [modifiedby], [AccOwner], [Industry], [Employees], [Annual_Revenue], [Telephone], [Mailing_Street], [Mailing_City], [Mailing_State], [Mailing_Postal_Code], [Mailing_Country], [Website], [Description]) VALUES (7, N'Md Infotech Solutions', CAST(N'2018-06-14T08:28:14.890' AS DateTime), 3, NULL, NULL, N'Deepa Marimuthu', N'Software Company', 2, CAST(200000.00 AS Decimal(10, 2)), N'04244540609', N'KottaiMuniyappan Street', N'Erode', N'Tamilnadu', 638001, N'India', N'www.mdinfotechs.com', N'test')
GO
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[CategoryMaster] ON 
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (1, N'Awareness of Resources')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (2, N'Additional Questions')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (3, N'Company considers effect on questions')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (4, N'Dissatisfication - asked if they indicated dissatisfaction with company response')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (5, N'ECI Demographic Information')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (6, N'Effectivness of Program')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (7, N'Ethics Related Actions - Culture Questions')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (8, N'Ethics Related Outcomes - Key measures used to assess ethical culture')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (9, N'General misconduct ')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (10, N'Misc. Questions')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (11, N'Participant Demographic Variables')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (12, N'Pressure')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (13, N'Reason for not reporting - only asked if they did not report at least once instance of misconduct')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (14, N'Reason you reported: Asked if they did report at least one type of misconduct')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (15, N'Reporting question - only asked if they observed general and/or specific type of misconduct')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (16, N'Retaliation - asked if they did report misconduct')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (17, N'Satisfaction with company response to misconduct')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (18, N'Specific misconduct questions')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (19, N'Specific Types of Pressure')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (20, N'Type of retaliation - asked if they did experience retaliation for report of misconduct')
GO
SET IDENTITY_INSERT [dbo].[CategoryMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[CategoryResponseMaster] ON 
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (1, 1, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (2, 1, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (3, 1, 88, N'Don`t Know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (4, 2, 1, N'Head of location')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (5, 2, 2, N'Functional VP')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (6, 2, 3, N'Head of BU')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (7, 2, 4, N'Single leader')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (8, 2, 5, N'Board of Directors')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (9, 2, 88, N'Don`t Know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (10, 3, 1, N'Strongly disagree')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (11, 3, 2, N'Disagree')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (12, 3, 3, N'Neither agree nor disagree')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (13, 3, 4, N'Agree')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (14, 3, 5, N'Strongly agree')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (15, 3, 88, N'Don`t Know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (16, 4, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (17, 4, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (18, 4, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (19, 6, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (20, 6, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (21, 6, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (22, 7, 1, N'Strongly disagree')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (23, 7, 2, N'Disagree')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (24, 7, 3, N'Neither agree nor disagree')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (25, 7, 4, N'Agree')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (26, 7, 5, N'Strongly agree')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (27, 7, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (28, 8, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (29, 8, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (30, 8, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (31, 9, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (32, 9, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (33, 9, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (34, 11, 1, N'Less than a year')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (35, 11, 2, N'1 to 2 yrs')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (36, 11, 3, N'3 to 5 yrs')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (37, 11, 4, N'6 to 10 yrs')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (38, 11, 5, N'11+ yrs')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (39, 11, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (40, 13, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (41, 13, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (42, 13, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (43, 14, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (44, 14, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (45, 14, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (46, 15, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (47, 15, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (48, 15, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (49, 16, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (50, 16, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (51, 16, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (52, 17, 1, N'Very dissatisfied')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (53, 17, 2, N'Dissatisfied')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (54, 17, 3, N'Neither satisfied nor dissatisfied')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (55, 17, 4, N'Satisfied')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (56, 17, 5, N'Very Satisfied')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (57, 17, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (58, 18, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (59, 18, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (60, 18, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (61, 19, 1, N'Very rarely')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (62, 19, 2, N'Periodically')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (63, 19, 3, N'Fairly often')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (64, 19, 4, N'All the time')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (65, 19, 88, N'Don`t know')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (66, 20, 0, N'No')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (67, 20, 1, N'Yes')
GO
INSERT [dbo].[CategoryResponseMaster] ([CatResId], [categoryid], [ResponseId], [ResponseValue]) VALUES (68, 20, 88, N'Don`t know')
GO
SET IDENTITY_INSERT [dbo].[CategoryResponseMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[DataLabelMaster] ON 
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (1, N'Standards', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (2, N'Training', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (3, N'Advice', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (4, N'Hotline', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (5, N'Appraisal', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (6, N'Discipline', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (7, N'DealsFairly', 2)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (8, N'EthicsLaw', 2)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (9, N'EthicsValues', 2)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (10, N'IrregularSituations', 2)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (11, N'PeerShowRespResults', 2)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (12, N'Satisfied', 2)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (13, N'SupRewardResults', 2)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (14, N'TopMgmt_ID', 2)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (15, N'ValuesConflict', 2)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (16, N'EmpEffects', 3)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (17, N'EnvEffects', 3)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (18, N'FutureGenEffects', 3)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (19, N'SocietyEffects', 3)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (20, N'DissatActionUnknown', 4)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (21, N'DissatDidntBelieveMgmt', 4)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (22, N'DissatDisagreeWithInvestigation', 4)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (23, N'DissatExpRetaliation', 4)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (24, N'DissatInsufficientPunishment', 4)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (25, N'DissatNoPursuit', 4)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (26, N'Country_NUM_Uniform', 5)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (27, N'Region_NUM_Uniform', 5)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (28, N'US_Intl', 5)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (29, N'Prepared', 6)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (30, N'RewardEthics', 6)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (31, N'RewardResults', 6)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (32, N'SupFeedback', 6)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (33, N'SupHoldsAccount', 6)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (34, N'TrainingApplicable', 6)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (35, N'UseStandards', 6)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (36, N'NonMgmtAccount', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (37, N'PeerConsider', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (38, N'PeerExample', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (39, N'PeerKeepProm', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (40, N'PeerSupport', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (41, N'PeerTalk', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (42, N'SupAccount', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (43, N'SupExample', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (44, N'SupInfo', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (45, N'SupKeepProm', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (46, N'SupSupport', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (47, N'SupTalks', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (48, N'TopMgmtAccount', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (49, N'TopMgmtExample', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (50, N'TopMgmtInfo', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (51, N'TopMgmtKeepProm', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (52, N'TopMgmtNoRetaliate', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (53, N'TopMgmtSupport', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (54, N'TopMgmtTalks', 7)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (55, N'Pressure', 8)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (56, N'MiscGen', 9)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (57, N'TrainingApplicable', 10)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (58, N'Age', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (59, N'Education', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (60, N'Ethnicity', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (61, N'Gender', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (62, N'LocationPerformWork', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (63, N'Mgmt', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (64, N'ReceiveReports', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (65, N'Salary', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (66, N'Supervisor', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (67, N'Tenure', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (68, N'Union', 11)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (69, N'Pressure', 12)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (70, N'NoRepAcceptedBhvr', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (71, N'NoRepAnotherDid', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (72, N'NoRepDidntBelieveMisc', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (73, N'NoRepFearMgmtRetal', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (74, N'NoRepFearPeerRetal', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (75, N'NoRepFearSupRetal', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (76, N'NoRepIResolve', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (77, N'NoRepNoAction', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (78, N'NoRepNoConfid', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (79, N'NoRepNotKnowWhom', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (80, N'NoRepPastRetal', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (81, N'NoRepResolve', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (82, N'NoRepSomeoneElse', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (83, N'NoRepToPerInvolved', 13)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (84, N'RepReasonAction', 14)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (85, N'RepReasonConfid', 14)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (86, N'RepReasonMgmtSupport', 14)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (87, N'RepReasonMyRespons', 14)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (88, N'RepReasonPeerSupport', 14)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (89, N'RepReasonSupSupport', 14)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (90, N'RepGen', 15)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (91, N'ExpRetaliation', 16)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (92, N'SatWithResponse', 17)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (93, N'ObsAbuse', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (94, N'ObsAlterDocs', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (95, N'ObsAntiCompPract', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (96, N'ObsCompetInfo', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (97, N'ObsConfInfo', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (98, N'ObsConflictInterest', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (99, N'ObsContractViols', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (100, N'ObsCoResourceAbuse', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (101, N'ObsCustPrivacyBreach', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (102, N'ObsDiscrim', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (103, N'ObsEmplBenViols', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (104, N'ObsEmplPrivacyBreach', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (105, N'ObsEnvViol', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (106, N'ObsFailProfStandards', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (107, N'ObsHealthViol', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (108, N'ObsImproperContracts', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (109, N'ObsImproperHire', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (110, N'ObsInternetAbuse', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (111, N'ObsLieEmployees', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (112, N'ObsLieExternal', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (113, N'ObsMisrepFinRecords', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (114, N'ObsOfferKickbacks', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (115, N'ObsOther', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (116, N'ObsSexHarass', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (117, N'ObsStealing', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (118, N'ObsSubstanceAbuse', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (119, N'ObsTimeCheat', 18)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (120, N'PressSourceDirectSupPressure', 19)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (121, N'PressSourceExtDemands', 19)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (122, N'PressSourceKeepJob', 19)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (123, N'PressSourceMyCareer', 19)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (124, N'PressSourcePerfGoals', 19)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (125, N'PressSourceSaveJobs', 19)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (126, N'PressureFreq', 19)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (127, N'RetalColdShoulder', 20)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (128, N'RetalDemoted', 20)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (129, N'RetalExcluded', 20)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (130, N'RetalJobShifted', 20)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (131, N'RetalJobThreatened', 20)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (132, N'RetalMgmtVerbalAbuse', 20)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (133, N'RetalNotAdvanced', 20)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (134, N'RetalOther', 20)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (135, N'RetalPeersVerbalAbuse', 20)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (136, N'RetalPhysHarm', 20)
GO
SET IDENTITY_INSERT [dbo].[DataLabelMaster] OFF
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Standards', 2015, 0, 6, 0.00452488687782805)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Standards', 2015, 1, 1593, 0.971341463414634)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Standards', 2015, 88, 41, 0.0254500310366232)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Training', 2015, 0, 38, 0.0249507550886408)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Training', 2015, 1, 1507, 0.91890243902439)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Training', 2015, 88, 95, 0.0647580095432856)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Advice', 2015, 0, 10, 0.00765110941086458)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Advice', 2015, 1, 1575, 0.960365853658537)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Advice', 2015, 88, 55, 0.0338253382533825)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Hotline', 2015, 0, 11, 0.0073875083948959)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Hotline', 2015, 1, 1586, 0.967073170731707)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Hotline', 2015, 88, 43, 0.0266584004959702)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Appraisal', 2015, 0, 225, 0.138546798029557)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Appraisal', 2015, 1, 934, 0.569512195121951)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Appraisal', 2015, 88, 481, 0.294189602446483)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Discipline', 2015, 0, 52, 0.0319214241866176)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Discipline', 2015, 1, 1286, 0.784146341463415)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Discipline', 2015, 88, 302, 0.184596577017115)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Standards', 2015, 0, 7, 0.0080091533180778)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Standards', 2015, 1, 883, 0.963973799126638)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Standards', 2015, 88, 26, 0.0288568257491676)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Training', 2015, 0, 15, 0.017162471395881)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Training', 2015, 1, 867, 0.946506550218341)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Training', 2015, 88, 34, 0.0386363636363636)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Advice', 2015, 0, 8, 0.0091533180778032)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Advice', 2015, 1, 855, 0.933406113537118)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Advice', 2015, 88, 53, 0.0588235294117647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Hotline', 2015, 0, 9, 0.0102974828375286)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Hotline', 2015, 1, 856, 0.934497816593887)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Hotline', 2015, 88, 51, 0.0562293274531422)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Appraisal', 2015, 0, 51, 0.0572390572390572)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Appraisal', 2015, 1, 714, 0.780327868852459)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Appraisal', 2015, 88, 151, 0.164847161572052)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Discipline', 2015, 0, 12, 0.0137299771167048)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Discipline', 2015, 1, 787, 0.859170305676856)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Discipline', 2015, 88, 117, 0.128289473684211)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2010, 0, 39, 0.0065989847715736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2010, 1, 5884, 0.980666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2010, 88, 77, 0.0130420054200542)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2010, 0, 104, 0.017514314584035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2010, 1, 5725, 0.954166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2010, 88, 171, 0.0285809794417516)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2010, 0, 78, 0.0131357359380263)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2010, 1, 5646, 0.941)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2010, 88, 276, 0.0461847389558233)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2010, 0, 81, 0.0136409565510273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2010, 1, 5756, 0.959333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2010, 88, 163, 0.027339818852734)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2010, 0, 338, 0.0564933979608892)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2010, 1, 5104, 0.850666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2010, 88, 558, 0.0933110367892977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2010, 0, 165, 0.027661357921207)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2010, 1, 4780, 0.796666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2010, 88, 1055, 0.175950633755837)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2013, 0, 28, 0.00496982605608804)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2013, 1, 5878, 0.979666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2013, 88, 94, 0.0157216925907342)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2013, 0, 84, 0.0143983544737744)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2013, 1, 5750, 0.958333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2013, 88, 166, 0.0280168776371308)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2013, 0, 70, 0.0117076434186319)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2013, 1, 5721, 0.9535)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2013, 88, 209, 0.0352742616033755)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2013, 0, 58, 0.0109454614078128)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2013, 1, 5781, 0.9635)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2013, 88, 161, 0.0269275798628533)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2013, 0, 182, 0.0304398728884429)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2013, 1, 5364, 0.894)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2013, 88, 454, 0.0760469011725293)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2013, 0, 156, 0.0260956841753095)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2013, 1, 5055, 0.8425)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2013, 88, 789, 0.131609674728941)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Participant Demographic Variables', N'Tenure', 2015, 4, 1424, 0.9436713)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Standards', 2015, 0, 6, 0.00452488687782805)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Standards', 2015, 1, 1593, 0.971341463414634)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Standards', 2015, 88, 41, 0.0254500310366232)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Training', 2015, 0, 38, 0.0249507550886408)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Training', 2015, 1, 1507, 0.91890243902439)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Training', 2015, 88, 95, 0.0647580095432856)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Advice', 2015, 0, 10, 0.00765110941086458)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Advice', 2015, 1, 1575, 0.960365853658537)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Advice', 2015, 88, 55, 0.0338253382533825)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Hotline', 2015, 0, 11, 0.0073875083948959)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Hotline', 2015, 1, 1586, 0.967073170731707)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Hotline', 2015, 88, 43, 0.0266584004959702)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Appraisal', 2015, 0, 225, 0.138546798029557)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Appraisal', 2015, 1, 934, 0.569512195121951)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Appraisal', 2015, 88, 481, 0.294189602446483)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Discipline', 2015, 0, 52, 0.0319214241866176)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Discipline', 2015, 1, 1286, 0.784146341463415)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Resources', N'Discipline', 2015, 88, 302, 0.184596577017115)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Misc. Questions', N'TrainingApplicable', 2015, 0, 66, 0.0409683426443203)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Misc. Questions', N'TrainingApplicable', 2015, 1, 1054, 0.642682926829268)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Misc. Questions', N'TrainingApplicable', 2015, 70, 474, 0.289377289377289)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Misc. Questions', N'TrainingApplicable', 2015, 77, 19, 0.0118380062305296)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Misc. Questions', N'TrainingApplicable', 2015, 88, 27, 0.0166256157635468)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'UseStandards', 2015, 0, 125, 0.0768757687576876)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'UseStandards', 2015, 1, 115, 0.0702933985330073)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'UseStandards', 2015, 2, 85, 0.0522433927473878)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'UseStandards', 2015, 3, 105, 0.0640634533251983)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'UseStandards', 2015, 4, 473, 0.289296636085627)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'UseStandards', 2015, 77, 726, 0.442682926829268)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'UseStandards', 2015, 88, 11, 0.00726552179656539)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2015, 1, 135, 0.0851735015772871)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2015, 2, 237, 0.145220588235294)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2015, 3, 370, 0.225609756097561)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2015, 4, 632, 0.385600976205003)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2015, 5, 255, 0.156154317207593)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2015, 88, 11, 0.00679851668726823)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Additional Questions', N'TopMgmt_ID', 2015, 1, 492, 0.300183038438072)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Additional Questions', N'TopMgmt_ID', 2015, 2, 520, 0.317654245571167)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Additional Questions', N'TopMgmt_ID', 2015, 3, 291, 0.180409175449473)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Additional Questions', N'TopMgmt_ID', 2015, 4, 227, 0.138922888616891)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Additional Questions', N'TopMgmt_ID', 2015, 5, 27, 0.0170347003154574)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Additional Questions', N'TopMgmt_ID', 2015, 88, 79, 0.0481707317073171)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 1, 47, 0.0300511508951407)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 2, 112, 0.0689655172413793)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 3, 317, 0.193646915088577)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 4, 673, 0.410365853658537)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 5, 426, 0.259914582062233)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 88, 44, 0.027603513174404)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 1, 46, 0.0287320424734541)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 2, 108, 0.0662983425414365)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 3, 262, 0.15995115995116)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 4, 683, 0.416463414634146)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 5, 510, 0.311165344722392)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 88, 10, 0.00696378830083566)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 1, 39, 0.0239852398523985)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 2, 108, 0.0660954712362301)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 3, 422, 0.257631257631258)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 4, 687, 0.41890243902439)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 5, 337, 0.205613178767541)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 88, 26, 0.017968210089841)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 1, 55, 0.0336597307221542)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 2, 107, 0.0660086366440469)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 3, 175, 0.108898568761668)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 4, 608, 0.371184371184371)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 5, 574, 0.35)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 88, 94, 0.0589711417816813)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 1, 32, 0.0196439533456108)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 2, 52, 0.031980319803198)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 3, 131, 0.0806153846153846)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 4, 702, 0.428571428571429)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 5, 645, 0.393292682926829)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 88, 51, 0.0362473347547974)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 1, 18, 0.0110837438423645)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 2, 37, 0.0227133210558625)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 3, 85, 0.0519242516799023)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 4, 668, 0.407814407814408)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 5, 769, 0.46890243902439)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 88, 36, 0.0225140712945591)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 1, 52, 0.03243917654398)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 2, 73, 0.0448954489544895)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 3, 289, 0.176219512195122)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 4, 637, 0.388888888888889)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 5, 479, 0.292251372788286)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 88, 79, 0.048406862745098)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 1, 38, 0.0237055520898316)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 2, 52, 0.0319214241866176)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 3, 178, 0.108735491753207)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 4, 675, 0.411585365853659)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 5, 655, 0.400856793145655)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 88, 11, 0.00790797987059669)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 1, 23, 0.0141625615763547)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 2, 58, 0.035670356703567)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 3, 196, 0.119512195121951)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 4, 810, 0.494203782794387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 5, 512, 0.315465187923598)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 88, 10, 0.00691085003455425)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 1, 61, 0.0388040712468193)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 2, 142, 0.0873308733087331)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 3, 282, 0.171951219512195)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 4, 684, 0.417327638804149)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 5, 432, 0.26438188494492)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 88, 8, 0.0066006600660066)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 1, 127, 0.0777233782129743)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 2, 196, 0.120245398773006)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 3, 355, 0.217524509803922)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 4, 577, 0.351829268292683)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 5, 318, 0.19473361910594)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 88, 32, 0.0221760221760222)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 1, 53, 0.0326958667489204)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 2, 81, 0.0496932515337423)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 3, 250, 0.15281173594132)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 4, 691, 0.421341463414634)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 5, 518, 0.317013463892289)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 88, 12, 0.00831600831600832)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 1, 22, 0.0159651669085631)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 2, 49, 0.0301724137931034)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 3, 289, 0.176758409785933)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 4, 819, 0.499390243902439)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 5, 409, 0.25030599755202)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 88, 17, 0.0117484450587422)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupFeedback', 2015, 1, 38, 0.0237055520898316)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupFeedback', 2015, 2, 92, 0.0564763658686311)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupFeedback', 2015, 3, 376, 0.229688454489921)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupFeedback', 2015, 4, 611, 0.372560975609756)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupFeedback', 2015, 5, 430, 0.263157894736842)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupFeedback', 2015, 88, 56, 0.0373084610259827)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupHoldsAccount', 2015, 1, 40, 0.0246305418719212)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupHoldsAccount', 2015, 2, 65, 0.039828431372549)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupHoldsAccount', 2015, 3, 277, 0.169211973121564)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupHoldsAccount', 2015, 4, 651, 0.396951219512195)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupHoldsAccount', 2015, 5, 447, 0.272727272727273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Effectivness of Program', N'SupHoldsAccount', 2015, 88, 123, 0.0782940802036919)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 1, 10, 0.00617665225447807)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 2, 46, 0.0283251231527094)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 3, 298, 0.182040317654246)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 4, 748, 0.45609756097561)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 5, 381, 0.234750462107209)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 88, 119, 0.07465495608532)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 1, 31, 0.0190651906519065)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 2, 38, 0.0234423195558297)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 3, 244, 0.149785144260282)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 4, 510, 0.310975609756098)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 5, 497, 0.304161566707466)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 88, 281, 0.172710510141365)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 1, 26, 0.0175557056043214)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 2, 38, 0.0249180327868852)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 3, 240, 0.146609651802077)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 4, 743, 0.453048780487805)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 5, 475, 0.290342298288509)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 88, 78, 0.0482076637824475)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 1, 22, 0.0159651669085631)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 2, 22, 0.0135886349598518)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 3, 180, 0.109957238851558)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 4, 771, 0.470121951219512)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 5, 572, 0.348993288590604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 88, 33, 0.0203955500618047)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 1, 11, 0.00679431747992588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 2, 25, 0.0156152404747033)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 3, 236, 0.144166157605376)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 4, 813, 0.495731707317073)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 5, 458, 0.279438682123246)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 88, 57, 0.0352286773794808)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 0, 78, 0.0486587648159701)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 1, 125, 0.0772081531809759)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 88, 8, 0.00492004920049201)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 0, 84, 0.0526976160602258)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 1, 26, 0.0161993769470405)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 88, 15, 0.00926497838171711)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'ReceiveReports', 2015, 0, 1305, 0.795731707317073)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'ReceiveReports', 2015, 1, 175, 0.107164727495407)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'ReceiveReports', 2015, 88, 115, 0.0712074303405573)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Tenure', 2015, 1, 95, 0.0583538083538084)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Tenure', 2015, 2, 141, 0.0862913096695226)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Tenure', 2015, 3, 181, 0.110568112400733)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Tenure', 2015, 4, 351, 0.214024390243902)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Tenure', 2015, 5, 285, 0.18664047151277)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Tenure', 2015, 6, 475, 0.302547770700637)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Tenure', 2015, 97, 67, 0.0435914118412492)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Supervisor', 2015, 0, 1071, 0.653846153846154)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Supervisor', 2015, 1, 512, 0.31219512195122)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Supervisor', 2015, 88, 12, 0.00763844684914067)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Mgmt', 2015, 1, 37, 0.0228113440197287)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Mgmt', 2015, 2, 177, 0.110763454317897)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Mgmt', 2015, 3, 219, 0.133618059792556)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Mgmt', 2015, 4, 975, 0.595238095238095)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Mgmt', 2015, 97, 187, 0.114024390243902)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Union', 2015, 0, 1206, 0.73581452104942)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Union', 2015, 1, 295, 0.185185185185185)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Union', 2015, 97, 94, 0.0573170731707317)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Age', 2015, 1, 113, 0.0690287110568112)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Age', 2015, 2, 437, 0.266625991458206)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Age', 2015, 3, 812, 0.497244335578689)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Age', 2015, 4, 23, 0.0143929912390488)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Age', 2015, 97, 210, 0.128048780487805)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Gender', 2015, 1, 831, 0.507016473459426)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Gender', 2015, 2, 521, 0.318070818070818)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'Gender', 2015, 97, 243, 0.148170731707317)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'LocationPerformWork', 2015, 1, 1266, 0.772422208663819)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'LocationPerformWork', 2015, 2, 240, 0.146609651802077)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'LocationPerformWork', 2015, 3, 11, 0.00801749271137026)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Participant Demographic Variables', N'LocationPerformWork', 2015, 97, 76, 0.0463414634146341)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'ECI Demographic Information', N'Country_NUM_Uniform', 2015, 195, 1640, 1)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'ECI Demographic Information', N'Region_NUM_Uniform', 2015, 5, 1640, 1)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'ECI Demographic Information', N'US_Intl', 2015, 1, 1640, 1)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2016, 1, 66, 0.00947731188971855)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2016, 2, 104, 0.0151382823871907)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2016, 3, 362, 0.0519666953775481)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2016, 4, 2620, 0.374821173104435)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2016, 5, 3652, 0.52290950744559)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2016, 88, 186, 0.0269331016507385)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2016, 1, 192, 0.0275981026304442)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2016, 2, 366, 0.0523605150214592)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2016, 3, 1194, 0.171207341554345)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2016, 4, 2896, 0.414365431392188)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2016, 5, 2061, 0.295103092783505)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2016, 77, 9, 0.00129236071223435)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2016, 88, 272, 0.038940586972083)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2016, 1, 139, 0.0199971227161559)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2016, 2, 225, 0.0324488029997116)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2016, 3, 545, 0.0781474046458274)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2016, 4, 2405, 0.344112176277007)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2016, 5, 3601, 0.51516452074392)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2016, 77, 12, 0.00172314761631246)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2016, 88, 63, 0.00923889133304004)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2016, 1, 63, 0.00935412026726058)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2016, 2, 124, 0.017800746482917)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2016, 3, 567, 0.0811739441660702)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2016, 4, 3115, 0.445636623748212)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2016, 5, 3031, 0.433991981672394)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2016, 77, 7, 0.00100516944284894)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2016, 88, 83, 0.0118962304715494)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2016, 1, 52, 0.00773349196906603)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2016, 2, 89, 0.0127361190612479)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2016, 3, 499, 0.0717263188155814)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2016, 4, 2759, 0.394706723891273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2016, 5, 3472, 0.496780655315496)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2016, 77, 6, 0.000861573808156232)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2016, 88, 99, 0.014189479719077)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2016, 1, 88, 0.0126600489138253)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2016, 2, 192, 0.0275624461670973)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2016, 3, 620, 0.0889016346429596)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2016, 4, 2482, 0.355078683834049)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2016, 5, 3528, 0.504793246530262)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2016, 77, 19, 0.00285456730769231)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2016, 88, 47, 0.0068533100029163)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2016, 1, 159, 0.0228415457549203)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2016, 2, 314, 0.0449341728677733)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2016, 3, 985, 0.140976098468585)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2016, 4, 2295, 0.328326180257511)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2016, 5, 2774, 0.396909429102876)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2016, 77, 3, 0.000450112528132033)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2016, 88, 422, 0.0604844489035402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2016, 1, 100, 0.0143864192202561)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2016, 2, 172, 0.0246913580246914)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2016, 3, 535, 0.0767795637198622)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2016, 4, 2499, 0.357510729613734)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2016, 5, 3395, 0.485763342395192)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2016, 77, 15, 0.00239043824701195)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2016, 88, 236, 0.0338545402381294)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2016, 1, 63, 0.00935412026726058)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2016, 2, 110, 0.0157909847832328)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2016, 3, 332, 0.0477285796434733)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2016, 4, 2314, 0.3310443490701)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2016, 5, 3980, 0.569466304192302)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2016, 77, 2, 0.00130633572828217)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2016, 88, 151, 0.0216425397735416)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2016, 1, 114, 0.0164005179110919)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2016, 2, 181, 0.0259015455065827)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2016, 3, 1136, 0.162587662802347)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2016, 4, 2638, 0.377396280400572)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2016, 5, 2495, 0.357245131729668)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2016, 77, 11, 0.00157955198161976)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2016, 88, 362, 0.0518253400143164)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2016, 1, 95, 0.0136670982592433)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2016, 2, 164, 0.0236447520184544)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2016, 3, 499, 0.071633649153029)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2016, 4, 2250, 0.321888412017167)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2016, 5, 3850, 0.550865646015167)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2016, 77, 18, 0.00286852589641434)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2016, 88, 61, 0.00880357916005195)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2016, 1, 64, 0.00919936754348138)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2016, 2, 93, 0.0133085289066972)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2016, 3, 818, 0.117292801835389)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2016, 4, 2713, 0.388125894134478)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2016, 5, 2930, 0.419230218915439)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2016, 77, 7, 0.00100516944284894)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2016, 88, 296, 0.0424251110792604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2016, 1, 56, 0.0080563947633434)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2016, 2, 90, 0.0133848899464604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2016, 3, 476, 0.0683220898521602)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2016, 4, 2519, 0.360371959942775)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2016, 5, 3702, 0.529689512090428)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2016, 77, 18, 0.00286852589641434)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2016, 88, 60, 0.00888757221152422)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2016, 1, 36, 0.00534521158129176)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2016, 2, 91, 0.0130634510479472)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2016, 3, 644, 0.0921711750393588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2016, 4, 2751, 0.393562231759657)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2016, 5, 3286, 0.470504009163803)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2016, 77, 6, 0.000900225056264066)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2016, 88, 107, 0.0153361043428408)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2016, 1, 89, 0.0128279042951859)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2016, 2, 176, 0.0252982607445738)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2016, 3, 1029, 0.147252432741843)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2016, 4, 1981, 0.283404864091559)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2016, 5, 2487, 0.356099656357388)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2016, 88, 1154, 0.165353202464536)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2016, 1, 98, 0.014098690835851)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2016, 2, 221, 0.0320661636680209)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2016, 3, 1135, 0.162374821173104)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2016, 4, 2467, 0.352983259407641)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2016, 5, 2666, 0.381565764992128)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2016, 77, 24, 0.00382470119521912)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2016, 88, 302, 0.0433347682594346)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'General misconduct ', N'MiscGen', 2016, 0, 5740, 0.821173104434907)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'General misconduct ', N'MiscGen', 2016, 1, 718, 0.103042479908152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'General misconduct ', N'MiscGen', 2016, 88, 452, 0.0649705332758373)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2016, 0, 261, 0.0374569460390356)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2016, 1, 411, 0.0590008613264427)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2016, 88, 45, 0.0065406976744186)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2016, 0, 37, 0.00530998851894374)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2016, 1, 187, 0.0269026039418789)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2016, 88, 33, 0.00478816018572258)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAcceptedBhvr', 2016, 0, 106, 0.0152123995407577)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAcceptedBhvr', 2016, 1, 94, 0.0135232340670407)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAcceptedBhvr', 2016, 88, 57, 0.00827045850261172)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2016, 0, 185, 0.0265499425947187)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2016, 1, 45, 0.00658087159988301)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2016, 88, 27, 0.00396942075860041)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2016, 0, 138, 0.019804822043628)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2016, 1, 88, 0.0126600489138253)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2016, 88, 31, 0.00455748309320788)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2016, 0, 134, 0.0194428322692977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2016, 1, 97, 0.0139207807118255)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2016, 88, 26, 0.00382240517494854)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2016, 0, 169, 0.0242537313432836)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2016, 1, 67, 0.00978102189781022)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2016, 88, 21, 0.003047011027278)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2016, 0, 73, 0.0104764638346728)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2016, 1, 160, 0.0230182707524097)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2016, 88, 24, 0.00373018340068387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2016, 0, 182, 0.0261194029850746)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2016, 1, 44, 0.00638421358096344)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2016, 88, 31, 0.00455748309320788)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2016, 0, 202, 0.0289896670493685)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2016, 1, 35, 0.00543225205649542)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2016, 88, 20, 0.00294031167303734)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2016, 0, 165, 0.0236796785304248)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2016, 1, 67, 0.00963890087757157)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2016, 88, 25, 0.00367538959129668)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2016, 0, 174, 0.0249712973593571)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2016, 1, 41, 0.00601173020527859)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2016, 88, 42, 0.00617465451337842)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepDidntBelieveMisc', 2016, 0, 205, 0.0294202066590126)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepDidntBelieveMisc', 2016, 1, 30, 0.00456482045039562)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepDidntBelieveMisc', 2016, 88, 22, 0.00323434284034108)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepPastRetal', 2016, 0, 201, 0.0291642484039466)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepPastRetal', 2016, 1, 34, 0.00487944890929966)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepPastRetal', 2016, 88, 22, 0.00323434284034108)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2016, 1, 104, 0.0149403821290045)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2016, 2, 96, 0.0139799038881608)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2016, 3, 86, 0.0123456790123457)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2016, 4, 58, 0.00836096295228485)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2016, 5, 53, 0.00766338924233661)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2016, 88, 13, 0.00212037188060675)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2016, 0, 69, 0.00991236891251257)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2016, 1, 98, 0.0142649199417758)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2016, 88, 32, 0.00465996796272026)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2016, 0, 35, 0.00504468146439896)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2016, 1, 86, 0.0123545467605229)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2016, 88, 78, 0.011353711790393)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatExpRetaliation', 2016, 0, 113, 0.0162332998132452)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatExpRetaliation', 2016, 1, 56, 0.00807149034303834)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatExpRetaliation', 2016, 88, 30, 0.00448765893792072)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2016, 0, 117, 0.01680792989513)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2016, 1, 35, 0.00509683995922528)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2016, 88, 47, 0.00677428653790718)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2016, 0, 32, 0.00512163892445583)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2016, 1, 106, 0.0152276971699468)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2016, 88, 61, 0.00889472149314669)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2016, 0, 35, 0.00502801321649188)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2016, 1, 91, 0.0132459970887918)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2016, 88, 73, 0.010521764197175)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2016, 0, 273, 0.0391903531438415)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2016, 1, 67, 0.00965696166042087)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2016, 88, 69, 0.0100436681222707)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Outcomes - Key measures used to assess ethical culture', N'Pressure', 2016, 0, 6521, 0.932904148783977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Outcomes - Key measures used to assess ethical culture', N'Pressure', 2016, 1, 228, 0.0328010358221839)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Outcomes - Key measures used to assess ethical culture', N'Pressure', 2016, 88, 149, 0.0213834672789897)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Standards', 2015, 0, 7, 0.0080091533180778)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Standards', 2015, 1, 883, 0.963973799126638)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Standards', 2015, 88, 26, 0.0288568257491676)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Training', 2015, 0, 15, 0.017162471395881)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Training', 2015, 1, 867, 0.946506550218341)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Training', 2015, 88, 34, 0.0386363636363636)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Advice', 2015, 0, 8, 0.0091533180778032)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Advice', 2015, 1, 855, 0.933406113537118)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Advice', 2015, 88, 53, 0.0588235294117647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Hotline', 2015, 0, 9, 0.0102974828375286)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Hotline', 2015, 1, 856, 0.934497816593887)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Hotline', 2015, 88, 51, 0.0562293274531422)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Appraisal', 2015, 0, 51, 0.0572390572390572)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Appraisal', 2015, 1, 714, 0.780327868852459)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Appraisal', 2015, 88, 151, 0.164847161572052)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Discipline', 2015, 0, 12, 0.0137299771167048)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Discipline', 2015, 1, 787, 0.859170305676856)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Resources', N'Discipline', 2015, 88, 117, 0.128289473684211)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'TrainingApplicable', 2015, 0, 22, 0.0249433106575964)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'TrainingApplicable', 2015, 1, 620, 0.676855895196507)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'TrainingApplicable', 2015, 70, 227, 0.248630887185104)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'TrainingApplicable', 2015, 77, 30, 0.0329308452250274)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'TrainingApplicable', 2015, 88, 17, 0.0205066344993969)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'UseStandards', 2015, 0, 48, 0.053156146179402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'UseStandards', 2015, 1, 49, 0.0543237250554324)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'UseStandards', 2015, 2, 45, 0.0491803278688525)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'UseStandards', 2015, 3, 71, 0.0777656078860898)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'UseStandards', 2015, 4, 290, 0.320796460176991)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'UseStandards', 2015, 77, 405, 0.442139737991266)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'UseStandards', 2015, 88, 8, 0.0110957004160888)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 1, 18, 0.0200445434298441)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 2, 50, 0.0568181818181818)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 3, 137, 0.149890590809628)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 4, 388, 0.423580786026201)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 5, 305, 0.333333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 88, 10, 0.0113378684807256)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 1, 12, 0.0140515222482436)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 2, 46, 0.0509413067552602)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 3, 207, 0.226477024070022)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 4, 400, 0.436681222707424)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 5, 230, 0.251366120218579)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2015, 88, 13, 0.0144766146993318)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 1, 14, 0.017478152309613)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 2, 29, 0.0329545454545455)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 3, 51, 0.0566666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 4, 366, 0.399563318777293)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 5, 407, 0.444808743169399)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 88, 36, 0.0395170142700329)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 1, 12, 0.0140515222482436)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 2, 27, 0.0364372469635627)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 3, 61, 0.067032967032967)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 4, 367, 0.400655021834061)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 5, 422, 0.46120218579235)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 88, 9, 0.0112359550561798)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 1, 4, 0.00468384074941452)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 2, 21, 0.023972602739726)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 3, 78, 0.0857142857142857)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 4, 430, 0.46943231441048)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 5, 358, 0.391256830601093)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2015, 88, 7, 0.0106870229007634)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 1, 24, 0.0281030444964871)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 2, 61, 0.0693181818181818)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 3, 102, 0.112458654906284)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 4, 398, 0.434497816593886)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 5, 304, 0.33224043715847)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2015, 88, 7, 0.0101156069364162)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 1, 13, 0.0152224824355972)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 2, 34, 0.0419753086419753)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 3, 86, 0.0955555555555556)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 4, 396, 0.432314410480349)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 5, 359, 0.392349726775956)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 88, 8, 0.00998751560549313)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 1, 5, 0.00585480093676815)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 2, 18, 0.0201793721973094)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 3, 102, 0.113333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 4, 480, 0.524017467248908)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 5, 286, 0.312568306010929)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 88, 5, 0.00617283950617284)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'SupFeedback', 2015, 1, 15, 0.0171232876712329)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'SupFeedback', 2015, 2, 32, 0.035437430786268)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'SupFeedback', 2015, 3, 164, 0.18021978021978)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'SupFeedback', 2015, 4, 351, 0.383187772925764)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'SupFeedback', 2015, 5, 294, 0.321311475409836)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Effectivness of Program', N'SupFeedback', 2015, 88, 40, 0.0441988950276243)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 1, 5, 0.00585480093676815)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 2, 20, 0.0264200792602378)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 3, 99, 0.109877913429523)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 4, 437, 0.47707423580786)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 5, 270, 0.295081967213115)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 88, 64, 0.0702524698133919)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 1, 7, 0.0102489019033675)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 2, 12, 0.0165289256198347)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 3, 59, 0.0655555555555556)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 4, 408, 0.445414847161572)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 5, 377, 0.412021857923497)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 88, 30, 0.0351288056206089)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 1, 2, 0.00292825768667643)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 2, 7, 0.0106707317073171)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 3, 82, 0.0910099889012209)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 4, 425, 0.463973799126638)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 5, 344, 0.375956284153005)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 88, 33, 0.0386416861826698)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Pressure', N'Pressure', 2015, 0, 824, 0.899563318777293)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Pressure', N'Pressure', 2015, 1, 38, 0.0438799076212471)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Pressure', N'Pressure', 2015, 88, 31, 0.0344444444444444)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'General misconduct ', N'MiscGen', 2015, 0, 759, 0.828602620087336)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'General misconduct ', N'MiscGen', 2015, 1, 82, 0.0957943925233645)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'General misconduct ', N'MiscGen', 2015, 88, 52, 0.0590909090909091)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 0, 34, 0.0398126463700234)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 1, 41, 0.0533159947984395)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 88, 7, 0.00817757009345794)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 0, 29, 0.0377113133940182)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 1, 6, 0.00826446280991736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 88, 6, 0.00888888888888889)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Participant Demographic Variables', N'Tenure', 2015, 1, 135, 0.147379912663755)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Participant Demographic Variables', N'Tenure', 2015, 2, 171, 0.188741721854305)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Participant Demographic Variables', N'Tenure', 2015, 3, 204, 0.223194748358862)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Participant Demographic Variables', N'Tenure', 2015, 4, 210, 0.233853006681514)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Participant Demographic Variables', N'Tenure', 2015, 5, 135, 0.152370203160271)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Participant Demographic Variables', N'Tenure', 2015, 6, 12, 0.0145985401459854)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Participant Demographic Variables', N'Tenure', 2015, 97, 23, 0.0279126213592233)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'ECI Demographic Information', N'US_Intl', 2015, 1, 916, 1)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'ECI Demographic Information', N'Country_NUM_Uniform', 2015, 195, 916, 1)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Standards', 2010, 0, 39, 0.0065989847715736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Standards', 2010, 1, 5884, 0.980666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Standards', 2010, 88, 77, 0.0130420054200542)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Training', 2010, 0, 104, 0.017514314584035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Training', 2010, 1, 5725, 0.954166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Training', 2010, 88, 171, 0.0285809794417516)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Advice', 2010, 0, 78, 0.0131357359380263)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Advice', 2010, 1, 5646, 0.941)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Advice', 2010, 88, 276, 0.0461847389558233)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Hotline', 2010, 0, 81, 0.0136409565510273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Hotline', 2010, 1, 5756, 0.959333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Hotline', 2010, 88, 163, 0.027339818852734)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Appraisal', 2010, 0, 338, 0.0564933979608892)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Appraisal', 2010, 1, 5104, 0.850666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Appraisal', 2010, 88, 558, 0.0933110367892977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Discipline', 2010, 0, 165, 0.027661357921207)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Discipline', 2010, 1, 4780, 0.796666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Discipline', 2010, 88, 1055, 0.175950633755837)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2010, 0, 287, 0.0478333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2010, 1, 704, 0.117411607738492)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2010, 2, 762, 0.127021170195033)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2010, 3, 947, 0.157885961987329)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2010, 4, 1677, 0.279733110925771)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2010, 77, 1599, 0.266633316658329)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2010, 88, 24, 0.00407608695652174)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2010, 1, 264, 0.0440513932921742)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2010, 2, 566, 0.0943333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2010, 3, 1209, 0.20163442294863201)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2010, 4, 2869, 0.478246374395733)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2010, 5, 1049, 0.174920793730198)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2010, 88, 32, 0.0054412514878422)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2010, 1, 3167, 0.527833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2010, 2, 518, 0.0865497076023392)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2010, 3, 1413, 0.235735735735736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2010, 4, 502, 0.0837085209271302)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2010, 5, 171, 0.028566655529569)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2010, 88, 202, 0.0346246143297909)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2010, 1, 43, 0.00728443164492631)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2010, 2, 162, 0.0270315367929251)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2010, 3, 855, 0.142904897208758)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2010, 4, 3136, 0.522840946982327)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2010, 5, 1405, 0.234205700950158)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2010, 88, 362, 0.0603333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2010, 1, 202, 0.033689126084056)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2010, 2, 415, 0.0692243536280234)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2010, 3, 937, 0.156453498079813)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2010, 4, 2931, 0.4885)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2010, 5, 1386, 0.231038506417736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2010, 77, 23, 0.00388973448334179)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2010, 88, 40, 0.0070459749867888)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2010, 1, 261, 0.0435508092774904)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2010, 2, 558, 0.093)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2010, 3, 1381, 0.23032021347565)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2010, 4, 2705, 0.450908484747458)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2010, 5, 904, 0.150716905635212)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2010, 88, 108, 0.0180360721442886)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2010, 1, 150, 0.0250292007341899)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2010, 2, 317, 0.0531433361274099)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2010, 3, 839, 0.139926617745163)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2010, 4, 2858, 0.476333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2010, 5, 1674, 0.279093031010337)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2010, 77, 16, 0.00270590224928124)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2010, 88, 63, 0.0105210420841683)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2010, 1, 40, 0.0066744535291173)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2010, 2, 207, 0.0346038114343029)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2010, 3, 1025, 0.170947298198799)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2010, 4, 3386, 0.564333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2010, 5, 1152, 0.192096048024012)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2010, 88, 107, 0.0178690714762859)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2010, 1, 54, 0.00954232196501149)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2010, 2, 121, 0.0201666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2010, 3, 424, 0.0707492074086434)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2010, 4, 3111, 0.518586431071845)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2010, 5, 2115, 0.352852852852853)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2010, 88, 62, 0.0103540414161657)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2010, 1, 57, 0.00989755165827401)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2010, 2, 183, 0.0306532663316583)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2010, 3, 582, 0.097)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2010, 4, 3004, 0.500750125020837)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2010, 5, 2001, 0.333833833833834)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2010, 77, 15, 0.00253678335870117)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2010, 88, 45, 0.00778008298755187)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2010, 1, 40, 0.00695531211963137)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2010, 2, 254, 0.0425460636515913)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2010, 3, 1238, 0.206470980653769)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2010, 4, 3006, 0.501)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2010, 5, 1251, 0.208882952078811)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2010, 77, 2, 0.000342759211653813)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2010, 88, 96, 0.0160669456066946)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2010, 1, 127, 0.0211843202668891)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2010, 2, 288, 0.0481363864282133)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2010, 3, 659, 0.109961621892208)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2010, 4, 2489, 0.414833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2010, 5, 1972, 0.328995662328996)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2010, 88, 327, 0.0545363575717145)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2010, 1, 71, 0.0123007623007623)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2010, 2, 142, 0.0240514905149051)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2010, 3, 475, 0.0792194796531021)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2010, 4, 2779, 0.463166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2010, 5, 2150, 0.358632193494579)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2010, 77, 14, 0.00236766446812109)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2010, 88, 231, 0.0385771543086172)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2010, 1, 33, 0.0055045871559633)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2010, 2, 101, 0.0168333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2010, 3, 362, 0.0603735823882588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2010, 4, 2770, 0.461743623937323)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2010, 5, 2445, 0.407907907907908)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2010, 88, 151, 0.0252171008684035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2010, 1, 140, 0.0233527939949958)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2010, 2, 298, 0.0500503862949278)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2010, 3, 1015, 0.169166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2010, 4, 2753, 0.45890981830305)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2010, 5, 1452, 0.242080693564521)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2010, 88, 179, 0.0298931195724783)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2010, 1, 88, 0.0146837977640581)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2010, 2, 169, 0.0283842794759825)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2010, 3, 581, 0.0968979319546364)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2010, 4, 2877, 0.4795)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2010, 5, 2044, 0.340780260086696)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2010, 77, 16, 0.00270590224928124)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2010, 88, 62, 0.0103540414161657)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2010, 1, 29, 0.00484140233722871)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2010, 2, 124, 0.0206908059402636)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2010, 3, 789, 0.1315)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2010, 4, 3337, 0.55625937656276)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2010, 5, 1484, 0.247580914247581)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2010, 77, 1, 0.000261301280376274)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2010, 88, 73, 0.0123519458544839)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2010, 1, 64, 0.0106791256465877)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2010, 2, 122, 0.0203502919099249)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2010, 3, 744, 0.124)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2010, 4, 3063, 0.510755377688844)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2010, 5, 1682, 0.280380063343891)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2010, 88, 123, 0.020682697158231)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2010, 1, 52, 0.00867678958785249)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2010, 2, 89, 0.0149203688181056)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2010, 3, 518, 0.0863909272848566)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2010, 4, 3025, 0.504166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2010, 5, 2053, 0.342223703950658)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2010, 77, 16, 0.00270590224928124)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2010, 88, 45, 0.00768705158865733)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2010, 1, 18, 0.00300350408810279)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2010, 2, 96, 0.016)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2010, 3, 841, 0.140260173448966)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2010, 4, 3188, 0.531599132899783)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2010, 5, 1581, 0.263543923987331)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2010, 77, 3, 0.00051413881748072)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2010, 88, 71, 0.0118570474281897)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2010, 1, 83, 0.0140892887455441)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2010, 2, 240, 0.0400266844563042)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2010, 3, 1139, 0.190055064241615)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2010, 4, 2688, 0.448074679113186)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2010, 5, 1436, 0.239413137712571)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2010, 77, 19, 0.00321325892102148)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2010, 88, 179, 0.0298333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2010, 1, 75, 0.0125104253544621)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2010, 2, 229, 0.0382751128196557)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2010, 3, 919, 0.153268845897265)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2010, 4, 2259, 0.3765)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2010, 5, 1403, 0.234262815161129)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2010, 88, 890, 0.148481815148482)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'General misconduct ', N'MiscGen', 2010, 0, 4415, 0.735955992665444)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'General misconduct ', N'MiscGen', 2010, 1, 908, 0.151333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'General misconduct ', N'MiscGen', 2010, 88, 440, 0.0734189888202903)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2010, 0, 399, 0.0667558976074954)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2010, 1, 478, 0.0796666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2010, 88, 30, 0.00502933780385583)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2010, 0, 94, 0.0158596254428885)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2010, 1, 265, 0.0443366237242764)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2010, 88, 37, 0.00644487023166696)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2010, 0, 319, 0.0534159410582719)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2010, 1, 46, 0.00769616864647817)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2010, 88, 31, 0.00540352100400906)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2010, 0, 181, 0.0303081044876088)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2010, 1, 170, 0.0284423623891584)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2010, 88, 45, 0.00759237388223384)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2010, 0, 239, 0.040020093770931)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2010, 1, 129, 0.0215827338129496)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2010, 88, 28, 0.0047241437489455)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2010, 0, 296, 0.0495231721599465)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2010, 1, 75, 0.0127312850110338)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2010, 88, 25, 0.00421798549012991)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2010, 0, 133, 0.022274325908558)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2010, 1, 225, 0.0376758204956463)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2010, 88, 38, 0.00635770453404718)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2010, 0, 288, 0.0482250502344273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2010, 1, 68, 0.0115430317433373)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2010, 88, 40, 0.00669232056215493)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2010, 0, 291, 0.0486866320896771)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2010, 1, 83, 0.0146798726565264)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2010, 88, 22, 0.00383208500261279)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2010, 0, 280, 0.0468854655056932)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2010, 1, 66, 0.0118983234180638)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2010, 88, 50, 0.00836540070269366)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2010, 0, 266, 0.0445411922304086)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2010, 1, 73, 0.0123875784829459)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2010, 88, 57, 0.00953655680107077)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2010, 0, 14, 0.00233333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2010, 1, 457, 0.0762301918265221)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2010, 88, 4, 0.00107642626480086)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2010, 0, 32, 0.00533333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2010, 1, 403, 0.0673575129533679)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2010, 88, 40, 0.00671817265703729)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2010, 0, 87, 0.0145)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2010, 1, 311, 0.0521112600536193)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2010, 88, 77, 0.0132530120481928)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2010, 0, 87, 0.0150623268698061)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2010, 1, 340, 0.0566666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2010, 88, 48, 0.00806180718844474)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2010, 0, 88, 0.0150401640745172)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2010, 1, 311, 0.0518333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2010, 88, 76, 0.0132635253054101)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2010, 0, 63, 0.0105)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2010, 1, 360, 0.0601704830352666)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2010, 88, 52, 0.00919377652050919)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2010, 1, 73, 0.012341504649197)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2010, 2, 87, 0.0145412000668561)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2010, 3, 76, 0.0126666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2010, 4, 142, 0.0237935656836461)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2010, 5, 78, 0.0131246845027764)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2010, 88, 19, 0.00369218810726778)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2010, 0, 46, 0.00778210116731518)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2010, 1, 94, 0.0158918005071851)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2010, 88, 20, 0.00334280461307037)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2010, 0, 32, 0.00541363559465404)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2010, 1, 65, 0.010989010989011)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2010, 88, 63, 0.0105298345311717)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2010, 0, 94, 0.0158918005071851)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2010, 1, 23, 0.00389105058365759)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2010, 88, 43, 0.00718702991810129)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2010, 0, 29, 0.00490610725765522)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2010, 1, 92, 0.0153769012201237)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2010, 88, 39, 0.00675207756232687)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2010, 0, 32, 0.00550964187327824)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2010, 1, 67, 0.0111983954537857)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2010, 88, 61, 0.0105609418282548)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2010, 0, 322, 0.0536666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2010, 1, 82, 0.0137977452465085)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2010, 88, 69, 0.0118801652892562)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2010, 0, 43, 0.00746657405799618)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2010, 1, 32, 0.00541363559465404)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2010, 88, 5, 0.000841325929665152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2010, 0, 33, 0.00575916230366492)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2010, 1, 41, 0.00693622060565048)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2010, 88, 6, 0.00100959111559818)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2010, 0, 47, 0.00812586445366528)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2010, 1, 28, 0.00473693114532228)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2010, 88, 5, 0.000841325929665152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2010, 0, 52, 0.0090293453724605)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2010, 1, 23, 0.00389105058365759)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2010, 88, 5, 0.000841325929665152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2010, 0, 69, 0.0116731517509728)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2010, 1, 6, 0.00119856172592889)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2010, 88, 5, 0.000841325929665152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2010, 0, 44, 0.00767888307155323)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2010, 1, 28, 0.00473693114532228)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2010, 88, 8, 0.00134612148746424)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2010, 0, 45, 0.00761292505498224)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2010, 1, 28, 0.00486195520055565)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2010, 88, 7, 0.00117785630153121)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2010, 0, 58, 0.00981221451531044)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2010, 1, 15, 0.00270660411403825)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2010, 88, 7, 0.00117785630153121)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2010, 0, 49, 0.00828962950431399)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2010, 1, 22, 0.00382010765757944)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2010, 88, 9, 0.00151438667339727)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2010, 0, 32, 0.00541363559465404)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2010, 1, 31, 0.00535961272475795)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2010, 88, 17, 0.00286050816086152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSexHarass', 2010, 0, 5269, 0.878313052175363)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSexHarass', 2010, 1, 241, 0.0401666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSexHarass', 2010, 88, 183, 0.0305356248957117)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOfferKickbacks', 2010, 0, 5354, 0.892333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOfferKickbacks', 2010, 1, 89, 0.0148531375166889)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOfferKickbacks', 2010, 88, 250, 0.0417153345569831)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAbuse', 2010, 0, 4433, 0.738956492748791)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAbuse', 2010, 1, 981, 0.1635)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAbuse', 2010, 88, 279, 0.0466555183946488)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsDiscrim', 2010, 0, 5023, 0.837166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsDiscrim', 2010, 1, 397, 0.0663657639585423)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsDiscrim', 2010, 88, 273, 0.0455531453362256)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsHealthViol', 2010, 0, 5020, 0.836945648549516)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsHealthViol', 2010, 1, 461, 0.0768333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsHealthViol', 2010, 88, 212, 0.0354041416165665)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConflictInterest', 2010, 0, 4751, 0.791833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConflictInterest', 2010, 1, 547, 0.0912731520106791)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConflictInterest', 2010, 88, 395, 0.0658772515010007)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsFailProfStandards', 2010, 0, 4929, 0.8215)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsFailProfStandards', 2010, 1, 370, 0.0628076727211)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsFailProfStandards', 2010, 88, 394, 0.0657104736490994)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAlterDocs', 2010, 0, 5185, 0.864166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAlterDocs', 2010, 1, 209, 0.0349323082065853)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAlterDocs', 2010, 88, 299, 0.0498915401301518)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCompetInfo', 2010, 0, 5280, 0.88)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCompetInfo', 2010, 1, 65, 0.0108841259209645)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCompetInfo', 2010, 88, 348, 0.0580677457033205)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConfInfo', 2010, 0, 5323, 0.887166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConfInfo', 2010, 1, 71, 0.0118967828418231)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConfInfo', 2010, 88, 299, 0.0498915401301518)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperHire', 2010, 0, 4959, 0.8265)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperHire', 2010, 1, 275, 0.0458868680126815)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperHire', 2010, 88, 459, 0.0765765765765766)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2010, 0, 5170, 0.861666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2010, 1, 96, 0.0160454621427378)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2010, 88, 427, 0.0712497914233272)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieExternal', 2010, 0, 5028, 0.838)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieExternal', 2010, 1, 250, 0.0417850576633796)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieExternal', 2010, 88, 415, 0.069247455364592)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieEmployees', 2010, 0, 4651, 0.775166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieEmployees', 2010, 1, 508, 0.0847372810675563)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieEmployees', 2010, 88, 534, 0.0891783567134269)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsTimeCheat', 2010, 0, 5004, 0.834)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsTimeCheat', 2010, 1, 340, 0.0567328549974971)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsTimeCheat', 2010, 88, 348, 0.0580386924616411)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsStealing', 2010, 0, 5330, 0.888333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsStealing', 2010, 1, 125, 0.0210650488709134)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsStealing', 2010, 88, 238, 0.039712998498248)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsInternetAbuse', 2010, 0, 4925, 0.820970161693615)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsInternetAbuse', 2010, 1, 412, 0.0686666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsInternetAbuse', 2010, 88, 356, 0.0593729152768512)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEnvViol', 2010, 0, 5347, 0.891166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEnvViol', 2010, 1, 74, 0.0123683770683604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEnvViol', 2010, 88, 272, 0.0453862839979977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplBenViols', 2010, 0, 5011, 0.835166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplBenViols', 2010, 1, 253, 0.0422159185716669)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplBenViols', 2010, 88, 429, 0.0715476984656438)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2010, 0, 5342, 0.890333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2010, 1, 37, 0.00618418853418018)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2010, 88, 314, 0.0523944602035708)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2010, 0, 5219, 0.869833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2010, 1, 163, 0.0272438575965235)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2010, 88, 311, 0.051893876188887)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2010, 0, 4791, 0.798633105517586)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2010, 1, 540, 0.09)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2010, 88, 362, 0.0603735823882588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2010, 0, 5363, 0.893833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2010, 1, 125, 0.0208960213975259)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2010, 88, 205, 0.0342065743367262)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAntiCompPract', 2010, 0, 5327, 0.887833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAntiCompPract', 2010, 1, 29, 0.00484706668895203)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAntiCompPract', 2010, 88, 337, 0.0562322709828133)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperContracts', 2010, 0, 5091, 0.8485)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperContracts', 2010, 1, 160, 0.0267022696929239)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperContracts', 2010, 88, 442, 0.0737158105403602)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsContractViols', 2010, 0, 5163, 0.8605)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsContractViols', 2010, 1, 113, 0.0189343163538874)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsContractViols', 2010, 88, 417, 0.0695811780410479)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOther', 2010, 0, 4427, 0.737833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOther', 2010, 1, 112, 0.0186822351959967)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOther', 2010, 88, 1154, 0.192557984315034)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Pressure', N'Pressure', 2010, 0, 5122, 0.853666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Pressure', N'Pressure', 2010, 1, 328, 0.0547122602168474)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Pressure', N'Pressure', 2010, 88, 210, 0.0350408810278658)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressureFreq', 2010, 1, 97, 0.0162778989763383)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressureFreq', 2010, 2, 139, 0.023185988323603)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressureFreq', 2010, 3, 51, 0.00862798172897987)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressureFreq', 2010, 4, 30, 0.00509251400441351)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressureFreq', 2010, 88, 12, 0.00212051599222477)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2010, 0, 652, 0.108866254800468)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2010, 1, 758, 0.126607649908134)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2010, 2, 2415, 0.4025)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2010, 3, 1723, 0.287406171809842)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2010, 88, 102, 0.0170198564992491)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceKeepJob', 2010, 0, 1474, 0.245748582860954)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceKeepJob', 2010, 1, 1199, 0.199833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceKeepJob', 2010, 2, 1844, 0.307384564094016)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceKeepJob', 2010, 3, 1011, 0.16869681294844)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceKeepJob', 2010, 88, 122, 0.0205283526838297)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2010, 0, 1937, 0.322887147857976)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2010, 1, 1221, 0.2035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2010, 2, 1544, 0.257590924257591)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2010, 3, 589, 0.0984785152984451)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2010, 88, 359, 0.0598632649658162)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2010, 0, 1673, 0.2790658882402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2010, 1, 1540, 0.256666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2010, 2, 1769, 0.294882480413402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2010, 3, 544, 0.0907725679959953)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2010, 88, 124, 0.0208648830556958)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceMyCareer', 2010, 0, 2006, 0.334444814938313)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceMyCareer', 2010, 1, 1408, 0.234666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceMyCareer', 2010, 2, 1516, 0.252708784797466)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceMyCareer', 2010, 3, 518, 0.0866220735785953)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceMyCareer', 2010, 88, 202, 0.0337059903220424)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceExtDemands', 2010, 0, 2861, 0.476833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceExtDemands', 2010, 1, 919, 0.153371161548732)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceExtDemands', 2010, 2, 868, 0.145077720207254)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceExtDemands', 2010, 3, 270, 0.0451505016722408)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceExtDemands', 2010, 88, 732, 0.122142499582847)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'IrregularSituations', 2010, 0, 3318, 0.553)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'IrregularSituations', 2010, 1, 1792, 0.298766255418473)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'IrregularSituations', 2010, 88, 539, 0.0898932621747832)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2010, 1, 35, 0.00592918854819583)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2010, 2, 61, 0.0105719237435009)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2010, 3, 769, 0.128166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2010, 4, 3175, 0.529254875812635)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2010, 5, 1475, 0.24603836530442)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2010, 88, 133, 0.0222110888443554)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2010, 1, 154, 0.0260884296120617)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2010, 2, 408, 0.0680567139282736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2010, 3, 1405, 0.234322881921281)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2010, 4, 2477, 0.412833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2010, 5, 740, 0.123374458152718)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2010, 88, 460, 0.076756215584849)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2010, 1, 74, 0.0125359986447569)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2010, 2, 203, 0.033861551292744)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2010, 3, 1063, 0.177284856571047)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2010, 4, 2893, 0.482166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2010, 5, 1060, 0.176725575191731)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2010, 88, 351, 0.0585683297180043)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2010, 1, 287, 0.0482190860215054)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2010, 2, 675, 0.112593828190158)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2010, 3, 1315, 0.219569210218734)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2010, 4, 2362, 0.393666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2010, 5, 730, 0.121707235745248)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2010, 88, 275, 0.0464998309097058)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2010, 1, 297, 0.0496489468405216)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2010, 2, 575, 0.0959132610508757)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2010, 3, 1862, 0.310333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2010, 4, 1844, 0.307384564094016)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2010, 5, 526, 0.0876958986328776)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2010, 88, 540, 0.0904825737265416)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2010, 1, 15, 0.00261278522905417)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2010, 2, 27, 0.00450375312760634)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2010, 3, 219, 0.0367141659681475)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2010, 4, 2638, 0.439666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2010, 5, 2712, 0.452075345890982)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2010, 88, 32, 0.00533956282329384)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2010, 1, 30, 0.00522557045810834)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2010, 2, 61, 0.0101751459549625)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2010, 3, 360, 0.0601704830352666)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2010, 4, 2595, 0.4325)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2010, 5, 2552, 0.425404234039007)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2010, 88, 45, 0.00750876022025697)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2010, 1, 95, 0.0158518271316536)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2010, 2, 279, 0.0465232616308154)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2010, 3, 892, 0.148666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2010, 4, 3024, 0.504084014002334)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2010, 5, 1336, 0.222963951935915)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2010, 88, 15, 0.0027603974972396)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2010, 1, 200, 0.0333722676455865)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2010, 2, 698, 0.116333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2010, 3, 2316, 0.386257505003335)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2010, 4, 1375, 0.229243081027009)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2010, 5, 332, 0.0557233971131252)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2010, 88, 719, 0.120073480293921)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2010, 1, 1398, 0.233311081441923)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2010, 2, 2180, 0.363515090878773)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2010, 3, 898, 0.149766511007338)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2010, 4, 322, 0.0536666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2010, 5, 101, 0.0168529951610212)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2010, 77, 24, 0.00405885337392187)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2010, 88, 717, 0.119519919986664)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2010, 1, 1083, 0.181043129388164)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2010, 2, 2228, 0.371519092879773)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2010, 3, 1017, 0.1695)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2010, 4, 370, 0.0616872290763588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2010, 5, 81, 0.0136248948696384)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2010, 88, 859, 0.143190531755293)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2010, 1, 1090, 0.182213306586426)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2010, 2, 2419, 0.403368350842088)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2010, 3, 1040, 0.173333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2010, 4, 304, 0.0506835611870624)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2010, 5, 51, 0.00857863751051304)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2010, 88, 733, 0.122187031171862)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2010, 1, 26, 0.00458553791887125)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2010, 2, 124, 0.0207253886010363)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2010, 3, 629, 0.104903268845897)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2010, 4, 3243, 0.5405)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2010, 5, 1064, 0.177570093457944)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2010, 88, 550, 0.0917125229281307)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'ValuesConflict', 2010, 0, 1843, 0.307166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'ValuesConflict', 2010, 1, 2517, 0.419569928321387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'ValuesConflict', 2010, 2, 1123, 0.187291527685123)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'ValuesConflict', 2010, 3, 59, 0.0099276459700488)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'ValuesConflict', 2010, 88, 93, 0.0155648535564854)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2010, 1, 110, 0.0183639398998331)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2010, 2, 442, 0.074036850921273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2010, 3, 1284, 0.214)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2010, 4, 970, 0.161801501251043)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2010, 5, 2529, 0.42164054684895)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2010, 99, 297, 0.0497904442581727)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Mgmt', 2010, 1, 215, 0.0360980523841504)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Mgmt', 2010, 2, 1137, 0.189563187729243)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Mgmt', 2010, 3, 814, 0.135802469135802)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Mgmt', 2010, 4, 2839, 0.473245540923487)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Mgmt', 2010, 99, 626, 0.104333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Age', 2010, 1, 543, 0.0905150858476413)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Age', 2010, 2, 1930, 0.321773924641547)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Age', 2010, 3, 2231, 0.372081387591728)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Age', 2010, 4, 65, 0.0114942528735632)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Age', 2010, 99, 862, 0.143666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2010, 1, 731, 0.122057104691935)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2010, 2, 118, 0.0196994991652755)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2010, 3, 262, 0.0436885109221277)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2010, 4, 38, 0.00636302746148694)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2010, 5, 22, 0.00394406597346719)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2010, 6, 3377, 0.562927154525754)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2010, 7, 97, 0.0163299663299663)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2010, 99, 986, 0.164333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2010, 1, 43, 0.00722932078009415)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2010, 2, 741, 0.123726832526298)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2010, 3, 768, 0.128021336889482)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2010, 4, 1813, 0.302267422474158)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2010, 5, 1602, 0.267178118745831)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2010, 99, 664, 0.110666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2010, 1, 450, 0.0750375187593797)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2010, 2, 407, 0.0680488212673466)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2010, 3, 836, 0.139356559426571)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2010, 4, 1499, 0.249916638879627)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2010, 5, 856, 0.142904841402337)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2010, 99, 1583, 0.263833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Gender', 2010, 0, 1398, 0.233155436957972)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Gender', 2010, 1, 3718, 0.619769961660277)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Gender', 2010, 99, 513, 0.0855)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Standards', 2013, 0, 28, 0.00496982605608804)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Standards', 2013, 1, 5878, 0.979666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Standards', 2013, 88, 94, 0.0157216925907342)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Training', 2013, 0, 84, 0.0143983544737744)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Training', 2013, 1, 5750, 0.958333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Training', 2013, 88, 166, 0.0280168776371308)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Advice', 2013, 0, 70, 0.0117076434186319)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Advice', 2013, 1, 5721, 0.9535)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Advice', 2013, 88, 209, 0.0352742616033755)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Hotline', 2013, 0, 58, 0.0109454614078128)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Hotline', 2013, 1, 5781, 0.9635)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Hotline', 2013, 88, 161, 0.0269275798628533)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Appraisal', 2013, 0, 182, 0.0304398728884429)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Appraisal', 2013, 1, 5364, 0.894)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Appraisal', 2013, 88, 454, 0.0760469011725293)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Discipline', 2013, 0, 156, 0.0260956841753095)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Discipline', 2013, 1, 5055, 0.8425)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Resources', N'Discipline', 2013, 88, 789, 0.131609674728941)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2013, 0, 231, 0.0390929091216788)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2013, 1, 614, 0.102555536996826)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2013, 2, 850, 0.141690281713619)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2013, 3, 904, 0.150842649758051)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2013, 4, 1738, 0.289666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2013, 77, 1566, 0.261174116077385)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'UseStandards', 2013, 88, 32, 0.00535206556280314)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2013, 1, 164, 0.0274293360093661)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2013, 2, 420, 0.0700583819849875)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2013, 3, 1070, 0.178363060510085)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2013, 4, 2884, 0.480666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2013, 5, 1316, 0.219772879091516)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2013, 88, 62, 0.01053884072752)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2013, 1, 3146, 0.524508169389797)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2013, 2, 854, 0.142333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2013, 3, 848, 0.141356892815469)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2013, 4, 650, 0.108568565224653)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2013, 5, 150, 0.0250710345980277)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'TopMgmt_ID', 2013, 88, 237, 0.0395329441201001)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2013, 1, 36, 0.0064216910453086)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2013, 2, 119, 0.0198498748957465)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2013, 3, 693, 0.115519253208868)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2013, 4, 2886, 0.481)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2013, 5, 1855, 0.309372915276851)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2013, 88, 278, 0.0464960695768523)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2013, 1, 132, 0.022077270446563)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2013, 2, 310, 0.0517097581317765)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2013, 3, 860, 0.143357226204367)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2013, 4, 2807, 0.467833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2013, 5, 1647, 0.274683122081388)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2013, 77, 19, 0.00321979325538044)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2013, 88, 59, 0.010457284650833)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2013, 1, 144, 0.0240440808148272)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2013, 2, 327, 0.0545454545454545)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2013, 3, 1113, 0.185747663551402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2013, 4, 2737, 0.456242707117853)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2013, 5, 1363, 0.227166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2013, 88, 124, 0.0216707444949318)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2013, 1, 111, 0.0185339789614293)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2013, 2, 217, 0.0361968306922435)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2013, 3, 788, 0.131530629277249)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2013, 4, 2713, 0.452242040340057)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2013, 5, 1888, 0.314666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2013, 77, 10, 0.0016946280291476)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2013, 88, 80, 0.0140671707402849)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2013, 1, 37, 0.0063606670104865)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2013, 2, 137, 0.0228523769808173)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2013, 3, 917, 0.152833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2013, 4, 3157, 0.526254375729288)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2013, 5, 1443, 0.240660440293529)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2013, 77, 2, 0.000637348629700446)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2013, 88, 114, 0.0190667335674862)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2013, 1, 19, 0.00338922582946843)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2013, 2, 69, 0.0115095913261051)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2013, 3, 353, 0.0591091761553918)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2013, 4, 2842, 0.473745624270712)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2013, 5, 2400, 0.4)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2013, 77, 1, 0.0007627765064836)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2013, 88, 76, 0.0127111557116575)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2013, 1, 31, 0.00552289328344914)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2013, 2, 121, 0.0201834862385321)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2013, 3, 462, 0.0773610180843938)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2013, 4, 2783, 0.463910651775296)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2013, 5, 2300, 0.383333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2013, 77, 16, 0.00271140484663616)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2013, 88, 46, 0.00769359424652952)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2013, 1, 44, 0.00784873349982162)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2013, 2, 152, 0.0253544620517098)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2013, 3, 948, 0.159274193548387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2013, 4, 2857, 0.476166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2013, 5, 1667, 0.278018679119413)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2013, 77, 1, 0.0007627765064836)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2013, 88, 90, 0.0150526843953838)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2013, 1, 62, 0.0103696270279311)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2013, 2, 170, 0.0284900284900285)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2013, 3, 496, 0.083151718357083)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2013, 4, 2449, 0.408234705784297)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2013, 5, 2263, 0.377166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2013, 77, 1, 0.000588235294117647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2013, 88, 277, 0.0462051709758132)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2013, 1, 41, 0.00685733400234153)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2013, 2, 105, 0.0175967823026647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2013, 3, 412, 0.0690695725062867)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2013, 4, 2581, 0.430238373062177)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2013, 5, 2343, 0.3905)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2013, 77, 13, 0.00228591524529629)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2013, 88, 223, 0.0371976647206005)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2013, 1, 23, 0.00393903065593424)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2013, 2, 83, 0.0139098374392492)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2013, 3, 346, 0.0580050293378039)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2013, 4, 2602, 0.433738956492749)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2013, 5, 2489, 0.414833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2013, 77, 3, 0.000946073793755913)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2013, 88, 172, 0.0286905754795663)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2013, 1, 72, 0.0120421475163071)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2013, 2, 172, 0.0294823448748714)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2013, 3, 792, 0.13255230125523)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2013, 4, 2564, 0.427404567427905)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2013, 5, 1905, 0.3175)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2013, 88, 196, 0.0326939115929942)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2013, 1, 59, 0.0098678708814183)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2013, 2, 130, 0.0217864923747277)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2013, 3, 561, 0.0938912133891213)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2013, 4, 2595, 0.4325)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2013, 5, 2250, 0.375250166777852)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2013, 77, 17, 0.00288086764955092)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupExample', 2013, 88, 89, 0.0148457047539616)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2013, 1, 15, 0.00267570460221192)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2013, 2, 89, 0.0149153678565443)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2013, 3, 729, 0.122008368200837)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2013, 4, 2967, 0.4945)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2013, 5, 1787, 0.298032021347565)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2013, 77, 2, 0.000482741974414675)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2013, 88, 112, 0.0186822351959967)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2013, 1, 40, 0.00688231245698555)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2013, 2, 97, 0.0166552197802198)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2013, 3, 510, 0.0853128136500502)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2013, 4, 2784, 0.464)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2013, 5, 2143, 0.357404936624416)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2013, 88, 95, 0.0158465387823186)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2013, 1, 27, 0.00481026189203634)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2013, 2, 82, 0.0137422490363667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2013, 3, 427, 0.0734055354993983)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2013, 4, 2762, 0.460333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2013, 5, 2309, 0.385090060040027)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2013, 77, 19, 0.00321979325538044)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2013, 88, 43, 0.00717264386989158)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2013, 1, 19, 0.00338922582946843)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2013, 2, 67, 0.0112284229931289)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2013, 3, 641, 0.108076209745405)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2013, 4, 2885, 0.480833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2013, 5, 1976, 0.329553035356905)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2013, 88, 81, 0.013511259382819)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2013, 1, 63, 0.0105580693815988)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2013, 2, 152, 0.0256670043904086)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2013, 3, 821, 0.137520938023451)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2013, 4, 2624, 0.437333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2013, 5, 1822, 0.303869246164109)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2013, 77, 19, 0.00334095305081765)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'SupFeedback', 2013, 88, 156, 0.0260216847372811)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2013, 1, 59, 0.0099847689964461)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2013, 2, 162, 0.0285361986964946)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2013, 3, 696, 0.116641528406234)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2013, 4, 2235, 0.37256209368228)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2013, 5, 1947, 0.3245)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2013, 88, 549, 0.0915763135946622)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'General misconduct ', N'MiscGen', 2013, 0, 4517, 0.752833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'General misconduct ', N'MiscGen', 2013, 1, 642, 0.107214428857715)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'General misconduct ', N'MiscGen', 2013, 88, 477, 0.0797792272955344)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2013, 0, 252, 0.0421404682274247)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2013, 1, 369, 0.061623246492986)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2013, 88, 22, 0.00401973323588526)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2013, 0, 95, 0.0158862876254181)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2013, 1, 116, 0.0197614991482112)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2013, 88, 36, 0.00697269029633934)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2013, 0, 195, 0.0326086956521739)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2013, 1, 39, 0.00794135613927917)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2013, 88, 13, 0.00251791594034476)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2013, 0, 144, 0.0240802675585284)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2013, 1, 85, 0.0146123431321987)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2013, 88, 18, 0.0035650623885918)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2013, 0, 138, 0.0230769230769231)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2013, 1, 90, 0.0153321976149915)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2013, 88, 19, 0.00368003098973465)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2013, 0, 183, 0.0306020066889632)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2013, 1, 53, 0.00911122571772391)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2013, 88, 11, 0.0021305442572148)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2013, 0, 106, 0.0177257525083612)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2013, 1, 125, 0.0212947189097104)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2013, 88, 16, 0.00309897346503971)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2013, 0, 169, 0.0282608695652174)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2013, 1, 48, 0.00822058571673232)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2013, 88, 31, 0.00589914367269267)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2013, 0, 174, 0.0290969899665552)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2013, 1, 60, 0.0106894708711919)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2013, 88, 13, 0.00251791594034476)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2013, 0, 154, 0.0262350936967632)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2013, 1, 71, 0.0118729096989967)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2013, 88, 22, 0.00426108851442959)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2013, 0, 139, 0.0232441471571906)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2013, 1, 75, 0.0129087779690189)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2013, 88, 34, 0.00584493725287949)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2013, 0, 11, 0.00187713310580205)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2013, 1, 349, 0.0582832331329325)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2013, 88, 5, 0.000999600159936026)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2013, 0, 26, 0.00443686006825939)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2013, 1, 326, 0.0544422177688711)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2013, 88, 13, 0.00233770904513577)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2013, 0, 41, 0.00699658703071672)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2013, 1, 286, 0.0477621910487642)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2013, 88, 38, 0.00668190610163531)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2013, 0, 45, 0.00829646017699115)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2013, 1, 284, 0.0474281897127589)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2013, 88, 36, 0.00633022683312819)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2013, 0, 44, 0.00740491417031303)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2013, 1, 276, 0.0460921843687375)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2013, 88, 45, 0.00771340418237916)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2013, 0, 41, 0.00690003365870078)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2013, 1, 283, 0.0472611890447562)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2013, 88, 41, 0.00720942500439599)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2013, 1, 35, 0.00599931436407268)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2013, 2, 39, 0.00656344665095927)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2013, 3, 46, 0.00808862317566379)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2013, 4, 123, 0.020936170212766)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2013, 5, 113, 0.0188710754843019)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2013, 88, 9, 0.00156876416245424)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2013, 0, 28, 0.00524246395806029)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2013, 1, 34, 0.00582790538224203)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2013, 88, 11, 0.00185122854257826)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2013, 0, 20, 0.00374461711290021)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2013, 1, 35, 0.00589027263547627)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2013, 88, 18, 0.00351425224521671)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2013, 0, 43, 0.00807057057057057)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2013, 1, 14, 0.00235610905419051)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2013, 88, 16, 0.00287718036324402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2013, 0, 19, 0.00325677065478231)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2013, 1, 39, 0.0070131271354073)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2013, 88, 15, 0.00252440255806126)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2013, 0, 13, 0.00222831676379842)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2013, 1, 37, 0.0066534795900018)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2013, 88, 23, 0.00387075058902726)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2013, 0, 230, 0.0387075058902726)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2013, 1, 77, 0.0128590514362057)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2013, 88, 56, 0.00955631399317406)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2013, 0, 36, 0.00641711229946524)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2013, 1, 32, 0.0053440213760855)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2013, 88, 7, 0.00124955373081042)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2013, 0, 31, 0.00552584670231729)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2013, 1, 38, 0.00634602538410154)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2013, 88, 6, 0.00102845389098389)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2013, 0, 38, 0.00651354130956462)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2013, 1, 30, 0.00501002004008016)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2013, 88, 7, 0.00124955373081042)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2013, 0, 37, 0.00659536541889483)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2013, 1, 30, 0.00501002004008016)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2013, 88, 8, 0.00142806140664049)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2013, 0, 48, 0.0082276311278711)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2013, 1, 23, 0.00384101536406146)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2013, 88, 4, 0.000810536980749747)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2013, 0, 35, 0.00599931436407268)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2013, 1, 36, 0.00601202404809619)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2013, 88, 4, 0.000810536980749747)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2013, 0, 44, 0.00784313725490196)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2013, 1, 25, 0.0041750167000668)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2013, 88, 6, 0.00102845389098389)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2013, 0, 43, 0.00766488413547237)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2013, 1, 24, 0.00400801603206413)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2013, 88, 8, 0.00137127185464518)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2013, 0, 40, 0.0071301247771836)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2013, 1, 25, 0.0041750167000668)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2013, 88, 10, 0.00171408981830648)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2013, 0, 29, 0.00593775593775594)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2013, 1, 36, 0.00601202404809619)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2013, 88, 10, 0.00171408981830648)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSexHarass', 2013, 0, 5251, 0.875166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSexHarass', 2013, 1, 89, 0.0148630594522378)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSexHarass', 2013, 88, 209, 0.0348623853211009)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOfferKickbacks', 2013, 0, 5208, 0.868)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOfferKickbacks', 2013, 1, 58, 0.0100276625172891)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOfferKickbacks', 2013, 88, 283, 0.0472060050041701)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAbuse', 2013, 0, 4733, 0.788833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAbuse', 2013, 1, 529, 0.0884763338350895)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAbuse', 2013, 88, 287, 0.0478732276897414)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsDiscrim', 2013, 0, 5116, 0.852666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsDiscrim', 2013, 1, 191, 0.0321440592393134)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsDiscrim', 2013, 88, 242, 0.0403669724770642)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsHealthViol', 2013, 0, 5043, 0.8405)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsHealthViol', 2013, 1, 260, 0.0434056761268781)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsHealthViol', 2013, 88, 246, 0.0410341951626355)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConflictInterest', 2013, 0, 4890, 0.815)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConflictInterest', 2013, 1, 274, 0.0458117371676977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConflictInterest', 2013, 88, 385, 0.0642201834862385)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsFailProfStandards', 2013, 0, 4987, 0.831166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsFailProfStandards', 2013, 1, 189, 0.0316106372303061)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsFailProfStandards', 2013, 88, 373, 0.0622185154295246)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAlterDocs', 2013, 0, 5105, 0.850833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAlterDocs', 2013, 1, 123, 0.0212729159460394)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAlterDocs', 2013, 88, 321, 0.0535446205170976)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCompetInfo', 2013, 0, 5157, 0.8595)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCompetInfo', 2013, 1, 45, 0.00823120541430401)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCompetInfo', 2013, 88, 347, 0.0578815679733111)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConfInfo', 2013, 0, 5170, 0.861666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConfInfo', 2013, 1, 54, 0.00922919159118099)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsConfInfo', 2013, 88, 325, 0.0542118432026689)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperHire', 2013, 0, 4981, 0.830166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperHire', 2013, 1, 144, 0.0245315161839864)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperHire', 2013, 88, 424, 0.0707256046705588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2013, 0, 5074, 0.845666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2013, 1, 55, 0.00940010254657324)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2013, 88, 420, 0.0700583819849875)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieExternal', 2013, 0, 4955, 0.825833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieExternal', 2013, 1, 177, 0.031828807768387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieExternal', 2013, 88, 417, 0.069557964970809)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieEmployees', 2013, 0, 4764, 0.794)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieEmployees', 2013, 1, 298, 0.0498411105536043)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsLieEmployees', 2013, 88, 487, 0.0812343619683069)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsTimeCheat', 2013, 0, 4978, 0.829666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsTimeCheat', 2013, 1, 206, 0.0344539220605452)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsTimeCheat', 2013, 88, 365, 0.060884070058382)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsStealing', 2013, 0, 5187, 0.8645)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsStealing', 2013, 1, 84, 0.0143565202529482)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsStealing', 2013, 88, 278, 0.046371976647206)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsInternetAbuse', 2013, 0, 5039, 0.839833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsInternetAbuse', 2013, 1, 151, 0.0255369524775917)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsInternetAbuse', 2013, 88, 359, 0.059883236030025)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEnvViol', 2013, 0, 5202, 0.867)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEnvViol', 2013, 1, 46, 0.00841412109017743)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEnvViol', 2013, 88, 301, 0.050208507089241)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplBenViols', 2013, 0, 4946, 0.824333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplBenViols', 2013, 1, 190, 0.0317460317460317)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplBenViols', 2013, 88, 413, 0.0688907422852377)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2013, 0, 5158, 0.859666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2013, 1, 30, 0.00548747027620267)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2013, 88, 361, 0.0602168473728107)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2013, 0, 5102, 0.850333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2013, 1, 77, 0.0131175468483816)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2013, 88, 370, 0.0617180984153461)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2013, 0, 4890, 0.815)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2013, 1, 276, 0.0464411913175164)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2013, 88, 383, 0.0638865721434529)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2013, 0, 5224, 0.870666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2013, 1, 62, 0.0113407719041522)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2013, 88, 263, 0.0438698915763136)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAntiCompPract', 2013, 0, 5165, 0.860833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAntiCompPract', 2013, 1, 26, 0.00475580757270898)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsAntiCompPract', 2013, 88, 358, 0.0597164303586322)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperContracts', 2013, 0, 5013, 0.8355)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperContracts', 2013, 1, 97, 0.0164909894593676)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsImproperContracts', 2013, 88, 439, 0.0732276897414512)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsContractViols', 2013, 0, 5064, 0.844)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsContractViols', 2013, 1, 73, 0.0127466387288284)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsContractViols', 2013, 88, 412, 0.0687239366138449)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOther', 2013, 0, 4530, 0.755)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOther', 2013, 1, 60, 0.0106951871657754)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific misconduct questions', N'ObsOther', 2013, 88, 957, 0.159633027522936)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Pressure', N'Pressure', 2013, 0, 5155, 0.859166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Pressure', N'Pressure', 2013, 1, 192, 0.032064128256513)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Pressure', N'Pressure', 2013, 88, 185, 0.0308590492076731)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressureFreq', 2013, 1, 54, 0.00927197802197802)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressureFreq', 2013, 2, 60, 0.0104766893661603)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressureFreq', 2013, 3, 44, 0.0073738897268309)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressureFreq', 2013, 4, 26, 0.00434201736806947)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressureFreq', 2013, 88, 8, 0.00166389351081531)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2013, 0, 867, 0.144668780243618)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2013, 1, 874, 0.145715238412804)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2013, 2, 2352, 0.392)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2013, 3, 1275, 0.212677231025855)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2013, 88, 156, 0.0261701056869653)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceKeepJob', 2013, 0, 1846, 0.307974641307975)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceKeepJob', 2013, 1, 1283, 0.213833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceKeepJob', 2013, 2, 1591, 0.26521086847808)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceKeepJob', 2013, 3, 652, 0.109048335842114)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceKeepJob', 2013, 88, 152, 0.025520483546004)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2013, 0, 2179, 0.363166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2013, 1, 1232, 0.2054018006002)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2013, 2, 1252, 0.208701450241707)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2013, 3, 415, 0.0693863902357465)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2013, 88, 446, 0.0748824714573539)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2013, 0, 1761, 0.2935)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2013, 1, 1578, 0.263043840640107)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2013, 2, 1582, 0.263798565949641)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2013, 3, 444, 0.0740617180984153)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2013, 88, 159, 0.0266957689724647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceMyCareer', 2013, 0, 1957, 0.326166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceMyCareer', 2013, 1, 1290, 0.215143428952635)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceMyCareer', 2013, 2, 1483, 0.247290311822578)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceMyCareer', 2013, 3, 519, 0.0865721434528774)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceMyCareer', 2013, 88, 275, 0.0460868107926931)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceExtDemands', 2013, 0, 2558, 0.426333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceExtDemands', 2013, 1, 949, 0.158272181454303)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceExtDemands', 2013, 2, 893, 0.149032042723632)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceExtDemands', 2013, 3, 274, 0.0457047539616347)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Specific Types of Pressure', N'PressSourceExtDemands', 2013, 88, 850, 0.141737535434384)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'IrregularSituations', 2013, 0, 3815, 0.635833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'IrregularSituations', 2013, 1, 1170, 0.19568489713999)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'IrregularSituations', 2013, 88, 535, 0.0892410341951626)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2013, 1, 20, 0.00493461633358006)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2013, 2, 87, 0.014512093411176)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2013, 3, 655, 0.109678499665104)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2013, 4, 3054, 0.509)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2013, 5, 1497, 0.249666444296197)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'Prepared', 2013, 88, 204, 0.0340510766149224)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2013, 1, 69, 0.0118719889883001)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2013, 2, 222, 0.0373046546798857)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2013, 3, 1193, 0.199631860776439)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2013, 4, 2628, 0.438)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2013, 5, 990, 0.165110073382255)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'SocietyEffects', 2013, 88, 412, 0.0687239366138449)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2013, 1, 52, 0.00891173950299914)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2013, 2, 181, 0.0308242506811989)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2013, 3, 1029, 0.172217573221757)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2013, 4, 2745, 0.4575)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2013, 5, 1145, 0.190960640426951)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EnvEffects', 2013, 88, 362, 0.0603836530442035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2013, 1, 193, 0.0322796454256565)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2013, 2, 432, 0.0723012552301255)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2013, 3, 1143, 0.190595297648824)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2013, 4, 2475, 0.4125)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2013, 5, 1003, 0.167278185456971)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'EmpEffects', 2013, 88, 268, 0.0447039199332777)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2013, 1, 162, 0.0270948319116909)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2013, 2, 339, 0.05678391959799)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2013, 3, 1697, 0.282974820743705)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2013, 4, 1974, 0.329054842473746)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2013, 5, 795, 0.1325)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Company considers effect on questions', N'FutureGenEffects', 2013, 88, 547, 0.0912427022518766)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2013, 1, 9, 0.00226472068444892)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2013, 2, 18, 0.0032108455226543)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2013, 3, 173, 0.0289685197588748)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2013, 4, 2264, 0.377396232705451)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2013, 5, 2990, 0.498333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsLaw', 2013, 88, 59, 0.00984153461217681)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2013, 1, 15, 0.00362056480811007)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2013, 2, 41, 0.00705437026841018)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2013, 3, 260, 0.043398430979803)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2013, 4, 2254, 0.375729288214702)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2013, 5, 2883, 0.4805)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'EthicsValues', 2013, 88, 60, 0.0100083402835696)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2013, 1, 56, 0.0106060606060606)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2013, 2, 172, 0.0286905754795663)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2013, 3, 753, 0.125667556742323)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2013, 4, 2907, 0.484580763460577)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2013, 5, 1596, 0.266)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'Satisfied', 2013, 88, 28, 0.00570148645896966)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2013, 1, 129, 0.0215755143000502)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2013, 2, 529, 0.0882402001668057)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2013, 3, 1743, 0.29088785046729)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2013, 4, 1773, 0.2955)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2013, 5, 715, 0.119285952619286)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardEthics', 2013, 88, 620, 0.103488566182607)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2013, 1, 1431, 0.238738738738739)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2013, 2, 1867, 0.311166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2013, 3, 835, 0.139189864977496)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2013, 4, 570, 0.0950792326939116)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2013, 5, 166, 0.0277221108884436)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2013, 77, 27, 0.00457549567869853)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'SupRewardResults', 2013, 88, 613, 0.102217775554444)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2013, 1, 1298, 0.21654988321655)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2013, 2, 2072, 0.345333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2013, 3, 922, 0.153692282047008)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2013, 4, 437, 0.0728940783986656)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2013, 5, 111, 0.0185370741482966)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Effectivness of Program', N'RewardResults', 2013, 88, 666, 0.111055527763882)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2013, 1, 1223, 0.203833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2013, 2, 2146, 0.357785928642881)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2013, 3, 925, 0.154192365394232)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2013, 4, 484, 0.0810991957104558)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2013, 5, 117, 0.0195390781563126)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'PeerShowRespResults', 2013, 88, 610, 0.101921470342523)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2013, 1, 36, 0.00607492406344921)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2013, 2, 126, 0.0210175145954962)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2013, 3, 578, 0.0967040321231387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2013, 4, 3010, 0.501666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2013, 5, 1301, 0.21705038371705)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'DealsFairly', 2013, 88, 452, 0.0754465030879653)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'ValuesConflict', 2013, 0, 2025, 0.337837837837838)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'ValuesConflict', 2013, 1, 2182, 0.363666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'ValuesConflict', 2013, 2, 1039, 0.173282188125417)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'ValuesConflict', 2013, 3, 99, 0.0165330661322645)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Additional Questions', N'ValuesConflict', 2013, 88, 157, 0.0261884904086739)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2013, 1, 440, 0.0734067400734067)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2013, 2, 736, 0.122809944935758)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2013, 3, 931, 0.155373831775701)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2013, 4, 1271, 0.21268406961178)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2013, 5, 1791, 0.2985)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Tenure', 2013, 99, 330, 0.055267124434768)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Mgmt', 2013, 1, 233, 0.0391925988225399)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Mgmt', 2013, 2, 1167, 0.19511787326534)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Mgmt', 2013, 3, 907, 0.151166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Mgmt', 2013, 4, 2488, 0.414735789298216)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Mgmt', 2013, 99, 703, 0.117854149203688)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Age', 2013, 1, 697, 0.116282949616283)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Age', 2013, 2, 2333, 0.388833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Age', 2013, 3, 1641, 0.273591197065689)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Age', 2013, 4, 42, 0.00723016009640213)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Age', 2013, 99, 784, 0.130775646371977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2013, 1, 1676, 0.279379896649442)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2013, 2, 80, 0.0135639199728722)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2013, 3, 179, 0.0301346801346801)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2013, 4, 28, 0.00471936625653127)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2013, 5, 8, 0.001643723032669)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2013, 6, 2470, 0.41387399463807)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2013, 7, 163, 0.0271757252417473)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Ethnicity', 2013, 99, 893, 0.148833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2013, 1, 85, 0.0150869719559815)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2013, 2, 812, 0.135378459486495)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2013, 3, 933, 0.1555)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2013, 4, 1826, 0.304485576121394)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2013, 5, 1076, 0.180234505862647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Education', 2013, 99, 763, 0.127293960627294)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2013, 1, 1057, 0.176196032672112)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2013, 2, 580, 0.0968118844934068)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2013, 3, 755, 0.126592890677398)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2013, 4, 866, 0.145131556896263)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2013, 5, 631, 0.105872483221477)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Salary', 2013, 99, 1604, 0.267333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Gender', 2013, 1, 3328, 0.554944138736035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Gender', 2013, 2, 1298, 0.216333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Participant Demographic Variables', N'Gender', 2013, 99, 865, 0.144503842298697)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2014, 1, 15, 0.00630782169890664)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2014, 2, 50, 0.0189250567751703)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2014, 3, 168, 0.0638297872340425)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2014, 4, 1205, 0.454374057315234)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2014, 5, 1160, 0.43757072802716)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2014, 88, 54, 0.0209871745044695)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2014, 1, 61, 0.0251235584843493)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2014, 2, 96, 0.037037037037037)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2014, 3, 240, 0.091150778579567)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2014, 4, 981, 0.369909502262443)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2014, 5, 1252, 0.472274613353451)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2014, 88, 22, 0.00844529750479846)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2014, 1, 21, 0.0088309503784693)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2014, 2, 64, 0.024224072672218)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2014, 3, 225, 0.0854863221884498)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2014, 4, 1287, 0.485294117647059)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2014, 5, 1023, 0.386037735849057)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2014, 88, 32, 0.0126732673267327)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2014, 1, 29, 0.0119439868204283)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2014, 2, 65, 0.0246960486322188)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2014, 3, 246, 0.0932524639878696)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2014, 4, 1031, 0.388763197586727)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2014, 5, 1262, 0.47622641509434)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2014, 88, 16, 0.00687580575848732)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2014, 1, 33, 0.0140905209222886)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2014, 2, 62, 0.0245739199365834)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2014, 3, 182, 0.0691226737561717)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2014, 4, 1077, 0.406108597285068)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2014, 5, 1227, 0.462844209732177)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2014, 88, 67, 0.0254559270516717)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2014, 1, 35, 0.0135030864197531)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2014, 2, 49, 0.0198059822150364)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2014, 3, 212, 0.0805165210786176)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2014, 4, 969, 0.365384615384615)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2014, 5, 1358, 0.512259524707657)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupExample', 2014, 88, 23, 0.00952380952380952)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2014, 1, 20, 0.00823723228995058)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2014, 2, 32, 0.0150730098916627)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2014, 3, 191, 0.0725408279529054)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2014, 4, 1064, 0.401206636500754)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2014, 5, 1304, 0.491889852885704)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2014, 88, 25, 0.00959692898272553)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2014, 1, 11, 0.00462573591253154)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2014, 2, 26, 0.0111924235901851)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2014, 3, 261, 0.0991264717052791)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2014, 4, 1157, 0.436274509803922)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2014, 5, 1143, 0.431320754716981)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2014, 88, 38, 0.0145873320537428)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2014, 1, 39, 0.0150462962962963)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2014, 2, 92, 0.0348749052312358)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2014, 3, 483, 0.18247072157159)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2014, 4, 1033, 0.389517345399698)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2014, 5, 888, 0.335094339622642)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Effectivness of Program', N'SupFeedback', 2014, 88, 99, 0.0374432677760968)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'General misconduct ', N'MiscGen', 2014, 0, 2160, 0.81447963800905)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'General misconduct ', N'MiscGen', 2014, 1, 276, 0.10446631339894)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'General misconduct ', N'MiscGen', 2014, 88, 197, 0.0751048417842165)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2014, 0, 93, 0.0352006056018168)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2014, 1, 174, 0.0689655172413793)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2014, 88, 10, 0.00388802488335925)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2014, 0, 6, 0.00253057781526782)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2014, 1, 76, 0.0287660862982589)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2014, 88, 10, 0.00413393964448119)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2014, 0, 75, 0.0313021702838063)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2014, 1, 14, 0.00529901589704769)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2014, 88, 3, 0.0016025641025641)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2014, 0, 51, 0.0193035579106737)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2014, 1, 34, 0.0129474485910129)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2014, 88, 7, 0.00289375775113683)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2014, 0, 60, 0.0231481481481482)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2014, 1, 26, 0.00984102952308857)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2014, 88, 6, 0.00269420745397396)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2014, 0, 23, 0.00970054829185998)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2014, 1, 63, 0.0238455715367146)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2014, 88, 6, 0.00320512820512821)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2014, 0, 55, 0.0212191358024691)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2014, 1, 25, 0.00952018278750952)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2014, 88, 12, 0.00454201362604088)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepIResolve', 2014, 0, 79, 0.0299015897047691)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepIResolve', 2014, 1, 10, 0.00380807311500381)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepIResolve', 2014, 88, 3, 0.00150451354062187)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2014, 0, 53, 0.0200605601816805)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2014, 1, 28, 0.0106626047220107)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2014, 88, 11, 0.00486080424215643)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2014, 0, 63, 0.0238455715367146)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2014, 1, 10, 0.00380807311500381)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2014, 88, 19, 0.00839593460008838)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2014, 1, 26, 0.0109519797809604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2014, 2, 48, 0.0197693574958814)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2014, 3, 34, 0.0139230139230139)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2014, 4, 38, 0.0150614347998415)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2014, 5, 23, 0.00922953451043339)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2014, 88, 5, 0.00212134068731438)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2014, 0, 26, 0.0109519797809604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2014, 1, 35, 0.0144151565074135)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2014, 88, 12, 0.00504625735912532)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2014, 0, 13, 0.00546677880571909)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2014, 1, 33, 0.0135914332784185)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2014, 88, 27, 0.0117852466171977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2014, 0, 43, 0.0177100494233937)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2014, 1, 16, 0.00698384984722828)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2014, 88, 14, 0.00612423447069116)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2014, 0, 12, 0.0050547598989048)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2014, 1, 37, 0.0152388797364086)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2014, 88, 24, 0.0105355575065847)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2014, 0, 19, 0.00798990748528175)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2014, 1, 29, 0.0119439868204283)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2014, 88, 25, 0.0109361329833771)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2014, 0, 118, 0.0467697185889814)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2014, 1, 26, 0.0107084019769358)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2014, 88, 29, 0.0127304653204565)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Pressure', N'Pressure', 2014, 0, 2475, 0.933257918552036)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Pressure', N'Pressure', 2014, 1, 86, 0.0357588357588358)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Pressure', N'Pressure', 2014, 88, 68, 0.0259245139153641)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Participant Demographic Variables', N'Tenure', 2014, 1, 384, 0.144796380090498)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Participant Demographic Variables', N'Tenure', 2014, 2, 483, 0.210274270787984)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Participant Demographic Variables', N'Tenure', 2014, 3, 361, 0.194819212088505)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company2', N'Participant Demographic Variables', N'Tenure', 2014, 4, 1424, 0.943671305500331)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Standards', 2017, 0, 39, 0.0065989847715736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Standards', 2017, 1, 5884, 0.980666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Standards', 2017, 88, 77, 0.0130420054200542)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Training', 2017, 0, 104, 0.017514314584035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Training', 2017, 1, 5725, 0.954166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Training', 2017, 88, 171, 0.0285809794417516)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Advice', 2017, 0, 78, 0.0131357359380263)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Advice', 2017, 1, 5646, 0.941)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Advice', 2017, 88, 276, 0.0461847389558233)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Hotline', 2017, 0, 81, 0.0136409565510273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Hotline', 2017, 1, 5756, 0.959333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Hotline', 2017, 88, 163, 0.027339818852734)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Appraisal', 2017, 0, 338, 0.0564933979608892)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Appraisal', 2017, 1, 5104, 0.850666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Appraisal', 2017, 88, 558, 0.0933110367892977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Discipline', 2017, 0, 165, 0.027661357921207)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Discipline', 2017, 1, 4780, 0.796666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Discipline', 2017, 88, 1055, 0.175950633755837)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 0, 287, 0.0478333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 1, 704, 0.117411607738492)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 2, 762, 0.127021170195033)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 3, 947, 0.157885961987329)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 4, 1677, 0.279733110925771)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 77, 1599, 0.266633316658329)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 88, 24, 0.00407608695652174)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 1, 264, 0.0440513932921742)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 2, 566, 0.0943333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 3, 1209, 0.20163442294863201)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 4, 2869, 0.478246374395733)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 5, 1049, 0.174920793730198)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 88, 32, 0.0054412514878422)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 1, 3167, 0.527833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 2, 518, 0.0865497076023392)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 3, 1413, 0.235735735735736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 4, 502, 0.0837085209271302)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 5, 171, 0.028566655529569)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 88, 202, 0.0346246143297909)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 1, 43, 0.00728443164492631)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 2, 162, 0.0270315367929251)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 3, 855, 0.142904897208758)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 4, 3136, 0.522840946982327)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 5, 1405, 0.234205700950158)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 88, 362, 0.0603333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 1, 202, 0.033689126084056)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 2, 415, 0.0692243536280234)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 3, 937, 0.156453498079813)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 4, 2931, 0.4885)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 5, 1386, 0.231038506417736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 77, 23, 0.00388973448334179)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 88, 40, 0.0070459749867888)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 1, 261, 0.0435508092774904)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 2, 558, 0.093)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 3, 1381, 0.23032021347565)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 4, 2705, 0.450908484747458)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 5, 904, 0.150716905635212)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 88, 108, 0.0180360721442886)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 1, 150, 0.0250292007341899)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 2, 317, 0.0531433361274099)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 3, 839, 0.139926617745163)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 4, 2858, 0.476333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 5, 1674, 0.279093031010337)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 77, 16, 0.00270590224928124)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 88, 63, 0.0105210420841683)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 1, 40, 0.0066744535291173)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 2, 207, 0.0346038114343029)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 3, 1025, 0.170947298198799)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 4, 3386, 0.564333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 5, 1152, 0.192096048024012)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 88, 107, 0.0178690714762859)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 1, 54, 0.00954232196501149)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 2, 121, 0.0201666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 3, 424, 0.0707492074086434)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 4, 3111, 0.518586431071845)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 5, 2115, 0.352852852852853)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 88, 62, 0.0103540414161657)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 1, 57, 0.00989755165827401)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 2, 183, 0.0306532663316583)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 3, 582, 0.097)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 4, 3004, 0.500750125020837)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 5, 2001, 0.333833833833834)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 77, 15, 0.00253678335870117)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 88, 45, 0.00778008298755187)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 1, 40, 0.00695531211963137)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 2, 254, 0.0425460636515913)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 3, 1238, 0.206470980653769)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 4, 3006, 0.501)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 5, 1251, 0.208882952078811)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 77, 2, 0.000342759211653813)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 88, 96, 0.0160669456066946)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 1, 127, 0.0211843202668891)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 2, 288, 0.0481363864282133)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 3, 659, 0.109961621892208)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 4, 2489, 0.414833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 5, 1972, 0.328995662328996)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 88, 327, 0.0545363575717145)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 1, 71, 0.0123007623007623)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 2, 142, 0.0240514905149051)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 3, 475, 0.0792194796531021)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 4, 2779, 0.463166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 5, 2150, 0.358632193494579)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 77, 14, 0.00236766446812109)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 88, 231, 0.0385771543086172)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 1, 33, 0.0055045871559633)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 2, 101, 0.0168333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 3, 362, 0.0603735823882588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 4, 2770, 0.461743623937323)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 5, 2445, 0.407907907907908)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 88, 151, 0.0252171008684035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 1, 140, 0.0233527939949958)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 2, 298, 0.0500503862949278)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 3, 1015, 0.169166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 4, 2753, 0.45890981830305)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 5, 1452, 0.242080693564521)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 88, 179, 0.0298931195724783)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 1, 88, 0.0146837977640581)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 2, 169, 0.0283842794759825)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 3, 581, 0.0968979319546364)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 4, 2877, 0.4795)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 5, 2044, 0.340780260086696)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 77, 16, 0.00270590224928124)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 88, 62, 0.0103540414161657)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 1, 29, 0.00484140233722871)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 2, 124, 0.0206908059402636)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 3, 789, 0.1315)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 4, 3337, 0.55625937656276)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 5, 1484, 0.247580914247581)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 77, 1, 0.000261301280376274)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 88, 73, 0.0123519458544839)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 1, 64, 0.0106791256465877)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 2, 122, 0.0203502919099249)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 3, 744, 0.124)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 4, 3063, 0.510755377688844)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 5, 1682, 0.280380063343891)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 88, 123, 0.020682697158231)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 1, 52, 0.00867678958785249)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 2, 89, 0.0149203688181056)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 3, 518, 0.0863909272848566)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 4, 3025, 0.504166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 5, 2053, 0.342223703950658)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 77, 16, 0.00270590224928124)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 88, 45, 0.00768705158865733)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 1, 18, 0.00300350408810279)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 2, 96, 0.016)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 3, 841, 0.140260173448966)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 4, 3188, 0.531599132899783)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 5, 1581, 0.263543923987331)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 77, 3, 0.00051413881748072)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 88, 71, 0.0118570474281897)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 1, 83, 0.0140892887455441)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 2, 240, 0.0400266844563042)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 3, 1139, 0.190055064241615)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 4, 2688, 0.448074679113186)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 5, 1436, 0.239413137712571)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 77, 19, 0.00321325892102148)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 88, 179, 0.0298333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 1, 75, 0.0125104253544621)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 2, 229, 0.0382751128196557)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 3, 919, 0.153268845897265)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 4, 2259, 0.3765)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 5, 1403, 0.234262815161129)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 88, 890, 0.148481815148482)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'General misconduct ', N'MiscGen', 2017, 0, 4415, 0.735955992665444)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'General misconduct ', N'MiscGen', 2017, 1, 908, 0.151333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'General misconduct ', N'MiscGen', 2017, 88, 440, 0.0734189888202903)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2017, 0, 399, 0.0667558976074954)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2017, 1, 478, 0.0796666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2017, 88, 30, 0.00502933780385583)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2017, 0, 94, 0.0158596254428885)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2017, 1, 265, 0.0443366237242764)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2017, 88, 37, 0.00644487023166696)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2017, 0, 319, 0.0534159410582719)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2017, 1, 46, 0.00769616864647817)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2017, 88, 31, 0.00540352100400906)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2017, 0, 181, 0.0303081044876088)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2017, 1, 170, 0.0284423623891584)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2017, 88, 45, 0.00759237388223384)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2017, 0, 239, 0.040020093770931)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2017, 1, 129, 0.0215827338129496)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2017, 88, 28, 0.0047241437489455)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2017, 0, 296, 0.0495231721599465)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2017, 1, 75, 0.0127312850110338)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2017, 88, 25, 0.00421798549012991)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2017, 0, 133, 0.022274325908558)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2017, 1, 225, 0.0376758204956463)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2017, 88, 38, 0.00635770453404718)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2017, 0, 288, 0.0482250502344273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2017, 1, 68, 0.0115430317433373)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2017, 88, 40, 0.00669232056215493)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2017, 0, 291, 0.0486866320896771)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2017, 1, 83, 0.0146798726565264)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2017, 88, 22, 0.00383208500261279)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2017, 0, 280, 0.0468854655056932)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2017, 1, 66, 0.0118983234180638)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2017, 88, 50, 0.00836540070269366)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2017, 0, 266, 0.0445411922304086)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2017, 1, 73, 0.0123875784829459)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2017, 88, 57, 0.00953655680107077)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2017, 0, 14, 0.00233333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2017, 1, 457, 0.0762301918265221)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2017, 88, 4, 0.00107642626480086)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2017, 0, 32, 0.00533333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2017, 1, 403, 0.0673575129533679)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2017, 88, 40, 0.00671817265703729)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2017, 0, 87, 0.0145)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2017, 1, 311, 0.0521112600536193)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2017, 88, 77, 0.0132530120481928)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2017, 0, 87, 0.0150623268698061)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2017, 1, 340, 0.0566666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2017, 88, 48, 0.00806180718844474)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2017, 0, 88, 0.0150401640745172)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2017, 1, 311, 0.0518333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2017, 88, 76, 0.0132635253054101)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2017, 0, 63, 0.0105)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2017, 1, 360, 0.0601704830352666)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2017, 88, 52, 0.00919377652050919)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 1, 73, 0.012341504649197)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 2, 87, 0.0145412000668561)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 3, 76, 0.0126666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 4, 142, 0.0237935656836461)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 5, 78, 0.0131246845027764)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 88, 19, 0.00369218810726778)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2017, 0, 46, 0.00778210116731518)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2017, 1, 94, 0.0158918005071851)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2017, 88, 20, 0.00334280461307037)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2017, 0, 32, 0.00541363559465404)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2017, 1, 65, 0.010989010989011)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2017, 88, 63, 0.0105298345311717)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2017, 0, 94, 0.0158918005071851)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2017, 1, 23, 0.00389105058365759)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2017, 88, 43, 0.00718702991810129)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2017, 0, 29, 0.00490610725765522)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2017, 1, 92, 0.0153769012201237)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2017, 88, 39, 0.00675207756232687)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2017, 0, 32, 0.00550964187327824)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2017, 1, 67, 0.0111983954537857)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2017, 88, 61, 0.0105609418282548)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2017, 0, 322, 0.0536666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2017, 1, 82, 0.0137977452465085)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2017, 88, 69, 0.0118801652892562)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2017, 0, 43, 0.00746657405799618)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2017, 1, 32, 0.00541363559465404)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2017, 88, 5, 0.000841325929665152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2017, 0, 33, 0.00575916230366492)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2017, 1, 41, 0.00693622060565048)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2017, 88, 6, 0.00100959111559818)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2017, 0, 47, 0.00812586445366528)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2017, 1, 28, 0.00473693114532228)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2017, 88, 5, 0.000841325929665152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2017, 0, 52, 0.0090293453724605)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2017, 1, 23, 0.00389105058365759)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2017, 88, 5, 0.000841325929665152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2017, 0, 69, 0.0116731517509728)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2017, 1, 6, 0.00119856172592889)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2017, 88, 5, 0.000841325929665152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2017, 0, 44, 0.00767888307155323)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2017, 1, 28, 0.00473693114532228)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2017, 88, 8, 0.00134612148746424)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2017, 0, 45, 0.00761292505498224)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2017, 1, 28, 0.00486195520055565)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2017, 88, 7, 0.00117785630153121)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2017, 0, 58, 0.00981221451531044)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2017, 1, 15, 0.00270660411403825)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2017, 88, 7, 0.00117785630153121)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2017, 0, 49, 0.00828962950431399)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2017, 1, 22, 0.00382010765757944)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2017, 88, 9, 0.00151438667339727)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2017, 0, 32, 0.00541363559465404)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2017, 1, 31, 0.00535961272475795)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2017, 88, 17, 0.00286050816086152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSexHarass', 2017, 0, 5269, 0.878313052175363)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSexHarass', 2017, 1, 241, 0.0401666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSexHarass', 2017, 88, 183, 0.0305356248957117)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOfferKickbacks', 2017, 0, 5354, 0.892333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOfferKickbacks', 2017, 1, 89, 0.0148531375166889)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOfferKickbacks', 2017, 88, 250, 0.0417153345569831)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAbuse', 2017, 0, 4433, 0.738956492748791)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAbuse', 2017, 1, 981, 0.1635)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAbuse', 2017, 88, 279, 0.0466555183946488)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsDiscrim', 2017, 0, 5023, 0.837166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsDiscrim', 2017, 1, 397, 0.0663657639585423)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsDiscrim', 2017, 88, 273, 0.0455531453362256)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsHealthViol', 2017, 0, 5020, 0.836945648549516)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsHealthViol', 2017, 1, 461, 0.0768333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsHealthViol', 2017, 88, 212, 0.0354041416165665)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConflictInterest', 2017, 0, 4751, 0.791833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConflictInterest', 2017, 1, 547, 0.0912731520106791)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConflictInterest', 2017, 88, 395, 0.0658772515010007)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsFailProfStandards', 2017, 0, 4929, 0.8215)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsFailProfStandards', 2017, 1, 370, 0.0628076727211)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsFailProfStandards', 2017, 88, 394, 0.0657104736490994)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAlterDocs', 2017, 0, 5185, 0.864166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAlterDocs', 2017, 1, 209, 0.0349323082065853)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAlterDocs', 2017, 88, 299, 0.0498915401301518)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCompetInfo', 2017, 0, 5280, 0.88)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCompetInfo', 2017, 1, 65, 0.0108841259209645)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCompetInfo', 2017, 88, 348, 0.0580677457033205)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConfInfo', 2017, 0, 5323, 0.887166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConfInfo', 2017, 1, 71, 0.0118967828418231)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConfInfo', 2017, 88, 299, 0.0498915401301518)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperHire', 2017, 0, 4959, 0.8265)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperHire', 2017, 1, 275, 0.0458868680126815)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperHire', 2017, 88, 459, 0.0765765765765766)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2017, 0, 5170, 0.861666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2017, 1, 96, 0.0160454621427378)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2017, 88, 427, 0.0712497914233272)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieExternal', 2017, 0, 5028, 0.838)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieExternal', 2017, 1, 250, 0.0417850576633796)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieExternal', 2017, 88, 415, 0.069247455364592)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieEmployees', 2017, 0, 4651, 0.775166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieEmployees', 2017, 1, 508, 0.0847372810675563)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieEmployees', 2017, 88, 534, 0.0891783567134269)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsTimeCheat', 2017, 0, 5004, 0.834)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsTimeCheat', 2017, 1, 340, 0.0567328549974971)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsTimeCheat', 2017, 88, 348, 0.0580386924616411)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsStealing', 2017, 0, 5330, 0.888333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsStealing', 2017, 1, 125, 0.0210650488709134)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsStealing', 2017, 88, 238, 0.039712998498248)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsInternetAbuse', 2017, 0, 4925, 0.820970161693615)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsInternetAbuse', 2017, 1, 412, 0.0686666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsInternetAbuse', 2017, 88, 356, 0.0593729152768512)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEnvViol', 2017, 0, 5347, 0.891166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEnvViol', 2017, 1, 74, 0.0123683770683604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEnvViol', 2017, 88, 272, 0.0453862839979977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplBenViols', 2017, 0, 5011, 0.835166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplBenViols', 2017, 1, 253, 0.0422159185716669)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplBenViols', 2017, 88, 429, 0.0715476984656438)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2017, 0, 5342, 0.890333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2017, 1, 37, 0.00618418853418018)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2017, 88, 314, 0.0523944602035708)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2017, 0, 5219, 0.869833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2017, 1, 163, 0.0272438575965235)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2017, 88, 311, 0.051893876188887)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2017, 0, 4791, 0.798633105517586)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2017, 1, 540, 0.09)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2017, 88, 362, 0.0603735823882588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2017, 0, 5363, 0.893833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2017, 1, 125, 0.0208960213975259)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2017, 88, 205, 0.0342065743367262)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAntiCompPract', 2017, 0, 5327, 0.887833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAntiCompPract', 2017, 1, 29, 0.00484706668895203)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAntiCompPract', 2017, 88, 337, 0.0562322709828133)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperContracts', 2017, 0, 5091, 0.8485)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperContracts', 2017, 1, 160, 0.0267022696929239)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperContracts', 2017, 88, 442, 0.0737158105403602)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsContractViols', 2017, 0, 5163, 0.8605)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsContractViols', 2017, 1, 113, 0.0189343163538874)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsContractViols', 2017, 88, 417, 0.0695811780410479)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOther', 2017, 0, 4427, 0.737833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOther', 2017, 1, 112, 0.0186822351959967)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOther', 2017, 88, 1154, 0.192557984315034)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Pressure', N'Pressure', 2017, 0, 5122, 0.853666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Pressure', N'Pressure', 2017, 1, 328, 0.0547122602168474)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Pressure', N'Pressure', 2017, 88, 210, 0.0350408810278658)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressureFreq', 2017, 1, 97, 0.0162778989763383)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressureFreq', 2017, 2, 139, 0.023185988323603)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressureFreq', 2017, 3, 51, 0.00862798172897987)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressureFreq', 2017, 4, 30, 0.00509251400441351)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressureFreq', 2017, 88, 12, 0.00212051599222477)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2017, 0, 652, 0.108866254800468)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2017, 1, 758, 0.126607649908134)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2017, 2, 2415, 0.4025)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2017, 3, 1723, 0.287406171809842)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2017, 88, 102, 0.0170198564992491)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceKeepJob', 2017, 0, 1474, 0.245748582860954)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceKeepJob', 2017, 1, 1199, 0.199833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceKeepJob', 2017, 2, 1844, 0.307384564094016)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceKeepJob', 2017, 3, 1011, 0.16869681294844)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceKeepJob', 2017, 88, 122, 0.0205283526838297)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2017, 0, 1937, 0.322887147857976)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2017, 1, 1221, 0.2035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2017, 2, 1544, 0.257590924257591)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2017, 3, 589, 0.0984785152984451)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2017, 88, 359, 0.0598632649658162)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2017, 0, 1673, 0.2790658882402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2017, 1, 1540, 0.256666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2017, 2, 1769, 0.294882480413402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2017, 3, 544, 0.0907725679959953)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2017, 88, 124, 0.0208648830556958)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceMyCareer', 2017, 0, 2006, 0.334444814938313)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceMyCareer', 2017, 1, 1408, 0.234666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceMyCareer', 2017, 2, 1516, 0.252708784797466)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceMyCareer', 2017, 3, 518, 0.0866220735785953)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceMyCareer', 2017, 88, 202, 0.0337059903220424)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceExtDemands', 2017, 0, 2861, 0.476833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceExtDemands', 2017, 1, 919, 0.153371161548732)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceExtDemands', 2017, 2, 868, 0.145077720207254)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceExtDemands', 2017, 3, 270, 0.0451505016722408)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceExtDemands', 2017, 88, 732, 0.122142499582847)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'IrregularSituations', 2017, 0, 3318, 0.553)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'IrregularSituations', 2017, 1, 1792, 0.298766255418473)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'IrregularSituations', 2017, 88, 539, 0.0898932621747832)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 1, 35, 0.00592918854819583)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 2, 61, 0.0105719237435009)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 3, 769, 0.128166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 4, 3175, 0.529254875812635)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 5, 1475, 0.24603836530442)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 88, 133, 0.0222110888443554)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 1, 154, 0.0260884296120617)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 2, 408, 0.0680567139282736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 3, 1405, 0.234322881921281)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 4, 2477, 0.412833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 5, 740, 0.123374458152718)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 88, 460, 0.076756215584849)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 1, 74, 0.0125359986447569)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 2, 203, 0.033861551292744)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 3, 1063, 0.177284856571047)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 4, 2893, 0.482166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 5, 1060, 0.176725575191731)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 88, 351, 0.0585683297180043)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 1, 287, 0.0482190860215054)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 2, 675, 0.112593828190158)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 3, 1315, 0.219569210218734)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 4, 2362, 0.393666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 5, 730, 0.121707235745248)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 88, 275, 0.0464998309097058)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 1, 297, 0.0496489468405216)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 2, 575, 0.0959132610508757)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 3, 1862, 0.310333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 4, 1844, 0.307384564094016)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 5, 526, 0.0876958986328776)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 88, 540, 0.0904825737265416)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 1, 15, 0.00261278522905417)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 2, 27, 0.00450375312760634)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 3, 219, 0.0367141659681475)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 4, 2638, 0.439666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 5, 2712, 0.452075345890982)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 88, 32, 0.00533956282329384)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 1, 30, 0.00522557045810834)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 2, 61, 0.0101751459549625)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 3, 360, 0.0601704830352666)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 4, 2595, 0.4325)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 5, 2552, 0.425404234039007)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 88, 45, 0.00750876022025697)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 1, 95, 0.0158518271316536)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 2, 279, 0.0465232616308154)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 3, 892, 0.148666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 4, 3024, 0.504084014002334)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 5, 1336, 0.222963951935915)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 88, 15, 0.0027603974972396)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 1, 200, 0.0333722676455865)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 2, 698, 0.116333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 3, 2316, 0.386257505003335)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 4, 1375, 0.229243081027009)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 5, 332, 0.0557233971131252)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 88, 719, 0.120073480293921)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 1, 1398, 0.233311081441923)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 2, 2180, 0.363515090878773)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 3, 898, 0.149766511007338)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 4, 322, 0.0536666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 5, 101, 0.0168529951610212)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 77, 24, 0.00405885337392187)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 88, 717, 0.119519919986664)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 1, 1083, 0.181043129388164)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 2, 2228, 0.371519092879773)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 3, 1017, 0.1695)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 4, 370, 0.0616872290763588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 5, 81, 0.0136248948696384)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 88, 859, 0.143190531755293)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 1, 1090, 0.182213306586426)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 2, 2419, 0.403368350842088)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 3, 1040, 0.173333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 4, 304, 0.0506835611870624)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 5, 51, 0.00857863751051304)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 88, 733, 0.122187031171862)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 1, 26, 0.00458553791887125)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 2, 124, 0.0207253886010363)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 3, 629, 0.104903268845897)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 4, 3243, 0.5405)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 5, 1064, 0.177570093457944)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 88, 550, 0.0917125229281307)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'ValuesConflict', 2017, 0, 1843, 0.307166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'ValuesConflict', 2017, 1, 2517, 0.419569928321387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'ValuesConflict', 2017, 2, 1123, 0.187291527685123)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'ValuesConflict', 2017, 3, 59, 0.0099276459700488)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'ValuesConflict', 2017, 88, 93, 0.0155648535564854)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 1, 110, 0.0183639398998331)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 2, 442, 0.074036850921273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 3, 1284, 0.214)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 4, 970, 0.161801501251043)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 5, 2529, 0.42164054684895)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 99, 297, 0.0497904442581727)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Mgmt', 2017, 1, 215, 0.0360980523841504)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Mgmt', 2017, 2, 1137, 0.189563187729243)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Mgmt', 2017, 3, 814, 0.135802469135802)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Mgmt', 2017, 4, 2839, 0.473245540923487)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Mgmt', 2017, 99, 626, 0.104333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Age', 2017, 1, 543, 0.0905150858476413)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Age', 2017, 2, 1930, 0.321773924641547)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Age', 2017, 3, 2231, 0.372081387591728)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Age', 2017, 4, 65, 0.0114942528735632)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Age', 2017, 99, 862, 0.143666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 1, 731, 0.122057104691935)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 2, 118, 0.0196994991652755)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 3, 262, 0.0436885109221277)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 4, 38, 0.00636302746148694)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 5, 22, 0.00394406597346719)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 6, 3377, 0.562927154525754)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 7, 97, 0.0163299663299663)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 99, 986, 0.164333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 1, 43, 0.00722932078009415)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 2, 741, 0.123726832526298)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 3, 768, 0.128021336889482)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 4, 1813, 0.302267422474158)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 5, 1602, 0.267178118745831)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 99, 664, 0.110666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 1, 450, 0.0750375187593797)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 2, 407, 0.0680488212673466)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 3, 836, 0.139356559426571)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 4, 1499, 0.249916638879627)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 5, 856, 0.142904841402337)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 99, 1583, 0.263833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Gender', 2017, 0, 1398, 0.233155436957972)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Gender', 2017, 1, 3718, 0.619769961660277)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Gender', 2017, 99, 513, 0.0855)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Standards', 2017, 0, 28, 0.00496982605608804)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Standards', 2017, 1, 5878, 0.979666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Standards', 2017, 88, 94, 0.0157216925907342)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Training', 2017, 0, 84, 0.0143983544737744)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Training', 2017, 1, 5750, 0.958333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Training', 2017, 88, 166, 0.0280168776371308)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Advice', 2017, 0, 70, 0.0117076434186319)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Advice', 2017, 1, 5721, 0.9535)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Advice', 2017, 88, 209, 0.0352742616033755)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Hotline', 2017, 0, 58, 0.0109454614078128)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Hotline', 2017, 1, 5781, 0.9635)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Hotline', 2017, 88, 161, 0.0269275798628533)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Appraisal', 2017, 0, 182, 0.0304398728884429)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Appraisal', 2017, 1, 5364, 0.894)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Appraisal', 2017, 88, 454, 0.0760469011725293)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Discipline', 2017, 0, 156, 0.0260956841753095)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Discipline', 2017, 1, 5055, 0.8425)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Awareness of Resources', N'Discipline', 2017, 88, 789, 0.131609674728941)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 0, 231, 0.0390929091216788)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 1, 614, 0.102555536996826)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 2, 850, 0.141690281713619)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 3, 904, 0.150842649758051)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 4, 1738, 0.289666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 77, 1566, 0.261174116077385)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'UseStandards', 2017, 88, 32, 0.00535206556280314)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 1, 164, 0.0274293360093661)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 2, 420, 0.0700583819849875)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 3, 1070, 0.178363060510085)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 4, 2884, 0.480666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 5, 1316, 0.219772879091516)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtInfo', 2017, 88, 62, 0.01053884072752)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 1, 3146, 0.524508169389797)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 2, 854, 0.142333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 3, 848, 0.141356892815469)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 4, 650, 0.108568565224653)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 5, 150, 0.0250710345980277)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'TopMgmt_ID', 2017, 88, 237, 0.0395329441201001)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 1, 36, 0.0064216910453086)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 2, 119, 0.0198498748957465)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 3, 693, 0.115519253208868)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 4, 2886, 0.481)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 5, 1855, 0.309372915276851)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2017, 88, 278, 0.0464960695768523)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 1, 132, 0.022077270446563)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 2, 310, 0.0517097581317765)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 3, 860, 0.143357226204367)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 4, 2807, 0.467833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 5, 1647, 0.274683122081388)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 77, 19, 0.00321979325538044)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupInfo', 2017, 88, 59, 0.010457284650833)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 1, 144, 0.0240440808148272)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 2, 327, 0.0545454545454545)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 3, 1113, 0.185747663551402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 4, 2737, 0.456242707117853)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 5, 1363, 0.227166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2017, 88, 124, 0.0216707444949318)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 1, 111, 0.0185339789614293)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 2, 217, 0.0361968306922435)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 3, 788, 0.131530629277249)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 4, 2713, 0.452242040340057)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 5, 1888, 0.314666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 77, 10, 0.0016946280291476)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2017, 88, 80, 0.0140671707402849)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 1, 37, 0.0063606670104865)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 2, 137, 0.0228523769808173)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 3, 917, 0.152833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 4, 3157, 0.526254375729288)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 5, 1443, 0.240660440293529)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 77, 2, 0.000637348629700446)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2017, 88, 114, 0.0190667335674862)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 1, 19, 0.00338922582946843)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 2, 69, 0.0115095913261051)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 3, 353, 0.0591091761553918)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 4, 2842, 0.473745624270712)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 5, 2400, 0.4)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 77, 1, 0.0007627765064836)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2017, 88, 76, 0.0127111557116575)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 1, 31, 0.00552289328344914)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 2, 121, 0.0201834862385321)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 3, 462, 0.0773610180843938)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 4, 2783, 0.463910651775296)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 5, 2300, 0.383333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 77, 16, 0.00271140484663616)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2017, 88, 46, 0.00769359424652952)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 1, 44, 0.00784873349982162)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 2, 152, 0.0253544620517098)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 3, 948, 0.159274193548387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 4, 2857, 0.476166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 5, 1667, 0.278018679119413)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 77, 1, 0.0007627765064836)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerTalk', 2017, 88, 90, 0.0150526843953838)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 1, 62, 0.0103696270279311)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 2, 170, 0.0284900284900285)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 3, 496, 0.083151718357083)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 4, 2449, 0.408234705784297)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 5, 2263, 0.377166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 77, 1, 0.000588235294117647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2017, 88, 277, 0.0462051709758132)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 1, 41, 0.00685733400234153)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 2, 105, 0.0175967823026647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 3, 412, 0.0690695725062867)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 4, 2581, 0.430238373062177)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 5, 2343, 0.3905)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 77, 13, 0.00228591524529629)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2017, 88, 223, 0.0371976647206005)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 1, 23, 0.00393903065593424)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 2, 83, 0.0139098374392492)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 3, 346, 0.0580050293378039)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 4, 2602, 0.433738956492749)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 5, 2489, 0.414833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 77, 3, 0.000946073793755913)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2017, 88, 172, 0.0286905754795663)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 1, 72, 0.0120421475163071)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 2, 172, 0.0294823448748714)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 3, 792, 0.13255230125523)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 4, 2564, 0.427404567427905)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 5, 1905, 0.3175)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2017, 88, 196, 0.0326939115929942)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 1, 59, 0.0098678708814183)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 2, 130, 0.0217864923747277)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 3, 561, 0.0938912133891213)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 4, 2595, 0.4325)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 5, 2250, 0.375250166777852)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 77, 17, 0.00288086764955092)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupExample', 2017, 88, 89, 0.0148457047539616)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 1, 15, 0.00267570460221192)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 2, 89, 0.0149153678565443)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 3, 729, 0.122008368200837)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 4, 2967, 0.4945)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 5, 1787, 0.298032021347565)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 77, 2, 0.000482741974414675)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerExample', 2017, 88, 112, 0.0186822351959967)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 1, 40, 0.00688231245698555)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 2, 97, 0.0166552197802198)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 3, 510, 0.0853128136500502)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 4, 2784, 0.464)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 5, 2143, 0.357404936624416)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2017, 88, 95, 0.0158465387823186)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 1, 27, 0.00481026189203634)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 2, 82, 0.0137422490363667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 3, 427, 0.0734055354993983)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 4, 2762, 0.460333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 5, 2309, 0.385090060040027)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 77, 19, 0.00321979325538044)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2017, 88, 43, 0.00717264386989158)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 1, 19, 0.00338922582946843)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 2, 67, 0.0112284229931289)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 3, 641, 0.108076209745405)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 4, 2885, 0.480833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 5, 1976, 0.329553035356905)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2017, 88, 81, 0.013511259382819)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 1, 63, 0.0105580693815988)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 2, 152, 0.0256670043904086)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 3, 821, 0.137520938023451)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 4, 2624, 0.437333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 5, 1822, 0.303869246164109)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 77, 19, 0.00334095305081765)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'SupFeedback', 2017, 88, 156, 0.0260216847372811)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 1, 59, 0.0099847689964461)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 2, 162, 0.0285361986964946)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 3, 696, 0.116641528406234)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 4, 2235, 0.37256209368228)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 5, 1947, 0.3245)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2017, 88, 549, 0.0915763135946622)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'General misconduct ', N'MiscGen', 2017, 0, 4517, 0.752833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'General misconduct ', N'MiscGen', 2017, 1, 642, 0.107214428857715)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'General misconduct ', N'MiscGen', 2017, 88, 477, 0.0797792272955344)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2017, 0, 252, 0.0421404682274247)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2017, 1, 369, 0.061623246492986)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2017, 88, 22, 0.00401973323588526)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2017, 0, 95, 0.0158862876254181)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2017, 1, 116, 0.0197614991482112)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2017, 88, 36, 0.00697269029633934)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2017, 0, 195, 0.0326086956521739)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2017, 1, 39, 0.00794135613927917)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2017, 88, 13, 0.00251791594034476)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2017, 0, 144, 0.0240802675585284)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2017, 1, 85, 0.0146123431321987)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2017, 88, 18, 0.0035650623885918)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2017, 0, 138, 0.0230769230769231)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2017, 1, 90, 0.0153321976149915)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2017, 88, 19, 0.00368003098973465)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2017, 0, 183, 0.0306020066889632)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2017, 1, 53, 0.00911122571772391)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2017, 88, 11, 0.0021305442572148)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2017, 0, 106, 0.0177257525083612)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2017, 1, 125, 0.0212947189097104)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2017, 88, 16, 0.00309897346503971)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2017, 0, 169, 0.0282608695652174)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2017, 1, 48, 0.00822058571673232)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2017, 88, 31, 0.00589914367269267)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2017, 0, 174, 0.0290969899665552)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2017, 1, 60, 0.0106894708711919)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2017, 88, 13, 0.00251791594034476)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2017, 0, 154, 0.0262350936967632)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2017, 1, 71, 0.0118729096989967)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2017, 88, 22, 0.00426108851442959)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2017, 0, 139, 0.0232441471571906)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2017, 1, 75, 0.0129087779690189)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2017, 88, 34, 0.00584493725287949)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2017, 0, 11, 0.00187713310580205)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2017, 1, 349, 0.0582832331329325)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMyRespons', 2017, 88, 5, 0.000999600159936026)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2017, 0, 26, 0.00443686006825939)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2017, 1, 326, 0.0544422177688711)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonAction', 2017, 88, 13, 0.00233770904513577)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2017, 0, 41, 0.00699658703071672)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2017, 1, 286, 0.0477621910487642)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonMgmtSupport', 2017, 88, 38, 0.00668190610163531)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2017, 0, 45, 0.00829646017699115)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2017, 1, 284, 0.0474281897127589)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonSupSupport', 2017, 88, 36, 0.00633022683312819)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2017, 0, 44, 0.00740491417031303)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2017, 1, 276, 0.0460921843687375)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonPeerSupport', 2017, 88, 45, 0.00771340418237916)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2017, 0, 41, 0.00690003365870078)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2017, 1, 283, 0.0472611890447562)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Reason you reported: Asked if they did report at least one type of misconduct', N'RepReasonConfid', 2017, 88, 41, 0.00720942500439599)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 1, 35, 0.00599931436407268)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 2, 39, 0.00656344665095927)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 3, 46, 0.00808862317566379)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 4, 123, 0.020936170212766)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 5, 113, 0.0188710754843019)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2017, 88, 9, 0.00156876416245424)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2017, 0, 28, 0.00524246395806029)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2017, 1, 34, 0.00582790538224203)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2017, 88, 11, 0.00185122854257826)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2017, 0, 20, 0.00374461711290021)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2017, 1, 35, 0.00589027263547627)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2017, 88, 18, 0.00351425224521671)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2017, 0, 43, 0.00807057057057057)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2017, 1, 14, 0.00235610905419051)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2017, 88, 16, 0.00287718036324402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2017, 0, 19, 0.00325677065478231)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2017, 1, 39, 0.0070131271354073)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2017, 88, 15, 0.00252440255806126)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2017, 0, 13, 0.00222831676379842)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2017, 1, 37, 0.0066534795900018)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2017, 88, 23, 0.00387075058902726)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2017, 0, 230, 0.0387075058902726)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2017, 1, 77, 0.0128590514362057)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2017, 88, 56, 0.00955631399317406)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2017, 0, 36, 0.00641711229946524)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2017, 1, 32, 0.0053440213760855)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalColdShoulder', 2017, 88, 7, 0.00124955373081042)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2017, 0, 31, 0.00552584670231729)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2017, 1, 38, 0.00634602538410154)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalExcluded', 2017, 88, 6, 0.00102845389098389)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2017, 0, 38, 0.00651354130956462)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2017, 1, 30, 0.00501002004008016)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalMgmtVerbalAbuse', 2017, 88, 7, 0.00124955373081042)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2017, 0, 37, 0.00659536541889483)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2017, 1, 30, 0.00501002004008016)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPeersVerbalAbuse', 2017, 88, 8, 0.00142806140664049)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2017, 0, 48, 0.0082276311278711)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2017, 1, 23, 0.00384101536406146)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalPhysHarm', 2017, 88, 4, 0.000810536980749747)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2017, 0, 35, 0.00599931436407268)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2017, 1, 36, 0.00601202404809619)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalNotAdvanced', 2017, 88, 4, 0.000810536980749747)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2017, 0, 44, 0.00784313725490196)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2017, 1, 25, 0.0041750167000668)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobShifted', 2017, 88, 6, 0.00102845389098389)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2017, 0, 43, 0.00766488413547237)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2017, 1, 24, 0.00400801603206413)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalDemoted', 2017, 88, 8, 0.00137127185464518)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2017, 0, 40, 0.0071301247771836)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2017, 1, 25, 0.0041750167000668)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalJobThreatened', 2017, 88, 10, 0.00171408981830648)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2017, 0, 29, 0.00593775593775594)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2017, 1, 36, 0.00601202404809619)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Type of retaliation - asked if they did experience retaliation for report of misconduct', N'RetalOther', 2017, 88, 10, 0.00171408981830648)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSexHarass', 2017, 0, 5251, 0.875166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSexHarass', 2017, 1, 89, 0.0148630594522378)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSexHarass', 2017, 88, 209, 0.0348623853211009)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOfferKickbacks', 2017, 0, 5208, 0.868)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOfferKickbacks', 2017, 1, 58, 0.0100276625172891)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOfferKickbacks', 2017, 88, 283, 0.0472060050041701)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAbuse', 2017, 0, 4733, 0.788833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAbuse', 2017, 1, 529, 0.0884763338350895)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAbuse', 2017, 88, 287, 0.0478732276897414)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsDiscrim', 2017, 0, 5116, 0.852666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsDiscrim', 2017, 1, 191, 0.0321440592393134)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsDiscrim', 2017, 88, 242, 0.0403669724770642)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsHealthViol', 2017, 0, 5043, 0.8405)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsHealthViol', 2017, 1, 260, 0.0434056761268781)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsHealthViol', 2017, 88, 246, 0.0410341951626355)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConflictInterest', 2017, 0, 4890, 0.815)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConflictInterest', 2017, 1, 274, 0.0458117371676977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConflictInterest', 2017, 88, 385, 0.0642201834862385)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsFailProfStandards', 2017, 0, 4987, 0.831166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsFailProfStandards', 2017, 1, 189, 0.0316106372303061)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsFailProfStandards', 2017, 88, 373, 0.0622185154295246)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAlterDocs', 2017, 0, 5105, 0.850833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAlterDocs', 2017, 1, 123, 0.0212729159460394)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAlterDocs', 2017, 88, 321, 0.0535446205170976)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCompetInfo', 2017, 0, 5157, 0.8595)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCompetInfo', 2017, 1, 45, 0.00823120541430401)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCompetInfo', 2017, 88, 347, 0.0578815679733111)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConfInfo', 2017, 0, 5170, 0.861666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConfInfo', 2017, 1, 54, 0.00922919159118099)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsConfInfo', 2017, 88, 325, 0.0542118432026689)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperHire', 2017, 0, 4981, 0.830166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperHire', 2017, 1, 144, 0.0245315161839864)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperHire', 2017, 88, 424, 0.0707256046705588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2017, 0, 5074, 0.845666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2017, 1, 55, 0.00940010254657324)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsMisrepFinRecords', 2017, 88, 420, 0.0700583819849875)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieExternal', 2017, 0, 4955, 0.825833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieExternal', 2017, 1, 177, 0.031828807768387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieExternal', 2017, 88, 417, 0.069557964970809)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieEmployees', 2017, 0, 4764, 0.794)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieEmployees', 2017, 1, 298, 0.0498411105536043)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsLieEmployees', 2017, 88, 487, 0.0812343619683069)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsTimeCheat', 2017, 0, 4978, 0.829666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsTimeCheat', 2017, 1, 206, 0.0344539220605452)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsTimeCheat', 2017, 88, 365, 0.060884070058382)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsStealing', 2017, 0, 5187, 0.8645)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsStealing', 2017, 1, 84, 0.0143565202529482)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsStealing', 2017, 88, 278, 0.046371976647206)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsInternetAbuse', 2017, 0, 5039, 0.839833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsInternetAbuse', 2017, 1, 151, 0.0255369524775917)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsInternetAbuse', 2017, 88, 359, 0.059883236030025)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEnvViol', 2017, 0, 5202, 0.867)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEnvViol', 2017, 1, 46, 0.00841412109017743)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEnvViol', 2017, 88, 301, 0.050208507089241)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplBenViols', 2017, 0, 4946, 0.824333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplBenViols', 2017, 1, 190, 0.0317460317460317)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplBenViols', 2017, 88, 413, 0.0688907422852377)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2017, 0, 5158, 0.859666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2017, 1, 30, 0.00548747027620267)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCustPrivacyBreach', 2017, 88, 361, 0.0602168473728107)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2017, 0, 5102, 0.850333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2017, 1, 77, 0.0131175468483816)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsEmplPrivacyBreach', 2017, 88, 370, 0.0617180984153461)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2017, 0, 4890, 0.815)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2017, 1, 276, 0.0464411913175164)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsCoResourceAbuse', 2017, 88, 383, 0.0638865721434529)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2017, 0, 5224, 0.870666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2017, 1, 62, 0.0113407719041522)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsSubstanceAbuse', 2017, 88, 263, 0.0438698915763136)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAntiCompPract', 2017, 0, 5165, 0.860833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAntiCompPract', 2017, 1, 26, 0.00475580757270898)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsAntiCompPract', 2017, 88, 358, 0.0597164303586322)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperContracts', 2017, 0, 5013, 0.8355)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperContracts', 2017, 1, 97, 0.0164909894593676)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsImproperContracts', 2017, 88, 439, 0.0732276897414512)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsContractViols', 2017, 0, 5064, 0.844)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsContractViols', 2017, 1, 73, 0.0127466387288284)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsContractViols', 2017, 88, 412, 0.0687239366138449)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOther', 2017, 0, 4530, 0.755)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOther', 2017, 1, 60, 0.0106951871657754)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific misconduct questions', N'ObsOther', 2017, 88, 957, 0.159633027522936)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Pressure', N'Pressure', 2017, 0, 5155, 0.859166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Pressure', N'Pressure', 2017, 1, 192, 0.032064128256513)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Pressure', N'Pressure', 2017, 88, 185, 0.0308590492076731)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressureFreq', 2017, 1, 54, 0.00927197802197802)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressureFreq', 2017, 2, 60, 0.0104766893661603)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressureFreq', 2017, 3, 44, 0.0073738897268309)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressureFreq', 2017, 4, 26, 0.00434201736806947)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressureFreq', 2017, 88, 8, 0.00166389351081531)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2017, 0, 867, 0.144668780243618)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2017, 1, 874, 0.145715238412804)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2017, 2, 2352, 0.392)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2017, 3, 1275, 0.212677231025855)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourcePerfGoals', 2017, 88, 156, 0.0261701056869653)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceKeepJob', 2017, 0, 1846, 0.307974641307975)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceKeepJob', 2017, 1, 1283, 0.213833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceKeepJob', 2017, 2, 1591, 0.26521086847808)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceKeepJob', 2017, 3, 652, 0.109048335842114)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceKeepJob', 2017, 88, 152, 0.025520483546004)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2017, 0, 2179, 0.363166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2017, 1, 1232, 0.2054018006002)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2017, 2, 1252, 0.208701450241707)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2017, 3, 415, 0.0693863902357465)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceSaveJobs', 2017, 88, 446, 0.0748824714573539)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2017, 0, 1761, 0.2935)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2017, 1, 1578, 0.263043840640107)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2017, 2, 1582, 0.263798565949641)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2017, 3, 444, 0.0740617180984153)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceDirectSupPressure', 2017, 88, 159, 0.0266957689724647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceMyCareer', 2017, 0, 1957, 0.326166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceMyCareer', 2017, 1, 1290, 0.215143428952635)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceMyCareer', 2017, 2, 1483, 0.247290311822578)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceMyCareer', 2017, 3, 519, 0.0865721434528774)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceMyCareer', 2017, 88, 275, 0.0460868107926931)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceExtDemands', 2017, 0, 2558, 0.426333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceExtDemands', 2017, 1, 949, 0.158272181454303)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceExtDemands', 2017, 2, 893, 0.149032042723632)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceExtDemands', 2017, 3, 274, 0.0457047539616347)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Specific Types of Pressure', N'PressSourceExtDemands', 2017, 88, 850, 0.141737535434384)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'IrregularSituations', 2017, 0, 3815, 0.635833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'IrregularSituations', 2017, 1, 1170, 0.19568489713999)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'IrregularSituations', 2017, 88, 535, 0.0892410341951626)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 1, 20, 0.00493461633358006)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 2, 87, 0.014512093411176)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 3, 655, 0.109678499665104)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 4, 3054, 0.509)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 5, 1497, 0.249666444296197)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'Prepared', 2017, 88, 204, 0.0340510766149224)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 1, 69, 0.0118719889883001)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 2, 222, 0.0373046546798857)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 3, 1193, 0.199631860776439)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 4, 2628, 0.438)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 5, 990, 0.165110073382255)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'SocietyEffects', 2017, 88, 412, 0.0687239366138449)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 1, 52, 0.00891173950299914)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 2, 181, 0.0308242506811989)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 3, 1029, 0.172217573221757)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 4, 2745, 0.4575)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 5, 1145, 0.190960640426951)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EnvEffects', 2017, 88, 362, 0.0603836530442035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 1, 193, 0.0322796454256565)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 2, 432, 0.0723012552301255)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 3, 1143, 0.190595297648824)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 4, 2475, 0.4125)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 5, 1003, 0.167278185456971)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'EmpEffects', 2017, 88, 268, 0.0447039199332777)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 1, 162, 0.0270948319116909)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 2, 339, 0.05678391959799)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 3, 1697, 0.282974820743705)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 4, 1974, 0.329054842473746)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 5, 795, 0.1325)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Company considers effect on questions', N'FutureGenEffects', 2017, 88, 547, 0.0912427022518766)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 1, 9, 0.00226472068444892)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 2, 18, 0.0032108455226543)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 3, 173, 0.0289685197588748)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 4, 2264, 0.377396232705451)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 5, 2990, 0.498333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsLaw', 2017, 88, 59, 0.00984153461217681)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 1, 15, 0.00362056480811007)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 2, 41, 0.00705437026841018)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 3, 260, 0.043398430979803)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 4, 2254, 0.375729288214702)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 5, 2883, 0.4805)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'EthicsValues', 2017, 88, 60, 0.0100083402835696)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 1, 56, 0.0106060606060606)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 2, 172, 0.0286905754795663)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 3, 753, 0.125667556742323)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 4, 2907, 0.484580763460577)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 5, 1596, 0.266)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'Satisfied', 2017, 88, 28, 0.00570148645896966)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 1, 129, 0.0215755143000502)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 2, 529, 0.0882402001668057)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 3, 1743, 0.29088785046729)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 4, 1773, 0.2955)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 5, 715, 0.119285952619286)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardEthics', 2017, 88, 620, 0.103488566182607)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 1, 1431, 0.238738738738739)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 2, 1867, 0.311166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 3, 835, 0.139189864977496)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 4, 570, 0.0950792326939116)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 5, 166, 0.0277221108884436)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 77, 27, 0.00457549567869853)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'SupRewardResults', 2017, 88, 613, 0.102217775554444)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 1, 1298, 0.21654988321655)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 2, 2072, 0.345333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 3, 922, 0.153692282047008)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 4, 437, 0.0728940783986656)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 5, 111, 0.0185370741482966)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Effectivness of Program', N'RewardResults', 2017, 88, 666, 0.111055527763882)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 1, 1223, 0.203833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 2, 2146, 0.357785928642881)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 3, 925, 0.154192365394232)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 4, 484, 0.0810991957104558)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 5, 117, 0.0195390781563126)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'PeerShowRespResults', 2017, 88, 610, 0.101921470342523)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 1, 36, 0.00607492406344921)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 2, 126, 0.0210175145954962)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 3, 578, 0.0967040321231387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 4, 3010, 0.501666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 5, 1301, 0.21705038371705)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'DealsFairly', 2017, 88, 452, 0.0754465030879653)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'ValuesConflict', 2017, 0, 2025, 0.337837837837838)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'ValuesConflict', 2017, 1, 2182, 0.363666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'ValuesConflict', 2017, 2, 1039, 0.173282188125417)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'ValuesConflict', 2017, 3, 99, 0.0165330661322645)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Additional Questions', N'ValuesConflict', 2017, 88, 157, 0.0261884904086739)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 1, 440, 0.0734067400734067)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 2, 736, 0.122809944935758)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 3, 931, 0.155373831775701)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 4, 1271, 0.21268406961178)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 5, 1791, 0.2985)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Tenure', 2017, 99, 330, 0.055267124434768)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Mgmt', 2017, 1, 233, 0.0391925988225399)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Mgmt', 2017, 2, 1167, 0.19511787326534)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Mgmt', 2017, 3, 907, 0.151166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Mgmt', 2017, 4, 2488, 0.414735789298216)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Mgmt', 2017, 99, 703, 0.117854149203688)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Age', 2017, 1, 697, 0.116282949616283)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Age', 2017, 2, 2333, 0.388833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Age', 2017, 3, 1641, 0.273591197065689)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Age', 2017, 4, 42, 0.00723016009640213)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Age', 2017, 99, 784, 0.130775646371977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 1, 1676, 0.279379896649442)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 2, 80, 0.0135639199728722)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 3, 179, 0.0301346801346801)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 4, 28, 0.00471936625653127)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 5, 8, 0.001643723032669)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 6, 2470, 0.41387399463807)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 7, 163, 0.0271757252417473)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Ethnicity', 2017, 99, 893, 0.148833333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 1, 85, 0.0150869719559815)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 2, 812, 0.135378459486495)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 3, 933, 0.1555)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 4, 1826, 0.304485576121394)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 5, 1076, 0.180234505862647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Education', 2017, 99, 763, 0.127293960627294)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 1, 1057, 0.176196032672112)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 2, 580, 0.0968118844934068)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 3, 755, 0.126592890677398)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 4, 866, 0.145131556896263)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 5, 631, 0.105872483221477)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Salary', 2017, 99, 1604, 0.267333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Gender', 2017, 1, 3328, 0.554944138736035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Gender', 2017, 2, 1298, 0.216333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Embraer', N'Participant Demographic Variables', N'Gender', 2017, 99, 865, 0.144503842298697)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 1, 66, 0.00947731188971855)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 2, 104, 0.0151382823871907)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 3, 362, 0.0519666953775481)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 4, 2620, 0.374821173104435)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 5, 3652, 0.52290950744559)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 88, 186, 0.0269331016507385)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 1, 192, 0.0275981026304442)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 2, 366, 0.0523605150214592)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 3, 1194, 0.171207341554345)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 4, 2896, 0.414365431392188)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 5, 2061, 0.295103092783505)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 77, 9, 0.00129236071223435)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtKeepProm', 2015, 88, 272, 0.038940586972083)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 1, 139, 0.0199971227161559)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 2, 225, 0.0324488029997116)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 3, 545, 0.0781474046458274)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 4, 2405, 0.344112176277007)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 5, 3601, 0.51516452074392)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 77, 12, 0.00172314761631246)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 88, 63, 0.00923889133304004)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 1, 63, 0.00935412026726058)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 2, 124, 0.017800746482917)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 3, 567, 0.0811739441660702)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 4, 3115, 0.445636623748212)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 5, 3031, 0.433991981672394)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 77, 7, 0.00100516944284894)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 88, 83, 0.0118962304715494)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 1, 52, 0.00773349196906603)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 2, 89, 0.0127361190612479)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 3, 499, 0.0717263188155814)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 4, 2759, 0.394706723891273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 5, 3472, 0.496780655315496)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 77, 6, 0.000861573808156232)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtTalks', 2015, 88, 99, 0.014189479719077)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 1, 88, 0.0126600489138253)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 2, 192, 0.0275624461670973)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 3, 620, 0.0889016346429596)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 4, 2482, 0.355078683834049)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 5, 3528, 0.504793246530262)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 77, 19, 0.00285456730769231)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 88, 47, 0.0068533100029163)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 1, 159, 0.0228415457549203)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 2, 314, 0.0449341728677733)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 3, 985, 0.140976098468585)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 4, 2295, 0.328326180257511)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 5, 2774, 0.396909429102876)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 77, 3, 0.000450112528132033)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtAccount', 2015, 88, 422, 0.0604844489035402)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 1, 100, 0.0143864192202561)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 2, 172, 0.0246913580246914)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 3, 535, 0.0767795637198622)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 4, 2499, 0.357510729613734)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 5, 3395, 0.485763342395192)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 77, 15, 0.00239043824701195)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 88, 236, 0.0338545402381294)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 1, 63, 0.00935412026726058)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 2, 110, 0.0157909847832328)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 3, 332, 0.0477285796434733)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 4, 2314, 0.3310443490701)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 5, 3980, 0.569466304192302)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 77, 2, 0.00130633572828217)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'NonMgmtAccount', 2015, 88, 151, 0.0216425397735416)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 1, 114, 0.0164005179110919)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 2, 181, 0.0259015455065827)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 3, 1136, 0.162587662802347)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 4, 2638, 0.377396280400572)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 5, 2495, 0.357245131729668)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 77, 11, 0.00157955198161976)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtExample', 2015, 88, 362, 0.0518253400143164)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 1, 95, 0.0136670982592433)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 2, 164, 0.0236447520184544)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 3, 499, 0.071633649153029)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 4, 2250, 0.321888412017167)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 5, 3850, 0.550865646015167)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 77, 18, 0.00286852589641434)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 88, 61, 0.00880357916005195)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 1, 64, 0.00919936754348138)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 2, 93, 0.0133085289066972)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 3, 818, 0.117292801835389)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 4, 2713, 0.388125894134478)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 5, 2930, 0.419230218915439)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 77, 7, 0.00100516944284894)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtSupport', 2015, 88, 296, 0.0424251110792604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 1, 56, 0.0080563947633434)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 2, 90, 0.0133848899464604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 3, 476, 0.0683220898521602)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 4, 2519, 0.360371959942775)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 5, 3702, 0.529689512090428)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 77, 18, 0.00286852589641434)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 88, 60, 0.00888757221152422)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 1, 36, 0.00534521158129176)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 2, 91, 0.0130634510479472)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 3, 644, 0.0921711750393588)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 4, 2751, 0.393562231759657)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 5, 3286, 0.470504009163803)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 77, 6, 0.000900225056264066)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 88, 107, 0.0153361043428408)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 1, 89, 0.0128279042951859)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 2, 176, 0.0252982607445738)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 3, 1029, 0.147252432741843)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 4, 1981, 0.283404864091559)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 5, 2487, 0.356099656357388)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'TopMgmtNoRetaliate', 2015, 88, 1154, 0.165353202464536)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 1, 98, 0.014098690835851)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 2, 221, 0.0320661636680209)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 3, 1135, 0.162374821173104)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 4, 2467, 0.352983259407641)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 5, 2666, 0.381565764992128)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 77, 24, 0.00382470119521912)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 88, 302, 0.0433347682594346)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'General misconduct ', N'MiscGen', 2015, 0, 5740, 0.821173104434907)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'General misconduct ', N'MiscGen', 2015, 1, 718, 0.103042479908152)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'General misconduct ', N'MiscGen', 2015, 88, 452, 0.0649705332758373)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 0, 261, 0.0374569460390356)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 1, 411, 0.0590008613264427)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 88, 45, 0.0065406976744186)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2015, 0, 37, 0.00530998851894374)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2015, 1, 187, 0.0269026039418789)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2015, 88, 33, 0.00478816018572258)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAcceptedBhvr', 2015, 0, 106, 0.0152123995407577)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAcceptedBhvr', 2015, 1, 94, 0.0135232340670407)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAcceptedBhvr', 2015, 88, 57, 0.00827045850261172)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2015, 0, 185, 0.0265499425947187)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2015, 1, 45, 0.00658087159988301)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2015, 88, 27, 0.00396942075860041)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2015, 0, 138, 0.019804822043628)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2015, 1, 88, 0.0126600489138253)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearMgmtRetal', 2015, 88, 31, 0.00455748309320788)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2015, 0, 134, 0.0194428322692977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2015, 1, 97, 0.0139207807118255)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2015, 88, 26, 0.00382240517494854)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2015, 0, 169, 0.0242537313432836)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2015, 1, 67, 0.00978102189781022)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2015, 88, 21, 0.003047011027278)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2015, 0, 73, 0.0104764638346728)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2015, 1, 160, 0.0230182707524097)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2015, 88, 24, 0.00373018340068387)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2015, 0, 182, 0.0261194029850746)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2015, 1, 44, 0.00638421358096344)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2015, 88, 31, 0.00455748309320788)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2015, 0, 202, 0.0289896670493685)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2015, 1, 35, 0.00543225205649542)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepResolve', 2015, 88, 20, 0.00294031167303734)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2015, 0, 165, 0.0236796785304248)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2015, 1, 67, 0.00963890087757157)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2015, 88, 25, 0.00367538959129668)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2015, 0, 174, 0.0249712973593571)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2015, 1, 41, 0.00601173020527859)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2015, 88, 42, 0.00617465451337842)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepDidntBelieveMisc', 2015, 0, 205, 0.0294202066590126)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepDidntBelieveMisc', 2015, 1, 30, 0.00456482045039562)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepDidntBelieveMisc', 2015, 88, 22, 0.00323434284034108)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepPastRetal', 2015, 0, 201, 0.0291642484039466)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepPastRetal', 2015, 1, 34, 0.00487944890929966)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepPastRetal', 2015, 88, 22, 0.00323434284034108)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 1, 104, 0.0149403821290045)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 2, 96, 0.0139799038881608)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 3, 86, 0.0123456790123457)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 4, 58, 0.00836096295228485)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 5, 53, 0.00766338924233661)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 88, 13, 0.00212037188060675)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2015, 0, 69, 0.00991236891251257)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2015, 1, 98, 0.0142649199417758)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2015, 88, 32, 0.00465996796272026)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2015, 0, 35, 0.00504468146439896)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2015, 1, 86, 0.0123545467605229)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2015, 88, 78, 0.011353711790393)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatExpRetaliation', 2015, 0, 113, 0.0162332998132452)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatExpRetaliation', 2015, 1, 56, 0.00807149034303834)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatExpRetaliation', 2015, 88, 30, 0.00448765893792072)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2015, 0, 117, 0.01680792989513)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2015, 1, 35, 0.00509683995922528)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2015, 88, 47, 0.00677428653790718)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2015, 0, 32, 0.00512163892445583)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2015, 1, 106, 0.0152276971699468)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2015, 88, 61, 0.00889472149314669)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2015, 0, 35, 0.00502801321649188)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2015, 1, 91, 0.0132459970887918)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2015, 88, 73, 0.010521764197175)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 0, 273, 0.0391903531438415)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 1, 67, 0.00965696166042087)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 88, 69, 0.0100436681222707)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Outcomes - Key measures used to assess ethical culture', N'Pressure', 2015, 0, 6521, 0.932904148783977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Outcomes - Key measures used to assess ethical culture', N'Pressure', 2015, 1, 228, 0.0328010358221839)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Outcomes - Key measures used to assess ethical culture', N'Pressure', 2015, 88, 149, 0.0213834672789897)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 1, 15, 0.00630782169890664)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 2, 50, 0.0189250567751703)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 3, 168, 0.0638297872340425)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 4, 1205, 0.454374057315234)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 5, 1160, 0.43757072802716)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerConsider', 2015, 88, 54, 0.0209871745044695)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 1, 61, 0.0251235584843493)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 2, 96, 0.037037037037037)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 3, 240, 0.091150778579567)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 4, 981, 0.369909502262443)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 5, 1252, 0.472274613353451)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupKeepProm', 2015, 88, 22, 0.00844529750479846)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 1, 21, 0.0088309503784693)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 2, 64, 0.024224072672218)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 3, 225, 0.0854863221884498)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 4, 1287, 0.485294117647059)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 5, 1023, 0.386037735849057)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerKeepProm', 2015, 88, 32, 0.0126732673267327)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 1, 29, 0.0119439868204283)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 2, 65, 0.0246960486322188)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 3, 246, 0.0932524639878696)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 4, 1031, 0.388763197586727)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 5, 1262, 0.47622641509434)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupTalks', 2015, 88, 16, 0.00687580575848732)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 1, 33, 0.0140905209222886)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 2, 62, 0.0245739199365834)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 3, 182, 0.0691226737561717)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 4, 1077, 0.406108597285068)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 5, 1227, 0.462844209732177)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupAccount', 2015, 88, 67, 0.0254559270516717)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 1, 35, 0.0135030864197531)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 2, 49, 0.0198059822150364)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 3, 212, 0.0805165210786176)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 4, 969, 0.365384615384615)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 5, 1358, 0.512259524707657)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupExample', 2015, 88, 23, 0.00952380952380952)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 1, 20, 0.00823723228995058)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 2, 32, 0.0150730098916627)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 3, 191, 0.0725408279529054)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 4, 1064, 0.401206636500754)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 5, 1304, 0.491889852885704)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'SupSupport', 2015, 88, 25, 0.00959692898272553)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 1, 11, 0.00462573591253154)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 2, 26, 0.0111924235901851)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 3, 261, 0.0991264717052791)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 4, 1157, 0.436274509803922)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 5, 1143, 0.431320754716981)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Ethics Related Actions - Culture Questions', N'PeerSupport', 2015, 88, 38, 0.0145873320537428)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 1, 39, 0.0150462962962963)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 2, 92, 0.0348749052312358)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 3, 483, 0.18247072157159)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 4, 1033, 0.389517345399698)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 5, 888, 0.335094339622642)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Effectivness of Program', N'SupFeedback', 2015, 88, 99, 0.0374432677760968)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'General misconduct ', N'MiscGen', 2015, 0, 2160, 0.81447963800905)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'General misconduct ', N'MiscGen', 2015, 1, 276, 0.10446631339894)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'General misconduct ', N'MiscGen', 2015, 88, 197, 0.0751048417842165)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 0, 93, 0.0352006056018168)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 1, 174, 0.0689655172413793)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reporting question - only asked if they observed general and/or specific type of misconduct', N'RepGen', 2015, 88, 10, 0.00388802488335925)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2015, 0, 6, 0.00253057781526782)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2015, 1, 76, 0.0287660862982589)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoAction', 2015, 88, 10, 0.00413393964448119)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2015, 0, 75, 0.0313021702838063)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2015, 1, 14, 0.00529901589704769)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNotKnowWhom', 2015, 88, 3, 0.0016025641025641)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2015, 0, 51, 0.0193035579106737)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2015, 1, 34, 0.0129474485910129)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearSupRetal', 2015, 88, 7, 0.00289375775113683)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2015, 0, 60, 0.0231481481481482)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2015, 1, 26, 0.00984102952308857)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepFearPeerRetal', 2015, 88, 6, 0.00269420745397396)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2015, 0, 23, 0.00970054829185998)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2015, 1, 63, 0.0238455715367146)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepNoConfid', 2015, 88, 6, 0.00320512820512821)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2015, 0, 55, 0.0212191358024691)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2015, 1, 25, 0.00952018278750952)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepSomeoneElse', 2015, 88, 12, 0.00454201362604088)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepIResolve', 2015, 0, 79, 0.0299015897047691)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepIResolve', 2015, 1, 10, 0.00380807311500381)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepIResolve', 2015, 88, 3, 0.00150451354062187)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2015, 0, 53, 0.0200605601816805)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2015, 1, 28, 0.0106626047220107)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepToPerInvolved', 2015, 88, 11, 0.00486080424215643)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2015, 0, 63, 0.0238455715367146)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2015, 1, 10, 0.00380807311500381)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Reason for not reporting - only asked if they did not report at least once instance of misconduct', N'NoRepAnotherDid', 2015, 88, 19, 0.00839593460008838)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 1, 26, 0.0109519797809604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 2, 48, 0.0197693574958814)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 3, 34, 0.0139230139230139)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 4, 38, 0.0150614347998415)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 5, 23, 0.00922953451043339)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Satisfaction with company response to misconduct', N'SatWithResponse', 2015, 88, 5, 0.00212134068731438)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2015, 0, 26, 0.0109519797809604)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2015, 1, 35, 0.0144151565074135)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatActionUnknown', 2015, 88, 12, 0.00504625735912532)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2015, 0, 13, 0.00546677880571909)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2015, 1, 33, 0.0135914332784185)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatNoPursuit', 2015, 88, 27, 0.0117852466171977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2015, 0, 43, 0.0177100494233937)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2015, 1, 16, 0.00698384984722828)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDidntBelieveMgmt', 2015, 88, 14, 0.00612423447069116)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2015, 0, 12, 0.0050547598989048)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2015, 1, 37, 0.0152388797364086)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatInsufficientPunishment', 2015, 88, 24, 0.0105355575065847)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2015, 0, 19, 0.00798990748528175)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2015, 1, 29, 0.0119439868204283)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Dissatisfication - asked if they indicated dissatisfaction with company response', N'DissatDisagreeWithInvestigation', 2015, 88, 25, 0.0109361329833771)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 0, 118, 0.0467697185889814)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 1, 26, 0.0107084019769358)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Retaliation - asked if they did report misconduct', N'ExpRetaliation', 2015, 88, 29, 0.0127304653204565)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Pressure', N'Pressure', 2015, 0, 2475, 0.933257918552036)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Pressure', N'Pressure', 2015, 1, 86, 0.0357588357588358)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Pressure', N'Pressure', 2015, 88, 68, 0.0259245139153641)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Participant Demographic Variables', N'Tenure', 2015, 1, 384, 0.144796380090498)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Participant Demographic Variables', N'Tenure', 2015, 2, 483, 0.210274270787984)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Participant Demographic Variables', N'Tenure', 2015, 3, 361, 0.194819212088505)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Schenker', N'Participant Demographic Variables', N'Tenure', 2015, 4, 1424, 0.943671305500331)
GO
SET IDENTITY_INSERT [dbo].[MenuOption] ON 
GO
INSERT [dbo].[MenuOption] ([id], [menuname], [IsActive], [IsHasChild], [ParentId], [URLPath], [menutype], [createdon], [createdby], [modifiedon], [modifiedby], [Orderid], [class]) VALUES (1, N'Dashboard', 1, 0, 0, N'SAReport', N'M', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, 1, NULL)
GO
INSERT [dbo].[MenuOption] ([id], [menuname], [IsActive], [IsHasChild], [ParentId], [URLPath], [menutype], [createdon], [createdby], [modifiedon], [modifiedby], [Orderid], [class]) VALUES (2, N'User', 1, 1, 0, NULL, N'M', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, 2, NULL)
GO
INSERT [dbo].[MenuOption] ([id], [menuname], [IsActive], [IsHasChild], [ParentId], [URLPath], [menutype], [createdon], [createdby], [modifiedon], [modifiedby], [Orderid], [class]) VALUES (3, N'View Users', 1, 0, 2, N'SAECIUser', N'M', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, 3, NULL)
GO
INSERT [dbo].[MenuOption] ([id], [menuname], [IsActive], [IsHasChild], [ParentId], [URLPath], [menutype], [createdon], [createdby], [modifiedon], [modifiedby], [Orderid], [class]) VALUES (4, N'Setup', 1, 0, 0, N'WIP', N'M', CAST(N'2018-07-06T00:00:00.000' AS DateTime), 0, NULL, NULL, 4, N'dripicons-trophy')
GO
SET IDENTITY_INSERT [dbo].[MenuOption] OFF
GO
SET IDENTITY_INSERT [dbo].[ProfileDtls] ON 
GO
INSERT [dbo].[ProfileDtls] ([id], [firstname], [lastname], [email], [createdon], [createdby], [modifiedon], [modifiedby], [Middle_Name], [Tittle], [Division], [Brithday], [Mobile], [Contact_Owner], [Telephone], [Mailing_Street], [Mailing_City], [Mailing_State], [Mailing_Postal_Code], [Mailing_Country], [Website], [Description]) VALUES (1, N'Deepa', N'Marimuthu', N'gdeepabcs@gmail.com', CAST(N'2018-06-14T11:58:54.070' AS DateTime), 3, NULL, NULL, NULL, NULL, NULL, NULL, N'04244540609', NULL, NULL, N'KottaiMuniyappan Street', N'Erode', N'Tamilnadu', 638001, N'India', NULL, NULL)
GO
INSERT [dbo].[ProfileDtls] ([id], [firstname], [lastname], [email], [createdon], [createdby], [modifiedon], [modifiedby], [Middle_Name], [Tittle], [Division], [Brithday], [Mobile], [Contact_Owner], [Telephone], [Mailing_Street], [Mailing_City], [Mailing_State], [Mailing_Postal_Code], [Mailing_Country], [Website], [Description]) VALUES (2, N'Pavithra', NULL, N'deepamarimuthu23@gmail.com', CAST(N'2018-06-14T17:44:18.353' AS DateTime), 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ProfileDtls] OFF
GO
SET IDENTITY_INSERT [dbo].[RoleMaster] ON 
GO
INSERT [dbo].[RoleMaster] ([id], [rolename], [isactive], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, N'SuperAdmin', 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[RoleMaster] ([id], [rolename], [isactive], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, N'ECI User', 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[RoleMaster] ([id], [rolename], [isactive], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (3, N'ECI Client User', 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[RoleMaster] OFF
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, 3, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (3, 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, 3, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, 4, CAST(N'2018-07-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[UserOTP] ([usernum], [opt], [starttime], [endtime]) VALUES (2, N'012882', CAST(N'2018-06-14T14:02:29.590' AS DateTime), CAST(N'2018-06-14T14:07:29.590' AS DateTime))
GO
INSERT [dbo].[UserOTP] ([usernum], [opt], [starttime], [endtime]) VALUES (3, N'650372', CAST(N'2018-06-18T13:03:27.690' AS DateTime), CAST(N'2018-06-18T13:08:27.690' AS DateTime))
GO
INSERT [dbo].[UserOTP] ([usernum], [opt], [starttime], [endtime]) VALUES (4, N'393578', CAST(N'2018-06-14T12:00:34.490' AS DateTime), CAST(N'2018-06-14T12:05:34.490' AS DateTime))
GO
INSERT [dbo].[UserRole] ([UserId], [roleid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, 3, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[UserRole] ([UserId], [roleid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, 3, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[UserRole] ([UserId], [roleid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (3, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[UserRole] ([UserId], [roleid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (5, 3, CAST(N'2018-06-14T11:58:54.263' AS DateTime), 3, NULL, NULL)
GO
INSERT [dbo].[UserRole] ([UserId], [roleid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (5, 3, CAST(N'2018-06-14T17:44:18.400' AS DateTime), 3, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Users] ON 
GO
INSERT [dbo].[Users] ([id], [Accountid], [profileid], [FirstName], [LastName], [EmailId], [Pwd], [UserType], [UserStatus], [CreatedOn], [UpdatedOn], [IpAddress]) VALUES (1, 1, 0, N'Deepa', N'Marimuthu', N'gdeepabcs@gmail.com', N'Pink20', 3, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), NULL, N'122.178.114.114')
GO
INSERT [dbo].[Users] ([id], [Accountid], [profileid], [FirstName], [LastName], [EmailId], [Pwd], [UserType], [UserStatus], [CreatedOn], [UpdatedOn], [IpAddress]) VALUES (2, 2, 0, N'Sririthupa', N'Marimuthu', N'deepamarimuthu@ibbsllp.com', N'12345', 3, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), NULL, N'122.178.83.204')
GO
INSERT [dbo].[Users] ([id], [Accountid], [profileid], [FirstName], [LastName], [EmailId], [Pwd], [UserType], [UserStatus], [CreatedOn], [UpdatedOn], [IpAddress]) VALUES (3, 0, 0, N'Ashok', N'Kumar', N'ashokkumar.s@ibridgellc.com ', N'12345', 2, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), NULL, N'122.178.92.188')
GO
INSERT [dbo].[Users] ([id], [Accountid], [profileid], [FirstName], [LastName], [EmailId], [Pwd], [UserType], [UserStatus], [CreatedOn], [UpdatedOn], [IpAddress]) VALUES (4, 7, 2, N'Deepa', N'Marimuthu', N'gdeepabcs@gmail.com', N'509958', 3, 2, CAST(N'2018-06-14T11:58:54.183' AS DateTime), NULL, N'122.178.83.204')
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
/****** Object:  StoredProcedure [dbo].[GetCategoryList]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Jun.1,2018
-- Description:	[GetCategoryList]
-- =============================================
Create procedure[dbo].[GetCategoryList]
AS
BEGIN

Select * From
CategoryMaster

RETURN

END





GO
/****** Object:  StoredProcedure [dbo].[GetCompanyLists]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Jun.1,2018
-- Description:	[GetCategoryList]
-- =============================================
CREATE procedure[dbo].[GetCompanyLists]
AS
BEGIN

Select * From
Account

RETURN

END





GO
/****** Object:  StoredProcedure [dbo].[GetDataLabelListByCatID]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Jun.1,2018
-- Description:	[GetCategoryList]
-- =============================================
Create procedure[dbo].[GetDataLabelListByCatID]
@catid int
AS
BEGIN

Select * From
DataLabelMaster
Where categoryid = @catid

RETURN

END





GO
/****** Object:  StoredProcedure [dbo].[GetMenusByUser]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Apr.27,2018
-- Description:	GetUsers
-- =============================================
CREATE procedure[dbo].[GetMenusByUser]
@UserNum int
AS
BEGIN
Select M.* from 
UserRole UR
Inner Join RoleMenuRel RM On UR.RoleID = RM.RoleID
Inner Join MenuOption M on M.id = RM.Menuid
Where UR.UserID = @UserNum and MenuType ='M';
	RETURN
END





GO
/****** Object:  StoredProcedure [dbo].[GetReports]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Jun.1,2018
-- Description:	[GetReports]
-- =============================================
CREATE procedure[dbo].[GetReports]
@catid int,
@labelid int,
@accountid int,
@yearid int
AS
BEGIN

declare @catname varchar(512)
set @catname = (Select CategoryName from CategoryMaster where categoryid = @catid);

declare @DataLabelName varchar(512)
set @DataLabelName = (Select DataLabelName from DataLabelMaster where DataLabelID = @labelid);

Select E.companyname, cr.responsevalue as responseid, avg(ReponsePercent)*100 as reportdata  
From ECIData E
inner join Account A on e.companyname = A.companyname
inner join CategoryMaster C on c.CategoryName = e.Category
inner join CategoryResponseMaster CR on C.categoryid = Cr.CategoryId and e.ResponseID = cr.responseid
Where Category = @catname and DataLabel = @DataLabelName and c.categoryid = @catid
and (A.id = @accountid or @accountid = 0) and (E.SurveyYear = @yearid or @yearid =0)
Group by E.companyname,cr.responsevalue order by E.companyname asc
	
RETURN

END





GO
/****** Object:  StoredProcedure [dbo].[GetUserOTP]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[GetUserOTP]
@Usernum int
as
begin
Select * from UserOTP Where UserNum = @Usernum
end
GO
/****** Object:  StoredProcedure [dbo].[GetUsers]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Apr.27,2018
-- Description:	GetUsers
-- =============================================
CREATE procedure[dbo].[GetUsers]
AS
BEGIN
select *,
Case UserStatus
When 0 then 'To Be Verified'
When 1 then 'To Be Change Password'
When 2 then 'Active'
When 3 then 'De-Active'
end as UserStatusVal,
Case UserType
When 1 then 'Super Admin'
When 2 then 'ECI User'
When 3 then 'ECI Client User'
end as UserTypeVal,
IpAddress
 From Users
	
	RETURN
END





GO
/****** Object:  StoredProcedure [dbo].[InsertAccount]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[InsertAccount]
@companyName varchar(512),
@createdby int,
@AccOwner varchar(512),
@Industry varchar(250),
@Employees int,
@Annual_Revenue decimal(10,2),
@Telephone varchar(30),
@Mailing_Street varchar(250),
@Mailing_City varchar(250),
@Mailing_State varchar(250),
@Mailing_Postal_Code varchar(250),
@Mailing_Country varchar(250),
@Website varchar(250),
@Description varchar(530),
@Result int Output
as
begin


INSERT INTO [dbo].[Account]
           ([companyName]
           ,[createdon]
           ,[createdby]
           ,[AccOwner]
           ,[Industry]
           ,[Employees]
           ,[Annual_Revenue]
           ,[Telephone]
           ,[Mailing_Street]
           ,[Mailing_City]
           ,[Mailing_State]
           ,[Mailing_Postal_Code]
           ,[Mailing_Country]
           ,[Website]
           ,[Description])
     VALUES
           (@companyName
           ,getdate()
           ,@createdby         
           ,@AccOwner
           ,@Industry
           ,@Employees
           ,@Annual_Revenue
           ,@Telephone
           ,@Mailing_Street
           ,@Mailing_City
           ,@Mailing_State
           ,@Mailing_Postal_Code
           ,@Mailing_Country
           ,@Website
           ,@Description)


set @Result = 1

Select @Result

end
GO
/****** Object:  StoredProcedure [dbo].[InsertOTP]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[InsertOTP]	
	@UserNum int,
	@otp varchar(10),	
	@Result int Output
as
begin

Delete From UserOTP Where Usernum = @UserNum;

Insert into UserOTP values (@UserNum, @otp, getdate(),  DATEADD(minute,5,GETDATE()))

set @Result = 1

Select @Result

end

GO
/****** Object:  StoredProcedure [dbo].[InsertProfileDtls]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[InsertProfileDtls]
@firstname varchar(150),
@Middle_Name varchar(250),
@lastname varchar(150),
@Tittle varchar(250),
@Division varchar(250),
@email varchar(150),
@Contact_Owner varchar(250),
@Mobile varchar(20),
@Telephone varchar(250),
@Mailing_Street varchar(250),
@Mailing_City varchar(250),
@Mailing_State varchar(250),
@Mailing_Postal_Code varchar(250),
@Mailing_Country varchar(250),
@Website varchar(250),
@Description varchar(530),
@Brithday datetime,
@createdby int,
@pwd varchar(10),
@roleid int,
@companyid int,
@Result int Output
as
begin


INSERT INTO [dbo].[ProfileDtls]
           ([firstname]
           ,[Middle_Name]
           ,[lastname]
           ,[Tittle]
           ,[Division]
           ,[email]
           ,[Contact_Owner]
           ,[Mobile]
           ,[Telephone]
           ,[Mailing_Street]
           ,[Mailing_City]
           ,[Mailing_State]
           ,[Mailing_Postal_Code]
           ,[Mailing_Country]
           ,[Website]
           ,[Description]
		   ,Brithday
		   ,createdon
		   ,createdby)
     VALUES
           (@firstname
           ,@Middle_Name
           ,@lastname        
           ,@Tittle
           ,@Division
           ,@email
           ,@Contact_owner
           ,@Mobile
           ,@Telephone
           ,@Mailing_Street
           ,@Mailing_City
           ,@Mailing_State
           ,@Mailing_Postal_Code
           ,@Mailing_Country
           ,@Website
           ,@Description
		   ,@Brithday
		   ,getdate()
		   ,@createdby)

		   declare @profileid  int
		   set @profileid = (select isnull((max(id) + 1), 1) from ProfileDtls)

		   INSERT INTO [dbo].[Users]
           ([FirstName]
           ,[LastName]
           ,[EmailId]
           ,[Pwd]
           ,[UserType]
           ,[UserStatus]
           ,[CreatedOn]
		   ,accountid
		   ,profileid
		   ,IpAddress)
     VALUES
           (@FirstName
           ,@LastName
           ,@email
           ,@Pwd
           ,@roleid
           ,2
           ,getdate()
		   ,@companyid
		    ,@profileid
			,'')

			declare @userid  int
		   set @userid = (select isnull((max(id) + 1), 1) from Users)

			Insert into userrole(userid, roleid, createdon, createdby) values (@userid, @roleid, getdate(), @createdby)


set @Result = 1

Select @Result

end
GO
/****** Object:  StoredProcedure [dbo].[InsertUserHistory]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[InsertUserHistory]
@usernum int,
@logintime datetime,
@Result int Output
as
begin
declare @count int
set @count = (Select Count(*) from UserHistory where usernum = @usernum and isconnected = 1);

if(@count = 0)
begin
	--insert

	INSERT INTO [dbo].[UserHistory]
           ([usernum]
           ,[logintime]
          ,Isconnected
           )
     VALUES
           (@usernum
           ,getdate()
           ,1 )

		   set @Result = 1
end
else
begin
	set @Result = 2
end

Select @Result

end
GO
/****** Object:  StoredProcedure [dbo].[SaveECIUser]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[SaveECIUser]
	@FirstName varchar(150),
	@LastName varchar(150),
	@EmailId varchar(150),
	@Pwd varchar(max),
	@UserType int,
	@UserStatus int,	
	@Result int Output
as
begin


INSERT INTO [dbo].[Users]
           ([FirstName]
           ,[LastName]
           ,[EmailId]
           ,[Pwd]
           ,[UserType]
           ,[UserStatus]
           ,[CreatedOn])
     VALUES
           (@FirstName
           ,@LastName
           ,@EmailId
           ,@Pwd
           ,@UserType
           ,@UserStatus
           ,getdate())

		   Select @Result
end





GO
/****** Object:  StoredProcedure [dbo].[UpdateIPAddress]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UpdateIPAddress]	
	@UserNum int,
	@IpAddress varchar(20),	
	@otp varchar(10) = null,
	@Result int Output
as
begin

if(@otp is null)
begin
	Update Users Set IpAddress = @IpAddress Where id = @UserNum
end
else
begin
	Update Users Set IpAddress = @IpAddress Where id = @UserNum

	exec InsertOTP @UserNum,@otp,@Result
end


set @Result = 1

Select @Result

end
GO
/****** Object:  StoredProcedure [dbo].[UpdatePwd]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UpdatePwd]	
	@UserNum int,
	@Pwd varchar(max),	
	@Result int Output
as
begin


Update Users Set Pwd = @Pwd Where id = @UserNum

set @Result = 1

Select @Result

end

GO
/****** Object:  StoredProcedure [dbo].[UpdateVerification]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[UpdateVerification]	
	@UserNum int,
	@UserStatus int,	
	@Result int Output
as
begin


Update Users Set UserStatus = @UserStatus Where id = @UserNum

set @Result = 1

Select @Result

end





GO
/****** Object:  StoredProcedure [dbo].[ValidateUser]    Script Date: 6/18/2018 1:48:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Apr.27,2018
-- Description:	GetUsers
-- =============================================
CREATE procedure[dbo].[ValidateUser]
@EmailId varchar(150),
@Pwd varchar(max),
@usernum int = null
AS
BEGIN
if(@usernum = 0)
begin
select *,
Case UserStatus
When 0 then 'To Be Verified'
When 1 then 'To Be Change Password'
When 2 then 'Active'
When 3 then 'De-Active'
end as UserStatusVal,
Case UserType
When 1 then 'Super Admin'
When 2 then 'ECI User'
When 3 then 'ECI Client User'
end as UserTypeVal,
IpAddress
 From Users
	Where emailid = @EmailId and pwd = @Pwd
	end
	else
	begin
	select *,
Case UserStatus
When 0 then 'To Be Verified'
When 1 then 'To Be Change Password'
When 2 then 'Active'
When 3 then 'De-Active'
end as UserStatusVal,
Case UserType
When 1 then 'Super Admin'
When 2 then 'ECI User'
When 3 then 'ECI Client User'
end as UserTypeVal,
IpAddress
 From Users
	Where id = @usernum
	end
	RETURN
END





GO
