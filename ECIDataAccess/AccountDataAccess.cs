﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using ECIModel;
using ECIBusinessLogic;

namespace ECIDataAccess
{
    public interface IAccountDataAccess
    {
        int AddAccount(AccountModel accountModel);
        DataTable GetAccountbyid(int accid);
        int UpdateAccount(AccountModel accountModel);
    }
    public class AccountDataAccess : BaseDataAccess, IAccountDataAccess, IDisposable
    {
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {

                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public int AddAccount(AccountModel accountModel)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("companyName", accountModel.companyName));
                parameterList.Add(base.GetParameter("createdby", accountModel.createdby));
                parameterList.Add(base.GetParameter("AccOwner", accountModel.AccOwner));
                parameterList.Add(base.GetParameter("Industry", accountModel.Industry));
                parameterList.Add(base.GetParameter("Employees", accountModel.Employees));
                parameterList.Add(base.GetParameter("Annual_Revenue", accountModel.Annual_Revenue));
                parameterList.Add(base.GetParameter("Telephone", accountModel.Telephone));
                parameterList.Add(base.GetParameter("Mailing_Street", accountModel.Mailing_Street));
                parameterList.Add(base.GetParameter("Mailing_City", accountModel.Mailing_City));
                parameterList.Add(base.GetParameter("Mailing_State", accountModel.Mailing_State));
                parameterList.Add(base.GetParameter("Mailing_Postal_Code", accountModel.Mailing_Postal_Code));
                parameterList.Add(base.GetParameter("Mailing_Country", accountModel.Mailing_Country));
                parameterList.Add(base.GetParameter("Website", accountModel.Website));
                parameterList.Add(base.GetParameter("Description", accountModel.Description));
                parameterList.Add(base.GetParameter("AliasName", accountModel.AliasName));

                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("InsertAccount", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public DataTable GetAccountbyid(int accid)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("accid", accid));
                DbDataReader dbDataReader = base.GetDataReader("GetAccountbyid", parameterList, CommandType.StoredProcedure);
                dt.Load(dbDataReader);
                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
        public int UpdateAccount(AccountModel accountModel)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("accid", accountModel.id));
                parameterList.Add(base.GetParameter("companyName", accountModel.companyName));
                parameterList.Add(base.GetParameter("createdby", accountModel.createdby));
                parameterList.Add(base.GetParameter("AccOwner", accountModel.AccOwner));
                parameterList.Add(base.GetParameter("Industry", accountModel.Industry));
                parameterList.Add(base.GetParameter("Employees", accountModel.Employees));
                parameterList.Add(base.GetParameter("Annual_Revenue", accountModel.Annual_Revenue));
                parameterList.Add(base.GetParameter("Telephone", accountModel.Telephone));
                parameterList.Add(base.GetParameter("Mailing_Street", accountModel.Mailing_Street));
                parameterList.Add(base.GetParameter("Mailing_City", accountModel.Mailing_City));
                parameterList.Add(base.GetParameter("Mailing_State", accountModel.Mailing_State));
                parameterList.Add(base.GetParameter("Mailing_Postal_Code", accountModel.Mailing_Postal_Code));
                parameterList.Add(base.GetParameter("Mailing_Country", accountModel.Mailing_Country));
                parameterList.Add(base.GetParameter("Website", accountModel.Website));
                parameterList.Add(base.GetParameter("Description", accountModel.Description));
                parameterList.Add(base.GetParameter("AliasName", accountModel.AliasName));

                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("UpdateAccount", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
    }
}
