﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using ECIModel;
using ECIBusinessLogic;

namespace ECIDataAccess
{
    public interface IReportAccess
    {
        List<Dictionary<string, object>> GetTableRows(DataTable dtData);
        DataTable GetCategoryList(int accountid, string yearid, int accountid1, string yearid1);
        DataTable GetCategoryList1();
        DataTable GetTypeCounts(int usernum);
        DataTable GetWidgetsReports(int usernum);
        DataTable GetECIFilter(int catid, string labelid, int accountid, string yearid, int accountid1, string yearid1,int Yearcount);
        DataTable GetECIFilterReportBuilderNew(int catid, string labelid, int accountid, string yearid, int accountid1, string yearid1, int Yearcount,int accountid2,string yearidexist);
        DataTable GetDataLabelListByCatID(int CategoryID,string SectionID);
        DataTable GetCatSectionListByCatID(int CategoryID);
        
        DataTable GetReports(int catid, int labelid, int accountid,int yearid);
        DataTable GetNewDashboardReport(int catid, string labelid, int accountid, string yearid, int accountid1, string yearid1);
        DataTable GetCompanyLists(int Usernum);
        DataTable GetCompanyListsAll();
        
        DataTable GetCompanyCompare();
        DataTable GetYear(int companyid);
        string AddUserPref(string xmlparamval, string xmlmatricsval, int Usernum, string ReportName, int CreatedBy,int SaveStatus,string Xaxis,string Yaxis);
        IEnumerable<UserRefModel> GetUserReports(int usernum);
        DataSet Getreportdata(int ReportId);
        DataTable GetNewDashboardReportOverall(int catid, int labelid, int accountid, int yearid, int accountid1, int yearid1);
        DataTable GetNewDashboardReportByFilter(int catid, string labelid, int accountid, string yearid, string ddlindustry_arry, string ddlcmpysize_arry, string ddlrevenue_arry, string ddlagebrands_arry, string ddlgender_arry, string ddlrace_arry, string ddlgeoregion_arry, string ddlsupplier_arry, string ddlsupervisor_arry, string ddlunion_arry, string ddlpublicprivate_arry, string ddltenure_arry, string ddlsalaried_arry, string ddlmanageval_arry, string ddlmarketval_arry, int accountid1, string yearid1);
        DataTable GetReportBuilderNewReport(int catid, string labelid, int accountid, string yearid, string ddlindustry_arry, string ddlcmpysize_arry, string ddlrevenue_arry, string ddlagebrands_arry, string ddlgender_arry, string ddlrace_arry, string ddlgeoregion_arry, string ddlsupplier_arry, string ddlsupervisor_arry, string ddlunion_arry, string ddlpublicprivate_arry, string ddltenure_arry, string ddlsalaried_arry, string ddlmanageval_arry, string ddlmarketval_arry, int accountid1, string yearid1, int accountid2, string yearid2);
        DataTable GetNewDashboardOverallByFilter(int catid, int labelid, int accountid, int yearid, string ddlindustry_arry, string ddlcmpysize_arry, string ddlrevenue_arry, string ddlagebrands_arry, string ddlgender_arry, string ddlrace_arry, string ddlgeoregion_arry, string ddlsupplier_arry, string ddlsupervisor_arry, string ddlunion_arry, string ddlpublicprivate_arry, string ddltenure_arry, string ddlsalaried_arry, string ddlmanageval_arry, string ddlmarketval_arry, int accountid1, int yearid1);
        int AddFilterSettings(int FiltId,int Usernum, int catid, string labelid, int accountid, string yearid, string FiltReportName, int CreatedBy, string ddlindustry_arry, string ddlcmpysize_arry, string ddlrevenue_arry, string ddlagebrands_arry, string ddlgender_arry, string ddlrace_arry, string ddlgeoregion_arry, string ddlsupplier_arry, string ddlsupervisor_arry, string ddlunion_arry, string ddlpublicprivate_arry, string ddltenure_arry, string ddlsalaried_arry, string ddlmanageval_arry, string ddlmarketval_arry, int comparativecompanyName,int funtype,string comparativeyear);
        int AddBuilderFilterSettings(int FiltId, int Usernum, int catid, string labelid, int accountid, string yearid, string FiltReportName, int CreatedBy, string ddlindustry_arry, string ddlcmpysize_arry, string ddlrevenue_arry, string ddlagebrands_arry, string ddlgender_arry, string ddlrace_arry, string ddlgeoregion_arry, string ddlsupplier_arry, string ddlsupervisor_arry, string ddlunion_arry, string ddlpublicprivate_arry, string ddltenure_arry, string ddlsalaried_arry, string ddlmanageval_arry, string ddlmarketval_arry, int comparativecompanyName, int funtype, string comparativeyear, int accountid2, string yearid2,string FolderPath);
        DataTable GetFilterSettingsByUserNum(int usernum);
        DataTable GetFilterSettingsByFiltReportid(int FiltReportid);
        DataSet GetUsersReport();
        string BulkDataUpload(string PathUrl);
        string GetChartversionname(int usernum, string chartname);
        int InsertChartImageData(string chartname, string imgdata, string storedpath, int usernum);
        int UpdateChartImageData(int ReportSaveGetId, string imgdata, string storedpath, int usernum);
        DataTable GetSavedcharts(int usernum, string storedpath);
        int CheckReportName(int UserNum, string ReportName);
    }
    public class ReportAccess : BaseDataAccess, IReportAccess, IDisposable
    {
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {

                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public List<Dictionary<string, object>> GetTableRows(DataTable dtData)
        {
            List<Dictionary<string, object>>
            lstRows = new List<Dictionary<string, object>>();
            Dictionary<string, object> dictRow = null;

            foreach (DataRow dr in dtData.Rows)
            {
                dictRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtData.Columns)
                {
                    dictRow.Add(col.ColumnName, dr[col]);
                }
                lstRows.Add(dictRow);
            }
            return lstRows;
        }
        public DataTable GetReports(int catid, int labelid, int accountid, int yearid)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("catid", catid));
                parameterList.Add(base.GetParameter("labelid", labelid));
                parameterList.Add(base.GetParameter("accountid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                DbDataReader dbDataReader = base.GetDataReader("GetReports", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetNewDashboardReport(int catid, string labelid, int accountid, string yearid, int accountid1, string yearid1)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("catid", catid));
                parameterList.Add(base.GetParameter("labelid", labelid));
                parameterList.Add(base.GetParameter("accountid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                parameterList.Add(base.GetParameter("accountid1", accountid1));
                parameterList.Add(base.GetParameter("yearid1", yearid1));

                DbDataReader dbDataReader = base.GetDataReader("GetNewDashboardReport", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetCategoryList1()
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
          

                DbDataReader dbDataReader = base.GetDataReader("GetCategoryList1", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetCategoryList(int accountid, string yearid, int accountid1, string yearid1)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();              
                parameterList.Add(base.GetParameter("companyid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                parameterList.Add(base.GetParameter("companyid1", accountid1));
                parameterList.Add(base.GetParameter("yearid1", yearid1));

                DbDataReader dbDataReader = base.GetDataReader("GetCategoryList", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetDataLabelListByCatID(int CategoryID, string SectionID)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("catid", CategoryID));
                parameterList.Add(base.GetParameter("secid", SectionID));

                DbDataReader dbDataReader = base.GetDataReader("GetDataLabelListByCatID", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetCatSectionListByCatID(int CategoryID)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("catid", CategoryID));

                DbDataReader dbDataReader = base.GetDataReader("GetCatSectionListByCatID", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetCompanyLists(int Usernum)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("Usernum", Usernum));

                DbDataReader dbDataReader = base.GetDataReader("GetCompanyLists", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetCompanyListsAll()
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();


                DbDataReader dbDataReader = base.GetDataReader("GetCompanyListsAll", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetCompanyCompare()
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                DbDataReader dbDataReader = base.GetDataReader("GetCompanycompare", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetYear(int companyid)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("Companyid", companyid));
                DbDataReader dbDataReader = base.GetDataReader("GetYear", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public string AddUserPref(string xmlparamval, string xmlmatricsval, int Usernum, string ReportName, int CreatedBy,int SaveStatus, string Xaxis, string Yaxis)
        {
            string Result ="";
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("xmlparamval", xmlparamval));
                parameterList.Add(base.GetParameter("xmlmatricsval", xmlmatricsval));
                parameterList.Add(base.GetParameter("Usernum", Usernum));
                parameterList.Add(base.GetParameter("ReportName", ReportName));
                parameterList.Add(base.GetParameter("CreatedBy ", CreatedBy));
                parameterList.Add(base.GetParameter("SaveStatus", SaveStatus));
                parameterList.Add(base.GetParameter("Xaxis ", Xaxis));
                parameterList.Add(base.GetParameter("Yaxis", Yaxis));


                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.VarChar,"");
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("SaveUserReference", parameterList, CommandType.StoredProcedure);

                Result = TestIdParamter.Value.ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public IEnumerable<UserRefModel> GetUserReports(int usernum)
        {
            var list = new List<UserRefModel>();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("UserNum ", usernum));

                DbDataReader dbDataReader = base.GetDataReader("GetUserReports", parameterList, CommandType.StoredProcedure);

                while (dbDataReader.Read())
                {
                    UserRefModel userModel = new UserRefModel();
                    userModel.ReportName = dbDataReader["ReportName"].ToString();
                    
                    userModel.Reportid = int.Parse(dbDataReader["Reportid"].ToString());

                    list.Add(userModel);
                }

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }       
        public DataSet Getreportdata(int ReportId)
        {
            DataSet dt = new DataSet();
            DataTable DataTable = new DataTable();
            DataTable ChartTable = new DataTable();

            // This information is cosmetic, only.
            DataTable.TableName = "DataTable";
            ChartTable.TableName = "ChartTable";

            dt.Tables.Add(DataTable);
            dt.Tables.Add(ChartTable);
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("reportid", ReportId));

                DbDataReader dbDataReader = base.GetDataReader("getreportdata", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader,LoadOption.PreserveChanges, DataTable, ChartTable);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetNewDashboardReportOverall(int catid, int labelid, int accountid, int yearid, int accountid1, int yearid1)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("catid", catid));
                parameterList.Add(base.GetParameter("labelid", labelid));
                parameterList.Add(base.GetParameter("accountid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                parameterList.Add(base.GetParameter("accountid1", accountid1));
                parameterList.Add(base.GetParameter("yearid1", yearid1));
                DbDataReader dbDataReader = base.GetDataReader("GetNewDashboardReportOverall", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetECIFilter(int catid, string labelid, int accountid, string yearid, int accountid1, string yearid1, int Yearcount)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("catid", catid));
                parameterList.Add(base.GetParameter("labelid", labelid));
                parameterList.Add(base.GetParameter("accountid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                parameterList.Add(base.GetParameter("accountid1", accountid1));
                parameterList.Add(base.GetParameter("yearid1", yearid1));
                parameterList.Add(base.GetParameter("Yearcount", Yearcount));
               
                DbDataReader dbDataReader = base.GetDataReader("GetECIFilter", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetECIFilterReportBuilderNew(int catid, string labelid, int accountid, string yearid, int accountid1, string yearid1, int Yearcount,int accountid2, string yearid2)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("catid", catid));
                parameterList.Add(base.GetParameter("labelid", labelid));
                parameterList.Add(base.GetParameter("accountid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                parameterList.Add(base.GetParameter("accountid1", accountid1));
                parameterList.Add(base.GetParameter("yearid1", yearid1));
                parameterList.Add(base.GetParameter("Yearcount", Yearcount));
                parameterList.Add(base.GetParameter("accountid2", accountid2));
                parameterList.Add(base.GetParameter("yearid2", yearid2));

                DbDataReader dbDataReader = base.GetDataReader("GetECIFilterReportBuilderNew", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetNewDashboardReportByFilter(int catid, string labelid, int accountid, string yearid, string ddlindustry_arry, string ddlcmpysize_arry, string ddlrevenue_arry, string ddlagebrands_arry, string ddlgender_arry, string ddlrace_arry, string ddlgeoregion_arry, string ddlsupplier_arry, string ddlsupervisor_arry, string ddlunion_arry, string ddlpublicprivate_arry, string ddltenure_arry, string ddlsalaried_arry, string ddlmanageval_arry, string ddlmarketval_arry, int accountid1, string yearid1)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("catid", catid));
                parameterList.Add(base.GetParameter("labelid", labelid));
                parameterList.Add(base.GetParameter("accountid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                parameterList.Add(base.GetParameter("accountid1", accountid1));
                parameterList.Add(base.GetParameter("yearid1", yearid1));

                //parameterList.Add(base.GetParameter("ddlindustry_arry", ddlindustry_arry));
                parameterList.Add(base.GetParameter("ddlindustry_arry", ddlindustry_arry));
                parameterList.Add(base.GetParameter("ddlcmpysize_arry", ddlcmpysize_arry));
                parameterList.Add(base.GetParameter("ddlrevenue_arry", ddlrevenue_arry));
                parameterList.Add(base.GetParameter("ddlagebrands_arry", ddlagebrands_arry));
                parameterList.Add(base.GetParameter("ddlgender_arry", ddlgender_arry));
                parameterList.Add(base.GetParameter("ddlrace_arry", ddlrace_arry));
                parameterList.Add(base.GetParameter("ddlgeoregion_arry", ddlgeoregion_arry));
                parameterList.Add(base.GetParameter("ddlsupplier_arry", ddlsupplier_arry));
                parameterList.Add(base.GetParameter("ddlsupervisor_arry", ddlsupervisor_arry));
                parameterList.Add(base.GetParameter("ddlunion_arry", ddlunion_arry));
                parameterList.Add(base.GetParameter("ddlpublicprivate_arry", ddlpublicprivate_arry));
                parameterList.Add(base.GetParameter("ddltenure_arry", ddltenure_arry));
                parameterList.Add(base.GetParameter("ddlsalaried_arry", ddlsalaried_arry));
                parameterList.Add(base.GetParameter("ddlmanageval_arry", ddlmanageval_arry));
                parameterList.Add(base.GetParameter("ddlmarketval_arry", ddlmarketval_arry));

                DbDataReader dbDataReader = base.GetDataReader("GetNewDashboardReportByFilter", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetReportBuilderNewReport(int catid, string labelid, int accountid, string yearid, string ddlindustry_arry, string ddlcmpysize_arry, string ddlrevenue_arry, string ddlagebrands_arry, string ddlgender_arry, string ddlrace_arry, string ddlgeoregion_arry, string ddlsupplier_arry, string ddlsupervisor_arry, string ddlunion_arry, string ddlpublicprivate_arry, string ddltenure_arry, string ddlsalaried_arry, string ddlmanageval_arry, string ddlmarketval_arry, int accountid1, string yearid1, int accountid2, string yearid2)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("catid", catid));
                parameterList.Add(base.GetParameter("labelid", labelid));
                parameterList.Add(base.GetParameter("accountid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                parameterList.Add(base.GetParameter("accountid1", accountid1));
                parameterList.Add(base.GetParameter("yearid1", yearid1));
                parameterList.Add(base.GetParameter("accountid2", accountid2));
                parameterList.Add(base.GetParameter("yearid2", yearid2));

                //parameterList.Add(base.GetParameter("ddlindustry_arry", ddlindustry_arry));
                parameterList.Add(base.GetParameter("ddlindustry_arry", ddlindustry_arry));
                parameterList.Add(base.GetParameter("ddlcmpysize_arry", ddlcmpysize_arry));
                parameterList.Add(base.GetParameter("ddlrevenue_arry", ddlrevenue_arry));
                parameterList.Add(base.GetParameter("ddlagebrands_arry", ddlagebrands_arry));
                parameterList.Add(base.GetParameter("ddlgender_arry", ddlgender_arry));
                parameterList.Add(base.GetParameter("ddlrace_arry", ddlrace_arry));
                parameterList.Add(base.GetParameter("ddlgeoregion_arry", ddlgeoregion_arry));
                parameterList.Add(base.GetParameter("ddlsupplier_arry", ddlsupplier_arry));
                parameterList.Add(base.GetParameter("ddlsupervisor_arry", ddlsupervisor_arry));
                parameterList.Add(base.GetParameter("ddlunion_arry", ddlunion_arry));
                parameterList.Add(base.GetParameter("ddlpublicprivate_arry", ddlpublicprivate_arry));
                parameterList.Add(base.GetParameter("ddltenure_arry", ddltenure_arry));
                parameterList.Add(base.GetParameter("ddlsalaried_arry", ddlsalaried_arry));
                parameterList.Add(base.GetParameter("ddlmanageval_arry", ddlmanageval_arry));
                parameterList.Add(base.GetParameter("ddlmarketval_arry", ddlmarketval_arry));

                DbDataReader dbDataReader = base.GetDataReader("GetReportBuilderNewReport", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetNewDashboardOverallByFilter(int catid, int labelid, int accountid, int yearid, string ddlindustry_arry, string ddlcmpysize_arry, string ddlrevenue_arry, string ddlagebrands_arry, string ddlgender_arry, string ddlrace_arry, string ddlgeoregion_arry, string ddlsupplier_arry, string ddlsupervisor_arry, string ddlunion_arry, string ddlpublicprivate_arry, string ddltenure_arry, string ddlsalaried_arry, string ddlmanageval_arry, string ddlmarketval_arry, int accountid1, int yearid1)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("catid", catid));
                parameterList.Add(base.GetParameter("labelid", labelid));
                parameterList.Add(base.GetParameter("accountid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                parameterList.Add(base.GetParameter("accountid1", accountid1));
                parameterList.Add(base.GetParameter("yearid1", yearid1));
                //parameterList.Add(base.GetParameter("ddlindustry_arry", ddlindustry_arry));
                parameterList.Add(base.GetParameter("ddlindustry_arry", ddlindustry_arry));
                parameterList.Add(base.GetParameter("ddlcmpysize_arry", ddlcmpysize_arry));
                parameterList.Add(base.GetParameter("ddlrevenue_arry", ddlrevenue_arry));
                parameterList.Add(base.GetParameter("ddlagebrands_arry", ddlagebrands_arry));
                parameterList.Add(base.GetParameter("ddlgender_arry", ddlgender_arry));
                parameterList.Add(base.GetParameter("ddlrace_arry", ddlrace_arry));
                parameterList.Add(base.GetParameter("ddlgeoregion_arry", ddlgeoregion_arry));
                parameterList.Add(base.GetParameter("ddlsupplier_arry", ddlsupplier_arry));
                parameterList.Add(base.GetParameter("ddlsupervisor_arry", ddlsupervisor_arry));
                parameterList.Add(base.GetParameter("ddlunion_arry", ddlunion_arry));
                parameterList.Add(base.GetParameter("ddlpublicprivate_arry", ddlpublicprivate_arry));
                parameterList.Add(base.GetParameter("ddltenure_arry", ddltenure_arry));
                parameterList.Add(base.GetParameter("ddlsalaried_arry", ddlsalaried_arry));
                parameterList.Add(base.GetParameter("ddlmanageval_arry", ddlmanageval_arry));
                parameterList.Add(base.GetParameter("ddlmarketval_arry", ddlmarketval_arry));
                DbDataReader dbDataReader = base.GetDataReader("GetNewDashboardOverallByFilter", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetTypeCounts(int usernum)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("UserNum ", usernum));

                DbDataReader dbDataReader = base.GetDataReader("GetTypeCounts", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetWidgetsReports(int usernum)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("UserNum ", usernum));

                DbDataReader dbDataReader = base.GetDataReader("GetUserReports", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public int AddFilterSettings(int FiltId, int Usernum, int catid, string labelid, int accountid, string yearid, string FiltReportName, int CreatedBy, string ddlindustry_arry, string ddlcmpysize_arry, string ddlrevenue_arry, string ddlagebrands_arry, string ddlgender_arry, string ddlrace_arry, string ddlgeoregion_arry, string ddlsupplier_arry, string ddlsupervisor_arry, string ddlunion_arry, string ddlpublicprivate_arry, string ddltenure_arry, string ddlsalaried_arry, string ddlmanageval_arry, string ddlmarketval_arry, int comparativecompanyName, int funtype, string comparativeyear)

        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("FiltId", FiltId));
                parameterList.Add(base.GetParameter("Usernum", Usernum));
                parameterList.Add(base.GetParameter("catid", catid));
                parameterList.Add(base.GetParameter("labelid", labelid));
                parameterList.Add(base.GetParameter("accountid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                parameterList.Add(base.GetParameter("FiltReportName", FiltReportName));
                parameterList.Add(base.GetParameter("CreatedBy", CreatedBy));
                parameterList.Add(base.GetParameter("ddlindustry_arry", ddlindustry_arry));
                parameterList.Add(base.GetParameter("ddlcmpysize_arry", ddlcmpysize_arry));
                parameterList.Add(base.GetParameter("ddlrevenue_arry", ddlrevenue_arry));
                parameterList.Add(base.GetParameter("ddlagebrands_arry", ddlagebrands_arry));
                parameterList.Add(base.GetParameter("ddlgender_arry", ddlgender_arry));
                parameterList.Add(base.GetParameter("ddlrace_arry", ddlrace_arry));
                parameterList.Add(base.GetParameter("ddlgeoregion_arry", ddlgeoregion_arry));
                parameterList.Add(base.GetParameter("ddlsupplier_arry", ddlsupplier_arry));
                parameterList.Add(base.GetParameter("ddlsupervisor_arry", ddlsupervisor_arry));
                parameterList.Add(base.GetParameter("ddlunion_arry", ddlunion_arry));
                parameterList.Add(base.GetParameter("ddlpublicprivate_arry", ddlpublicprivate_arry));
                parameterList.Add(base.GetParameter("ddltenure_arry", ddltenure_arry));
                parameterList.Add(base.GetParameter("ddlsalaried_arry", ddlsalaried_arry));
                parameterList.Add(base.GetParameter("ddlmanageval_arry", ddlmanageval_arry));
                parameterList.Add(base.GetParameter("ddlmarketval_arry", ddlmarketval_arry));
                parameterList.Add(base.GetParameter("comparativecompanyName", comparativecompanyName));
                parameterList.Add(base.GetParameter("funtype", funtype));
                parameterList.Add(base.GetParameter("comparativeyear", comparativeyear));



                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("InsertFilterReports", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public int AddBuilderFilterSettings(int FiltId, int Usernum, int catid, string labelid, int accountid, string yearid, string FiltReportName, int CreatedBy, string ddlindustry_arry, string ddlcmpysize_arry, string ddlrevenue_arry, string ddlagebrands_arry, string ddlgender_arry, string ddlrace_arry, string ddlgeoregion_arry, string ddlsupplier_arry, string ddlsupervisor_arry, string ddlunion_arry, string ddlpublicprivate_arry, string ddltenure_arry, string ddlsalaried_arry, string ddlmanageval_arry, string ddlmarketval_arry, int comparativecompanyName, int funtype, string comparativeyear, int accountid2, string yearid2, string FolderPath)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("FiltId", FiltId));
                parameterList.Add(base.GetParameter("Usernum", Usernum));
                parameterList.Add(base.GetParameter("catid", catid));
                parameterList.Add(base.GetParameter("labelid", labelid));
                parameterList.Add(base.GetParameter("accountid", accountid));
                parameterList.Add(base.GetParameter("yearid", yearid));
                parameterList.Add(base.GetParameter("FiltReportName", FiltReportName));
                parameterList.Add(base.GetParameter("CreatedBy", CreatedBy));
                parameterList.Add(base.GetParameter("ddlindustry_arry", ddlindustry_arry));
                parameterList.Add(base.GetParameter("ddlcmpysize_arry", ddlcmpysize_arry));
                parameterList.Add(base.GetParameter("ddlrevenue_arry", ddlrevenue_arry));
                parameterList.Add(base.GetParameter("ddlagebrands_arry", ddlagebrands_arry));
                parameterList.Add(base.GetParameter("ddlgender_arry", ddlgender_arry));
                parameterList.Add(base.GetParameter("ddlrace_arry", ddlrace_arry));
                parameterList.Add(base.GetParameter("ddlgeoregion_arry", ddlgeoregion_arry));
                parameterList.Add(base.GetParameter("ddlsupplier_arry", ddlsupplier_arry));
                parameterList.Add(base.GetParameter("ddlsupervisor_arry", ddlsupervisor_arry));
                parameterList.Add(base.GetParameter("ddlunion_arry", ddlunion_arry));
                parameterList.Add(base.GetParameter("ddlpublicprivate_arry", ddlpublicprivate_arry));
                parameterList.Add(base.GetParameter("ddltenure_arry", ddltenure_arry));
                parameterList.Add(base.GetParameter("ddlsalaried_arry", ddlsalaried_arry));
                parameterList.Add(base.GetParameter("ddlmanageval_arry", ddlmanageval_arry));
                parameterList.Add(base.GetParameter("ddlmarketval_arry", ddlmarketval_arry));
                parameterList.Add(base.GetParameter("comparativecompanyName", comparativecompanyName));
                parameterList.Add(base.GetParameter("funtype", funtype));
                parameterList.Add(base.GetParameter("comparativeyear", comparativeyear));
                parameterList.Add(base.GetParameter("accountid2", accountid2));
                parameterList.Add(base.GetParameter("yearid2", yearid2));
                parameterList.Add(base.GetParameter("FolderPath", FolderPath));


                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("InsertBuildFilterReports", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public DataTable GetFilterSettingsByUserNum(int usernum)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("Usernum ", usernum));

                DbDataReader dbDataReader = base.GetDataReader("GetFilterSettingsByUserNum", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetFilterSettingsByFiltReportid(int FiltReportid)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("FiltReportId ", FiltReportid));

                DbDataReader dbDataReader = base.GetDataReader("GetFilterSettingsByFiltReportid", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataSet GetUsersReport()
        {
            DataSet dt = new DataSet();
            DataTable DataTable = new DataTable();
            DataTable ChartTable = new DataTable();

            // This information is cosmetic, only.
            DataTable.TableName = "CompanyUserTable";
            ChartTable.TableName = "UserTypeTable";

            dt.Tables.Add(DataTable);
            dt.Tables.Add(ChartTable);
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                DbDataReader dbDataReader = base.GetDataReader("UsersReport", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader, LoadOption.PreserveChanges, DataTable, ChartTable);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public string BulkDataUpload(string PathUrl)
        {
            string Result = "";
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("PathUrl", PathUrl));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.VarChar, "");
                parameterList.Add(TestIdParamter);
                base.ExecuteNonQuery("BulkDataUpload", parameterList, CommandType.StoredProcedure);
                Result = TestIdParamter.Value.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public string GetChartversionname(int usernum, string chartname)
        {
            string Result = "";
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
             
                parameterList.Add(base.GetParameter("usernum", usernum));
                parameterList.Add(base.GetParameter("chartname", chartname));

                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.VarChar, "");
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("GetChartversionname", parameterList, CommandType.StoredProcedure);

                Result = TestIdParamter.Value.ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public int InsertChartImageData(string chartname, string imgdata, string storedpath, int usernum)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("chartname", chartname));
                parameterList.Add(base.GetParameter("imgdata", imgdata));
                parameterList.Add(base.GetParameter("storedpath", storedpath));
                parameterList.Add(base.GetParameter("usernum", usernum));


                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("InsertChartImageData", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public int UpdateChartImageData(int ReportSaveGetId, string imgdata, string storedpath, int usernum)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("ReportSaveGetId", ReportSaveGetId));
                parameterList.Add(base.GetParameter("imgdata", imgdata));
                parameterList.Add(base.GetParameter("storedpath", storedpath));
                parameterList.Add(base.GetParameter("usernum", usernum));


                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("UpdateChartImageData", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }

        public DataTable GetSavedcharts(int usernum, string storedpath)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("usernum ", usernum));
                parameterList.Add(base.GetParameter("storedpath ", storedpath));

                DbDataReader dbDataReader = base.GetDataReader("GetSavedcharts", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public int CheckReportName(int UserNum,string ReportName)
        {

            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("UserNum", UserNum));
                parameterList.Add(base.GetParameter("ReportName", ReportName));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("CheckReportName", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
    }
}
