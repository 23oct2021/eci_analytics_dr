﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using ECIModel;
using ECIBusinessLogic;
using System.Globalization;

namespace ECIDataAccess
{
    public interface IUserDataAccess
    {
        int Deleteuser(int Userid);
        int checkmailid(string Email);
        int checkaccount(string companyname);
        int Deleteaccount(int accid);
        int DeleteRole(int RoleId);
        int AddECIUser(UserModel userModel);
        List<UserModel> GetUsers();
        int UpdateVerification(UserModel userModel);
        int UpdatePwd(UserModel userModel);
        UserModel ValidateUser(string emailid, string pwd, int usernum);
        int UpdateIPAddress(UserModel userModel);
        int InsertOTP(UserModel userModel);
        DataTable GetMenusByUser(int usernum);
        IEnumerable<AccountModel> GetCompanyListsAll();
        int AddProfile(ProfileModel profileModel);
        int GetUserByEmailId(string emailid);
        int IsSameUserLoggedIn(string emailid, string Pass, string IpAddress);
        int InsertUserHistory(UserModel userModel);
        int UpdateUserHistory(int UserNum);
        string GetSessionidByUserNum(int usernum);
        DataTable GetProfilebyuserid(int Userid);
        int UpdateProfile(ProfileModel profileModel);
        DataTable GetUserFolders(int Userid);
        int InsertUserFolder(string foldername, int usernum, int parentfolderid);
        string IsValidSession(int usernum, int resetsession);
    }

    public class UserDataAccess : BaseDataAccess, IUserDataAccess, IDisposable
    {
        public int AddECIUser( UserModel userModel)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("FirstName", userModel.FirstName));
                parameterList.Add(base.GetParameter("LastName", userModel.LastName));
                parameterList.Add(base.GetParameter("Emailid", userModel.Emailid));
                parameterList.Add(base.GetParameter("Pwd", userModel.Password));
                parameterList.Add(base.GetParameter("UserType", userModel.UserType));
                parameterList.Add(base.GetParameter("UserStatus", userModel.UserStatus));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("SaveECIUser", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;

                
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public int Deleteuser(int Userid)
        {

            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("UserId", Userid));      
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("DeleteUser", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }

        public int checkmailid(string Email)
        {

            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("Email", SecureLogic.Encrypt(Email)));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("CheckMailSp", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public int checkaccount(string companyname)
        {

            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("companyname", companyname));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("CheckAccountSp", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public int Deleteaccount(int accid)
        {

            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("accid", accid));      
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("Deleteaccount", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public int DeleteRole(int RoleId)
        {

            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("RoleId", RoleId));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("DeleteRole", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public List<UserModel> GetUsers()
        {
            List<UserModel> list = new List<UserModel>();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                DbDataReader dbDataReader = base.GetDataReader("GetUsers", parameterList, CommandType.StoredProcedure);

                while(dbDataReader.Read())
                {
                    UserModel userModel = new UserModel();
                    userModel.FirstName =SecureLogic.Decrypt(dbDataReader["FirstName"].ToString());
                    userModel.LastName = SecureLogic.Decrypt(dbDataReader["LastName"].ToString());
                    userModel.Emailid = SecureLogic.Decrypt(dbDataReader["EmailId"].ToString());
                    userModel.Accountname = dbDataReader["companyName"].ToString();
                    userModel.UserStatus = int.Parse(dbDataReader["UserStatus"].ToString());
                    userModel.UserType = int.Parse(dbDataReader["UserType"].ToString());
                    userModel.UserTypeVal = dbDataReader["UserTypeVal"].ToString();
                    userModel.UserStatusVal = dbDataReader["UserStatusVal"].ToString();
                    userModel.UserNum = int.Parse(dbDataReader["id"].ToString());
                    list.Add(userModel);
                }

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {

                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public int UpdateVerification(UserModel userModel)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("UserNum", userModel.UserNum));
                parameterList.Add(base.GetParameter("UserStatus", userModel.UserStatus));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("UpdateVerification", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public int UpdatePwd(UserModel userModel)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("UserNum", userModel.UserNum));
                parameterList.Add(base.GetParameter("Pwd", SecureLogic.Encrypt(userModel.Password)));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("UpdatePwd", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public UserModel ValidateUser(string emailid, string pwd, int usernum)
        {
            UserModel userModel = new UserModel();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("EmailId", SecureLogic.Encrypt(emailid)));
                parameterList.Add(base.GetParameter("Pwd", SecureLogic.Encrypt(pwd)));
                parameterList.Add(base.GetParameter("usernum", usernum));

                DbDataReader dbDataReader = base.GetDataReader("ValidateUser", parameterList, CommandType.StoredProcedure);

                while (dbDataReader.Read())
                {
                    if (dbDataReader["ExpiryStatus"].ToString() == "Account Expired")
                    {
                        userModel.UserNum = -1;
                    }
                    else
                    {
                        userModel.UserNum = int.Parse(dbDataReader["id"].ToString());
                    }

                    userModel.FirstName = SecureLogic.Decrypt(dbDataReader["FirstName"].ToString());
                    userModel.LastName = SecureLogic.Decrypt(dbDataReader["LastName"].ToString());
                    userModel.Emailid = SecureLogic.Decrypt(dbDataReader["EmailId"].ToString());
                    userModel.UserStatus = int.Parse(dbDataReader["UserStatus"].ToString());
                    userModel.UserType = int.Parse(dbDataReader["UserType"].ToString());
                    userModel.UserTypeVal = dbDataReader["UserTypeVal"].ToString();
                    userModel.UserStatusVal = dbDataReader["UserStatusVal"].ToString();                    
                    userModel.IpAddress = dbDataReader["IpAddress"].ToString();
                    userModel.Accountid = int.Parse(dbDataReader["AccountId"].ToString());
                    
                }

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return userModel;
        }
        public int UpdateIPAddress(UserModel userModel)
        {
            int Result = 0;
            try
            {
                

                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("UserNum", userModel.UserNum));
                parameterList.Add(base.GetParameter("IpAddress", userModel.IpAddress));
                if(userModel.otp != null)
                {
                    parameterList.Add(base.GetParameter("otp", userModel.otp));
                }
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("UpdateIPAddress", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;

                string htmlstring = @"<div class='WordSection1'>
<p class='MsoPlainText'>Dear " + userModel.FirstName + "&nbsp;" + userModel.LastName + @"</p>
<p class='MsoPlainText'>You recently logged in to ECI from a browser or app that we don't recognize.</p>
<p class='MsoPlainText'>System Configuration: " + userModel.sysconfig + @"</p>
<p class='MsoPlainText'>Username: " + userModel.Emailid + @"</p>
<p class='MsoPlainText'>To ensure your account's security, we need to verify your identity. Enter the following code where prompted by ECI Dashboard.</p>
<p class='MsoPlainText'>Verification Code: " + userModel.otp + @"</p>
<p class='MsoPlainText'>If you didn't recently log in to ECI Dashboard, or you don't recognize this browser or operating system, contact your ECI administrator.</p>
<p class='MsoPlainText'>Regards,<br/> ECI Team.</p>";


                int ret = EmailLogic.SendEmail("ECI Verification Code", userModel.Emailid, "<html><body style='font-family:Calibri'>"+ htmlstring + "</body></html>");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public int InsertOTP(UserModel userModel)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("UserNum", userModel.UserNum));
                parameterList.Add(base.GetParameter("otp", userModel.otp));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("InsertOTP", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public DataTable GetMenusByUser(int usernum)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("UserNum", usernum));

                DbDataReader dbDataReader = base.GetDataReader("GetMenusByUser", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public int AddProfile(ProfileModel profileModel)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                
                parameterList.Add(base.GetParameter("firstname", SecureLogic.Encrypt(profileModel.firstname)));
                parameterList.Add(base.GetParameter("lastname", SecureLogic.Encrypt(profileModel.lastname)));
                parameterList.Add(base.GetParameter("email", profileModel.email != null ? SecureLogic.Encrypt(profileModel.email) : profileModel.email));
                parameterList.Add(base.GetParameter("createdby",profileModel.createdby));
                parameterList.Add(base.GetParameter("Middle_Name", profileModel.Middle_Name != null ? SecureLogic.Encrypt(profileModel.Middle_Name) : profileModel.Middle_Name));
                parameterList.Add(base.GetParameter("Tittle", profileModel.Tittle != null ? SecureLogic.Encrypt(profileModel.Tittle) : profileModel.Tittle));
                parameterList.Add(base.GetParameter("Division",profileModel.Division != null ? SecureLogic.Encrypt(profileModel.Division) : profileModel.Division));
                parameterList.Add(base.GetParameter("Brithday", profileModel.Brithday != null ? SecureLogic.Encrypt(profileModel.Brithday) : profileModel.Brithday));

                parameterList.Add(base.GetParameter("Mobile", profileModel.Mobile != null ? SecureLogic.Encrypt(profileModel.Mobile) : profileModel.Mobile));
                parameterList.Add(base.GetParameter("Contact_Owner", profileModel.Contact_Owner != null ? SecureLogic.Encrypt(profileModel.Contact_Owner) : profileModel.Contact_Owner));
                parameterList.Add(base.GetParameter("Telephone",profileModel.Telephone != null ? SecureLogic.Encrypt(profileModel.Telephone) : profileModel.Telephone));
                parameterList.Add(base.GetParameter("Mailing_Street", profileModel.Mailing_Street != null ? SecureLogic.Encrypt(profileModel.Mailing_Street) : profileModel.Mailing_Street));
                parameterList.Add(base.GetParameter("Mailing_City", profileModel.Mailing_City != null ? SecureLogic.Encrypt(profileModel.Mailing_City) : profileModel.Mailing_City));
                parameterList.Add(base.GetParameter("Mailing_State", profileModel.Mailing_State != null ? SecureLogic.Encrypt(profileModel.Mailing_State) : profileModel.Mailing_State));
                parameterList.Add(base.GetParameter("Mailing_Postal_Code", profileModel.Mailing_Postal_Code != null ? SecureLogic.Encrypt(profileModel.Mailing_Postal_Code) : profileModel.Mailing_Postal_Code));
                parameterList.Add(base.GetParameter("Mailing_Country",profileModel.Mailing_Country != null ? SecureLogic.Encrypt(profileModel.Mailing_Country) : profileModel.Mailing_Country));
                parameterList.Add(base.GetParameter("Website", profileModel.Website != null ? SecureLogic.Encrypt(profileModel.Website):profileModel.Website));
                parameterList.Add(base.GetParameter("Description", profileModel.Description != null ? SecureLogic.Encrypt(profileModel.Description) : profileModel.Description));

                parameterList.Add(base.GetParameter("ExpiryDate", profileModel.ExpiryDate != null ? DateTime.ParseExact(profileModel.ExpiryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd") : profileModel.ExpiryDate));
               
                string primarydataset = string.Join(",", profileModel.PrimaryID);
                string comparisiondataset = string.Join(",",profileModel.ComparisionID);
                parameterList.Add(base.GetParameter("PrimaryID", primarydataset));
                parameterList.Add(base.GetParameter("ComparisionID", comparisiondataset));



                Random r = new Random();
                var pwd = r.Next(0, 999999).ToString("D6");

                parameterList.Add(base.GetParameter("pwd", SecureLogic.Encrypt(pwd)));

                parameterList.Add(base.GetParameter("companyid", profileModel.companyid));
                parameterList.Add(base.GetParameter("roleid", profileModel.roleid));

                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("InsertProfileDtls", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;

                string url = profileModel.AppURL + "?UserNum=" + Result.ToString() + "&status=Yes";

                string htmlstring = @"Login Id : "+profileModel.email+"<br></br> URL :<a href='" + url + "'>Click Me</a> <br></br> Regards,  <br></br>ECI Team";

                int ret = EmailLogic.SendEmail("ECI Welcome", profileModel.email, "<html><body style='font-family:Calibri'>" + htmlstring + "</body></html>");

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public IEnumerable<AccountModel> GetCompanyListsAll()
        {
            var list = new List<AccountModel>();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                DbDataReader dbDataReader = base.GetDataReader("GetCompanyListsAll", parameterList, CommandType.StoredProcedure);

                while (dbDataReader.Read())
                {
                    AccountModel userModel = new AccountModel();                    
                    userModel.companyName = dbDataReader["companyName"].ToString();
                    userModel.AccOwner = dbDataReader["AccOwner"].ToString();
                    userModel.Industry = dbDataReader["Industry"].ToString();
                    if (dbDataReader["Employees"] != DBNull.Value)
                    {
                        if (dbDataReader["Employees"].ToString() != "")
                        {
                            userModel.Employees = int.Parse(dbDataReader["Employees"].ToString());
                        }
                        else
                        {
                            userModel.Employees = 0;
                        }
                    }
                    else
                    {
                        if (dbDataReader["Employees"].ToString() != "")
                        {
                            userModel.Employees = int.Parse(dbDataReader["Employees"].ToString());
                        }
                        else
                        {
                            userModel.Employees = 0;
                        }
                    }
                   
                    userModel.Website = dbDataReader["WebSite"].ToString();
                    userModel.id = int.Parse(dbDataReader["id"].ToString());
                  
                    list.Add(userModel);
                }

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }
        public int GetUserByEmailId(string emailid)
        {
            int UserNum = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("EmailId", SecureLogic.Encrypt(emailid)));

                DbDataReader dbDataReader = base.GetDataReader("GetUserByEmailId", parameterList, CommandType.StoredProcedure);

                while (dbDataReader.Read())
                {

                    UserNum = Convert.ToInt32(dbDataReader["Id"].ToString());

                }
                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return UserNum;
        }
        public int IsSameUserLoggedIn(string emailid, string Pass, string IpAddress)
        {
            int Cnt = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("emailid",SecureLogic.Encrypt(emailid)));
                parameterList.Add(base.GetParameter("Pass", SecureLogic.Encrypt(Pass)));
                parameterList.Add(base.GetParameter("IpAddress", IpAddress));

                DbDataReader dbDataReader = base.GetDataReader("IsSameUserLoggedIn", parameterList, CommandType.StoredProcedure);

                while (dbDataReader.Read())
                {

                    Cnt = Convert.ToInt32(dbDataReader["Result"].ToString());

                }
                dbDataReader.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Cnt;
        }
        public int InsertUserHistory(UserModel userModel)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("usernum", userModel.UserNum));
                parameterList.Add(base.GetParameter("sessionid", userModel.SessionId));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("InsertUserHistory", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public int UpdateUserHistory(int UserNUm)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("usernum", UserNUm));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("UpdateUserHistory", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public string GetSessionidByUserNum(int usernum)
        {
            string sessionid = "";
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("usernum", usernum));

                DbDataReader dbDataReader = base.GetDataReader("GetSessionidByUserNum", parameterList, CommandType.StoredProcedure);

                while (dbDataReader.Read())
                {

                    sessionid = dbDataReader["sessionid"].ToString();

                }
                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sessionid;
        }
        public DataTable GetProfilebyuserid(int Userid)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("Userid", Userid));
                DbDataReader dbDataReader = base.GetDataReader("GetProfilebyuserid", parameterList, CommandType.StoredProcedure);
                dt.Load(dbDataReader);
                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
        public int UpdateProfile(ProfileModel profileModel)
        {
            int Result = 0;
            try
            {
   

                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("profileid", profileModel.id));
                parameterList.Add(base.GetParameter("firstname", profileModel.firstname != null ? SecureLogic.Encrypt(profileModel.firstname) : profileModel.firstname));
                parameterList.Add(base.GetParameter("lastname",profileModel.lastname != null ? SecureLogic.Encrypt(profileModel.lastname) : profileModel.lastname));
                parameterList.Add(base.GetParameter("email", profileModel.email != null ? SecureLogic.Encrypt(profileModel.email) : profileModel.email));
                parameterList.Add(base.GetParameter("createdby", profileModel.createdby.ToString()));
                parameterList.Add(base.GetParameter("Middle_Name", profileModel.Middle_Name != null ? SecureLogic.Encrypt(profileModel.Middle_Name) : profileModel.Middle_Name));
                parameterList.Add(base.GetParameter("Tittle", profileModel.Tittle != null ? SecureLogic.Encrypt(profileModel.Tittle) : profileModel.Tittle));
                parameterList.Add(base.GetParameter("Division", profileModel.Division != null ? SecureLogic.Encrypt(profileModel.Division) : profileModel.Division));
                parameterList.Add(base.GetParameter("Brithday", profileModel.Brithday != null ? SecureLogic.Encrypt(profileModel.Brithday) : profileModel.Brithday));
                parameterList.Add(base.GetParameter("Mobile", profileModel.Mobile != null ? SecureLogic.Encrypt(profileModel.Mobile) : profileModel.Mobile));
                parameterList.Add(base.GetParameter("Contact_Owner", profileModel.Contact_Owner != null ? SecureLogic.Encrypt(profileModel.Contact_Owner) : profileModel.Contact_Owner));
                parameterList.Add(base.GetParameter("Telephone", profileModel.Telephone != null ? SecureLogic.Encrypt(profileModel.Telephone) : profileModel.Telephone));
                parameterList.Add(base.GetParameter("Mailing_Street", profileModel.Mailing_Street != null ? SecureLogic.Encrypt(profileModel.Mailing_Street) : profileModel.Mailing_Street));
                parameterList.Add(base.GetParameter("Mailing_City", profileModel.Mailing_City != null ? SecureLogic.Encrypt(profileModel.Mailing_City) : profileModel.Mailing_City));
                parameterList.Add(base.GetParameter("Mailing_State", profileModel.Mailing_State != null ? SecureLogic.Encrypt(profileModel.Mailing_State) : profileModel.Mailing_State));
                parameterList.Add(base.GetParameter("Mailing_Postal_Code", profileModel.Mailing_Postal_Code != null ? SecureLogic.Encrypt(profileModel.Mailing_Postal_Code): profileModel.Mailing_Postal_Code));
                parameterList.Add(base.GetParameter("Mailing_Country", profileModel.Mailing_Country != null ? SecureLogic.Encrypt(profileModel.Mailing_Country) : profileModel.Mailing_Country));
                parameterList.Add(base.GetParameter("Website", profileModel.Website != null ? SecureLogic.Encrypt(profileModel.Website) : profileModel.Website));
                parameterList.Add(base.GetParameter("Description", profileModel.Description != null ? SecureLogic.Encrypt(profileModel.Description) : profileModel.Description));
                parameterList.Add(base.GetParameter("ExpiryDate", profileModel.ExpiryDate != null ? DateTime.ParseExact(profileModel.ExpiryDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd") : profileModel.ExpiryDate));
                parameterList.Add(base.GetParameter("companyid", profileModel.companyid));
                parameterList.Add(base.GetParameter("roleid", profileModel.roleid));
                string primarydataset = string.Join(",", profileModel.PrimaryID);
                string comparisiondataset = string.Join(",", profileModel.ComparisionID);
                parameterList.Add(base.GetParameter("PrimaryID", primarydataset));
                parameterList.Add(base.GetParameter("ComparisionID", comparisiondataset));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("UpdateProfileDtls", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public DataTable GetUserFolders(int Userid)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("Userid", Userid));
                DbDataReader dbDataReader = base.GetDataReader("GetUserFolders", parameterList, CommandType.StoredProcedure);
                dt.Load(dbDataReader);
                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }
        public int InsertUserFolder(string foldername, int usernum, int parentfolderid)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("foldername", foldername));
                parameterList.Add(base.GetParameter("usernum", usernum));
                parameterList.Add(base.GetParameter("parentfolderid", parentfolderid));
                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("InsertUserFolder", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public string IsValidSession(int usernum, int resetsession)
        {
            string Result = "";
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("usernum", usernum));
                parameterList.Add(base.GetParameter("resetsession", usernum));

                DbDataReader dbDataReader = base.GetDataReader("IsValidSession", parameterList, CommandType.StoredProcedure);

                while (dbDataReader.Read())
                {

                    Result = dbDataReader["Result"].ToString();

                }
                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
    }
}
