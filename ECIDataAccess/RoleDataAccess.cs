﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ECIModel;

namespace ECIDataAccess
{
    public interface IRoleDataAccess
    {
        DataTable GetAllMenus();
        IEnumerable<RoleModel> GetAllRoles();
        int AddRoleloop(string Rolexml,string RoleName,int createdby);
        int UpdateRoleloop(int RoleId,string Rolexml, string RoleName, int createdby);

        int AddUserRole(string Rolexml, int UserNum, int createdby);
        DataTable GetSelectedUserRole(string UserId);
        DataTable GetMenubyRoleid(int RoleId);
    }
    public class RoleDataAccess : BaseDataAccess, IRoleDataAccess, IDisposable
    {
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {

                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public DataTable GetAllMenus()
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                DbDataReader dbDataReader = base.GetDataReader("GetAllMenus", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public IEnumerable<RoleModel> GetAllRoles()
        {
            var list = new List<RoleModel>();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                DbDataReader dbDataReader = base.GetDataReader("GetAllRoles", parameterList, CommandType.StoredProcedure);

                while (dbDataReader.Read())
                {
                    RoleModel userModel = new RoleModel();
                    userModel.RoleName = dbDataReader["rolename"].ToString();
                    userModel.RoleID = int.Parse(dbDataReader["id"].ToString());
                    userModel.Description = dbDataReader["Description"].ToString();

                    list.Add(userModel);
                }

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }
        public int AddRoleloop(string Rolexml, string RoleName, int createdby)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("xmlval",Rolexml));
                parameterList.Add(base.GetParameter("RoleName", RoleName));
                parameterList.Add(base.GetParameter("Createdby", createdby));


                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("AddRoleSp", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }

        public int UpdateRoleloop(int RoleId,string Rolexml, string RoleName, int createdby)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();
                parameterList.Add(base.GetParameter("RoleId", RoleId));
                parameterList.Add(base.GetParameter("xmlval", Rolexml));
                parameterList.Add(base.GetParameter("RoleName", RoleName));
                parameterList.Add(base.GetParameter("Modifiedby", createdby));


                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("UpdateRoleSp", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }

        public int AddUserRole(string Rolexml, int UserNum, int createdby)
        {
            int Result = 0;
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("xmlval", Rolexml));
                parameterList.Add(base.GetParameter("UserNum", UserNum));
                parameterList.Add(base.GetParameter("Createdby", createdby));


                DbParameter TestIdParamter = base.GetParameterOut("Result", SqlDbType.Int, 0);
                parameterList.Add(TestIdParamter);

                base.ExecuteNonQuery("AddUserRole", parameterList, CommandType.StoredProcedure);

                Result = (int)TestIdParamter.Value;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }
        public DataTable GetSelectedUserRole(string UserId)
        {

            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("UserID", UserId));

                DbDataReader dbDataReader = base.GetDataReader("GetUserSelectedRole", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetMenubyRoleid(int RoleId)
        {
            DataTable dt = new DataTable();
            try
            {
                List<DbParameter> parameterList = new List<DbParameter>();

                parameterList.Add(base.GetParameter("RoleId", RoleId));

                DbDataReader dbDataReader = base.GetDataReader("GetMenubyRoleid", parameterList, CommandType.StoredProcedure);

                dt.Load(dbDataReader);

                dbDataReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
    }
}
