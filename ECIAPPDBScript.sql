USE [ECIApp]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 6/6/2018 8:03:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[companyName] [varchar](512) NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 6/6/2018 8:03:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[categoryid] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](512) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DataLabelMaster]    Script Date: 6/6/2018 8:03:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataLabelMaster](
	[DataLabelID] [int] IDENTITY(1,1) NOT NULL,
	[DataLabelName] [varchar](512) NULL,
	[categoryid] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ECIData]    Script Date: 6/6/2018 8:03:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ECIData](
	[companyname] [varchar](250) NULL,
	[Category] [varchar](512) NULL,
	[DataLabel] [varchar](150) NULL,
	[SurveyYear] [int] NULL,
	[ResponseID] [int] NULL,
	[ResponseCount] [int] NULL,
	[ReponsePercent] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuOption]    Script Date: 6/6/2018 8:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuOption](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[menuname] [varchar](150) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsHasChild] [bit] NOT NULL,
	[ParentId] [int] NULL,
	[URLPath] [varchar](512) NULL,
	[menutype] [char](1) NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL,
	[Orderid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProfileDtls]    Script Date: 6/6/2018 8:03:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProfileDtls](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [varchar](150) NOT NULL,
	[lastname] [varchar](150) NULL,
	[email] [varchar](150) NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleMaster]    Script Date: 6/6/2018 8:03:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rolename] [varchar](150) NOT NULL,
	[isactive] [bit] NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rolemenurel]    Script Date: 6/6/2018 8:03:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rolemenurel](
	[roleid] [int] NOT NULL,
	[menuid] [int] NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserOTP]    Script Date: 6/6/2018 8:03:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserOTP](
	[usernum] [int] NOT NULL,
	[opt] [varchar](10) NULL,
	[starttime] [datetime] NULL,
	[endtime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[usernum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 6/6/2018 8:03:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserId] [int] NOT NULL,
	[roleid] [int] NOT NULL,
	[createdon] [datetime] NOT NULL,
	[createdby] [int] NOT NULL,
	[modifiedon] [datetime] NULL,
	[modifiedby] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 6/6/2018 8:03:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Accountid] [int] NULL,
	[profileid] [int] NULL,
	[FirstName] [varchar](150) NOT NULL,
	[LastName] [varchar](150) NOT NULL,
	[EmailId] [varchar](150) NOT NULL,
	[Pwd] [varchar](max) NOT NULL,
	[UserType] [int] NULL,
	[UserStatus] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[IpAddress] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 
GO
INSERT [dbo].[Account] ([id], [companyName], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, N'Company1', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[Account] ([id], [companyName], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, N'Company3', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[Account] ([id], [companyName], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (3, N'Company4', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[CategoryMaster] ON 
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (1, N'Awareness of Program Resources')
GO
INSERT [dbo].[CategoryMaster] ([categoryid], [CategoryName]) VALUES (2, N'Misc. Questions')
GO
SET IDENTITY_INSERT [dbo].[CategoryMaster] OFF
GO
SET IDENTITY_INSERT [dbo].[DataLabelMaster] ON 
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (1, N'Standards', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (2, N'Training', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (3, N'Advice', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (4, N'Hotline', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (5, N'Appraisal', 1)
GO
INSERT [dbo].[DataLabelMaster] ([DataLabelID], [DataLabelName], [categoryid]) VALUES (6, N'Discipline', 1)
GO
SET IDENTITY_INSERT [dbo].[DataLabelMaster] OFF
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Standards', 2015, 0, 6, 0.00452488687782805)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Standards', 2015, 1, 1593, 0.971341463414634)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Standards', 2015, 88, 41, 0.0254500310366232)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Training', 2015, 0, 38, 0.0249507550886408)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Training', 2015, 1, 1507, 0.91890243902439)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Training', 2015, 88, 95, 0.0647580095432856)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Advice', 2015, 0, 10, 0.00765110941086458)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Advice', 2015, 1, 1575, 0.960365853658537)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Advice', 2015, 88, 55, 0.0338253382533825)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Hotline', 2015, 0, 11, 0.0073875083948959)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Hotline', 2015, 1, 1586, 0.967073170731707)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Hotline', 2015, 88, 43, 0.0266584004959702)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Appraisal', 2015, 0, 225, 0.138546798029557)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Appraisal', 2015, 1, 934, 0.569512195121951)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Appraisal', 2015, 88, 481, 0.294189602446483)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Discipline', 2015, 0, 52, 0.0319214241866176)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Discipline', 2015, 1, 1286, 0.784146341463415)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company1', N'Awareness of Program Resources', N'Discipline', 2015, 88, 302, 0.184596577017115)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Standards', 2015, 0, 7, 0.0080091533180778)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Standards', 2015, 1, 883, 0.963973799126638)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Standards', 2015, 88, 26, 0.0288568257491676)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Training', 2015, 0, 15, 0.017162471395881)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Training', 2015, 1, 867, 0.946506550218341)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Training', 2015, 88, 34, 0.0386363636363636)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Advice', 2015, 0, 8, 0.0091533180778032)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Advice', 2015, 1, 855, 0.933406113537118)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Advice', 2015, 88, 53, 0.0588235294117647)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Hotline', 2015, 0, 9, 0.0102974828375286)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Hotline', 2015, 1, 856, 0.934497816593887)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Hotline', 2015, 88, 51, 0.0562293274531422)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Appraisal', 2015, 0, 51, 0.0572390572390572)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Appraisal', 2015, 1, 714, 0.780327868852459)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Appraisal', 2015, 88, 151, 0.164847161572052)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Discipline', 2015, 0, 12, 0.0137299771167048)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Discipline', 2015, 1, 787, 0.859170305676856)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company3', N'Awareness of Program Resources', N'Discipline', 2015, 88, 117, 0.128289473684211)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2010, 0, 39, 0.0065989847715736)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2010, 1, 5884, 0.980666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2010, 88, 77, 0.0130420054200542)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2010, 0, 104, 0.017514314584035)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2010, 1, 5725, 0.954166666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2010, 88, 171, 0.0285809794417516)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2010, 0, 78, 0.0131357359380263)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2010, 1, 5646, 0.941)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2010, 88, 276, 0.0461847389558233)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2010, 0, 81, 0.0136409565510273)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2010, 1, 5756, 0.959333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2010, 88, 163, 0.027339818852734)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2010, 0, 338, 0.0564933979608892)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2010, 1, 5104, 0.850666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2010, 88, 558, 0.0933110367892977)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2010, 0, 165, 0.027661357921207)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2010, 1, 4780, 0.796666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2010, 88, 1055, 0.175950633755837)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2013, 0, 28, 0.00496982605608804)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2013, 1, 5878, 0.979666666666667)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Standards', 2013, 88, 94, 0.0157216925907342)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2013, 0, 84, 0.0143983544737744)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2013, 1, 5750, 0.958333333333333)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Training', 2013, 88, 166, 0.0280168776371308)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2013, 0, 70, 0.0117076434186319)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2013, 1, 5721, 0.9535)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Advice', 2013, 88, 209, 0.0352742616033755)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2013, 0, 58, 0.0109454614078128)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2013, 1, 5781, 0.9635)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Hotline', 2013, 88, 161, 0.0269275798628533)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2013, 0, 182, 0.0304398728884429)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2013, 1, 5364, 0.894)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Appraisal', 2013, 88, 454, 0.0760469011725293)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2013, 0, 156, 0.0260956841753095)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2013, 1, 5055, 0.8425)
GO
INSERT [dbo].[ECIData] ([companyname], [Category], [DataLabel], [SurveyYear], [ResponseID], [ResponseCount], [ReponsePercent]) VALUES (N'Company4', N'Awareness of Program Resources', N'Discipline', 2013, 88, 789, 0.131609674728941)
GO
SET IDENTITY_INSERT [dbo].[MenuOption] ON 
GO
INSERT [dbo].[MenuOption] ([id], [menuname], [IsActive], [IsHasChild], [ParentId], [URLPath], [menutype], [createdon], [createdby], [modifiedon], [modifiedby], [Orderid]) VALUES (1, N'Dashboard', 1, 0, 0, N'SAReport', N'M', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, 1)
GO
INSERT [dbo].[MenuOption] ([id], [menuname], [IsActive], [IsHasChild], [ParentId], [URLPath], [menutype], [createdon], [createdby], [modifiedon], [modifiedby], [Orderid]) VALUES (2, N'User', 1, 1, 0, NULL, N'M', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, 2)
GO
INSERT [dbo].[MenuOption] ([id], [menuname], [IsActive], [IsHasChild], [ParentId], [URLPath], [menutype], [createdon], [createdby], [modifiedon], [modifiedby], [Orderid]) VALUES (3, N'View Users', 1, 0, 2, N'SAECIUser', N'M', CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL, 3)
GO
SET IDENTITY_INSERT [dbo].[MenuOption] OFF
GO
SET IDENTITY_INSERT [dbo].[RoleMaster] ON 
GO
INSERT [dbo].[RoleMaster] ([id], [rolename], [isactive], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, N'SuperAdmin', 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[RoleMaster] ([id], [rolename], [isactive], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, N'ECI User', 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[RoleMaster] ([id], [rolename], [isactive], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (3, N'ECI Client User', 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[RoleMaster] OFF
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, 3, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (3, 1, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[rolemenurel] ([roleid], [menuid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, 3, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[UserRole] ([UserId], [roleid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (1, 3, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[UserRole] ([UserId], [roleid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (2, 3, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
INSERT [dbo].[UserRole] ([UserId], [roleid], [createdon], [createdby], [modifiedon], [modifiedby]) VALUES (3, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Users] ON 
GO
INSERT [dbo].[Users] ([id], [Accountid], [profileid], [FirstName], [LastName], [EmailId], [Pwd], [UserType], [UserStatus], [CreatedOn], [UpdatedOn], [IpAddress]) VALUES (1, 1, 0, N'Deepa', N'Marimuthu', N'gdeepabcs@gmail.com', N'12345', 3, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[Users] ([id], [Accountid], [profileid], [FirstName], [LastName], [EmailId], [Pwd], [UserType], [UserStatus], [CreatedOn], [UpdatedOn], [IpAddress]) VALUES (2, 2, 0, N'Sririthupa', N'Marimuthu', N'deepamarimuthu@ibbsllp.com', N'12345', 3, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), NULL, N'')
GO
INSERT [dbo].[Users] ([id], [Accountid], [profileid], [FirstName], [LastName], [EmailId], [Pwd], [UserType], [UserStatus], [CreatedOn], [UpdatedOn], [IpAddress]) VALUES (3, 0, 0, N'Ashok', N'Kumar', N'ashokkumar.s@ibridgellc.com ', N'12345', 2, 2, CAST(N'2018-06-06T00:00:00.000' AS DateTime), NULL, N'')
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
/****** Object:  StoredProcedure [dbo].[GetCategoryList]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Jun.1,2018
-- Description:	[GetCategoryList]
-- =============================================
Create procedure[dbo].[GetCategoryList]
AS
BEGIN

Select * From
CategoryMaster

RETURN

END





GO
/****** Object:  StoredProcedure [dbo].[GetDataLabelListByCatID]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Jun.1,2018
-- Description:	[GetCategoryList]
-- =============================================
Create procedure[dbo].[GetDataLabelListByCatID]
@catid int
AS
BEGIN

Select * From
DataLabelMaster
Where categoryid = @catid

RETURN

END





GO
/****** Object:  StoredProcedure [dbo].[GetMenusByUser]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Apr.27,2018
-- Description:	GetUsers
-- =============================================
Create procedure[dbo].[GetMenusByUser]
@UserNum int
AS
BEGIN
Select M.* from 
UserRole UR
Inner Join RoleMenuRel RM On UR.RoleID = RM.RoleID
Inner Join MenuOption M on M.id = RM.Menuid
Where UR.UserID = @UserNum;
	RETURN
END





GO
/****** Object:  StoredProcedure [dbo].[GetReports]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Jun.1,2018
-- Description:	[GetReports]
-- =============================================
CREATE procedure[dbo].[GetReports]
@catid int,
@labelid int,
@accountid int
AS
BEGIN

declare @catname varchar(512)
set @catname = (Select CategoryName from CategoryMaster where categoryid = @catid);

declare @DataLabelName varchar(512)
set @DataLabelName = (Select DataLabelName from DataLabelMaster where DataLabelID = @labelid);

Select E.companyname, responseid, avg(ReponsePercent)*100 as reportdata  
From ECIData E
inner join Account A on e.companyname = A.companyname
Where Category = @catname and DataLabel = @DataLabelName
and (A.id = @accountid or @accountid = 0)
Group by E.companyname, responseid order by E.companyname asc
	
RETURN

END





GO
/****** Object:  StoredProcedure [dbo].[GetUserOTP]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[GetUserOTP]
@Usernum int
as
begin
Select * from UserOTP Where UserNum = @Usernum
end
GO
/****** Object:  StoredProcedure [dbo].[GetUsers]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Apr.27,2018
-- Description:	GetUsers
-- =============================================
CREATE procedure[dbo].[GetUsers]
AS
BEGIN
select *,
Case UserStatus
When 0 then 'To Be Verified'
When 1 then 'To Be Change Password'
When 2 then 'Active'
When 3 then 'De-Active'
end as UserStatusVal,
Case UserType
When 1 then 'Super Admin'
When 2 then 'ECI User'
When 3 then 'ECI Client User'
end as UserTypeVal,
IpAddress
 From Users
	
	RETURN
END





GO
/****** Object:  StoredProcedure [dbo].[InsertOTP]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[InsertOTP]	
	@UserNum int,
	@otp varchar(10),	
	@Result int Output
as
begin

Delete From UserOTP Where Usernum = @UserNum;

Insert into UserOTP values (@UserNum, @otp, getdate(),  DATEADD(minute,5,GETDATE()))

set @Result = 1

Select @Result

end

GO
/****** Object:  StoredProcedure [dbo].[SaveECIUser]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[SaveECIUser]
	@FirstName varchar(150),
	@LastName varchar(150),
	@EmailId varchar(150),
	@Pwd varchar(max),
	@UserType int,
	@UserStatus int,	
	@Result int Output
as
begin


INSERT INTO [dbo].[Users]
           ([FirstName]
           ,[LastName]
           ,[EmailId]
           ,[Pwd]
           ,[UserType]
           ,[UserStatus]
           ,[CreatedOn])
     VALUES
           (@FirstName
           ,@LastName
           ,@EmailId
           ,@Pwd
           ,@UserType
           ,@UserStatus
           ,getdate())

		   Select @Result
end





GO
/****** Object:  StoredProcedure [dbo].[UpdateIPAddress]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UpdateIPAddress]	
	@UserNum int,
	@IpAddress varchar(20),	
	@otp varchar(10) = null,
	@Result int Output
as
begin

if(@otp is null)
begin
	Update Users Set IpAddress = @IpAddress Where id = @UserNum
end
else
begin
	Update Users Set IpAddress = @IpAddress Where id = @UserNum

	exec InsertOTP @UserNum,@otp,@Result
end


set @Result = 1

Select @Result

end
GO
/****** Object:  StoredProcedure [dbo].[UpdatePwd]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[UpdatePwd]	
	@UserNum int,
	@Pwd varchar(max),	
	@Result int Output
as
begin


Update Users Set Pwd = @Pwd Where id = @UserNum

set @Result = 1

Select @Result

end

GO
/****** Object:  StoredProcedure [dbo].[UpdateVerification]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[UpdateVerification]	
	@UserNum int,
	@UserStatus int,	
	@Result int Output
as
begin


Update Users Set UserStatus = @UserStatus Where id = @UserNum

set @Result = 1

Select @Result

end





GO
/****** Object:  StoredProcedure [dbo].[ValidateUser]    Script Date: 6/6/2018 8:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Deepa
-- Create date: Apr.27,2018
-- Description:	GetUsers
-- =============================================
CREATE procedure[dbo].[ValidateUser]
@EmailId varchar(150),
@Pwd varchar(max)
AS
BEGIN
select *,
Case UserStatus
When 0 then 'To Be Verified'
When 1 then 'To Be Change Password'
When 2 then 'Active'
When 3 then 'De-Active'
end as UserStatusVal,
Case UserType
When 1 then 'Super Admin'
When 2 then 'ECI User'
When 3 then 'ECI Client User'
end as UserTypeVal,
IpAddress
 From Users
	Where emailid = @EmailId and pwd = @Pwd
	RETURN
END





GO
