﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

namespace ECIBusinessLogic
{
    public static class EmailLogic
    {
        public static int SendEmail(string Sub, string To, string Body)
        {
            int result = 0;
            try
            {
                string mailserver = System.Configuration.ConfigurationSettings.AppSettings["mailserver"].ToString();
                string CommonFrom = System.Configuration.ConfigurationSettings.AppSettings["CommonFrom"].ToString();
                string CommonMailUserPwd = System.Configuration.ConfigurationSettings.AppSettings["CommonMailUserPwd"].ToString();
                string SmtpPortNo = System.Configuration.ConfigurationSettings.AppSettings["SmtpPortNo"].ToString();

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(mailserver);

                mail.From = new MailAddress(CommonFrom);
                mail.To.Add(To);
                mail.Subject = Sub;


                mail.Body = Body;
                mail.IsBodyHtml = true;

                SmtpServer.Port = int.Parse(SmtpPortNo);
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.EnableSsl = true;
                SmtpServer.Credentials = new System.Net.NetworkCredential(CommonFrom, CommonMailUserPwd);


                SmtpServer.Send(mail);
                result = 1;
            }
            catch(Exception  ex)
            {
                result = 0;
            }
            return result;
        }
    }
}
