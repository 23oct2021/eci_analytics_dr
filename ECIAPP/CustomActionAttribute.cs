﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ECIModel;
using ECIAPP.Helpers;
using ECIDataAccess;

namespace ECIAPP
{
    public class CustomActionAttribute
    {
    }
    public class SessionExpireFilterAttribute : FilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext Context = HttpContext.Current;

            if (Context.Session != null)
            {                
                if (Context.Session.IsNewSession)
                {
                    string sCookieHeader = Context.Request.Headers["Cookie"];
                    if ((null != sCookieHeader) && (sCookieHeader.IndexOf("ASP.NET_SessionId") >= 0))
                    {

                        if (Context.Request.IsAuthenticated)
                        {
                            System.Web.Security.FormsAuthentication.SignOut();
                        }
                        ActionResult unAuthorised1 = new RedirectToRouteResult(
                                            new RouteValueDictionary {
                                            {"area",""},
                                            { "Controller", "Home" },
                                            { "Action", "SessionLogout" }
                                            });
                        filterContext.Result = unAuthorised1;
                        return;
                    }
                }
                if (Context.Session["SessionUser"] != null)
                {
                    SessionUser SessionUser = (SessionUser)Context.Session["SessionUser"];
                    IUserDataAccess _userDataAccess = new UserDataAccess();

                    string sessionid = _userDataAccess.GetSessionidByUserNum(SessionUser.UserNum);

                    if (sessionid != Context.Session.SessionID)
                    {
                        if (Context.Request.IsAuthenticated)
                        {
                            System.Web.Security.FormsAuthentication.SignOut();
                        }
                        ActionResult unAuthorised1 = new RedirectToRouteResult(
                                            new RouteValueDictionary {
                                            {"area",""},
                                            { "Controller", "Home" },
                                            { "Action", "SessionLogout" }
                                            });
                        filterContext.Result = unAuthorised1;
                        return;
                    }
                }
                //SessionUser SessionUser = (SessionUser)HSSession.getSessionUser();
                //if (SessionUser != null)
                //{
                //    if ((!SessionUser.IsAdmin) && (SessionUser.UserStatus != 3))
                //    {
                //        if (SessionUser.UserStatus == 2)
                //        {

                //            ActionResult unAuthorised1 = new RedirectToRouteResult(
                //                            new RouteValueDictionary {
                //                            {"area","SHRMPM"},
                //                            { "Controller", "Profile" },
                //                            { "Action", "UpdateDetails" }
                //                            });
                //            filterContext.Result = unAuthorised1;
                //            return;
                //        }
                //    }
                //    else if (SessionUser.IsAdmin)
                //    {
                //        if (SessionUser.UserStatus == 1)
                //        {
                //            ActionResult unAuthorised1 = new RedirectToRouteResult(
                //                           new RouteValueDictionary {
                //                            {"area","SHRMPM"},
                //                            { "Controller", "Profile" },
                //                            { "Action", "UpdateDetails" }
                //                            });
                //            filterContext.Result = unAuthorised1;
                //            return;
                //        }
                //    }
                //}
            }
        }
        void IActionFilter.OnActionExecuted(ActionExecutedContext filterContext)
        {

        }
    }
}