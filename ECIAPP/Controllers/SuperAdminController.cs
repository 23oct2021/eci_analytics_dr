﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECIAPP.Models;
using System.Net;
using System.Net.Mail;
using ECIModel;
using ECIDataAccess;
using System.IO;
using System.Text;
using System.Drawing;
using System.Web.Script.Serialization;
using ECIBusinessLogic;
using Newtonsoft.Json;

namespace ECIAPP.Controllers
{
    //[SessionExpireFilterAttribute()]
    public class SuperAdminController : Controller
    {
        //
        // GET: /Home/
        private readonly IUserDataAccess _userDataAccess;
        private readonly IReportAccess _reportAccess;
        private readonly IAccountDataAccess _accountDataAccess;
        private readonly IRoleDataAccess _RoleDataAccess;
        public SuperAdminController()
        {
            this._userDataAccess = new UserDataAccess();
            this._reportAccess = new ReportAccess();
            this._accountDataAccess = new AccountDataAccess();
            this._RoleDataAccess = new RoleDataAccess();
        }       
        public ActionResult SADashboard()
        {
            return View();
        }

        public ActionResult TestPage()
        {
            ViewBag.PageTitle = "Users";
            ViewBag.TitleClass = "dripicons-user-group";
            List<UserModel> list = new List<UserModel>();
            list = _userDataAccess.GetUsers();
            return View(list);
        }

        public ActionResult SAECIUser()
        {
            ViewBag.PageTitle = "Users";
            ViewBag.TitleClass = "dripicons-user-group";
            List<UserModel> list = new List<UserModel>();
            list = _userDataAccess.GetUsers();
            return View(list);

        }

        public ActionResult SAECIAccounts()
        {
            ViewBag.PageTitle = "Accounts";
            ViewBag.TitleClass = "dripicons-Contact-group";
            List<AccountModel> list = new List<AccountModel>();
            list = _userDataAccess.GetCompanyListsAll().ToList();
            return View(list);

        }


        public ActionResult SAECIUserAdd()
        {
            UserModel userModel = new UserModel();
            return PartialView(userModel);
        }

        [HttpPost]
        public ActionResult SAECIUserAdd(UserModel userModel)
        {
            if (ModelState.IsValid)
            {
                userModel.UserType = 2;
                userModel.UserStatus = 0;
                userModel.Password = "12345";
               int Result  = _userDataAccess.AddECIUser(userModel);
            }
            return RedirectToAction("SAECIUser");
        }
        

        public ActionResult SAReport()
        {
            //int j = 0;
            //int i = 9;
            //float p = i / j;

            ViewBag.PageTitle = "Reports";
            ViewBag.TitleClass = "dripicons-graph-bar";          
            return View();
        }

        public JsonResult GetReport(int catname, int datalabel, int accountid, int yearid)
        {
            DataTable dt = _reportAccess.GetReports(catname, datalabel, accountid, yearid);

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CheckReportName(int usernum, string reportname)
        {
            int result = _reportAccess.CheckReportName(usernum, reportname);
            return Json(result.ToString(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNewDashboardReport(int catname, string[] datalabel, int accountid, string[] yearid, int compareaccount, string[] compareyear)
        {
            int accountid1 = compareaccount;
            string yearidprime = string.Join(",", yearid);
            string yearid1 = string.Join(",", compareyear);
            string datalabel1 = string.Join(",", datalabel);
            //if (accountyear != "0")
            //{
            //    string[] val = accountyear.Split('-');
            //    accountid1 = Convert.ToInt32(val[0].ToString());
            //    yearid1 = Convert.ToInt32(val[1].ToString());
            //}
            DataTable dt = _reportAccess.GetNewDashboardReport(catname, datalabel1, accountid, yearidprime, accountid1, yearid1);

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNewDashboardReportOverall(int catname, int datalabel, int accountid, int yearid, int compareaccount, int compareyear)
        {
            int accountid1 = compareaccount;
            int yearid1 = compareyear;
            //if (accountyear != "0")
            //{
            //    string[] val = accountyear.Split('-');
            //    accountid1 = Convert.ToInt32(val[0].ToString());
            //    yearid1 = Convert.ToInt32(val[1].ToString());
            //}
            DataTable dt = _reportAccess.GetNewDashboardReportOverall(catname, datalabel, accountid, yearid, accountid1, yearid1);

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNewDashboardReportByFilters(int catname, string[] datalabel, int accountid, string[] yearid, string[] ddlindustry_arry, string[] ddlcmpysize_arry, string[] ddlrevenue_arry, string[] ddlagebrands_arry, string[] ddlgender_arry, string[] ddlrace_arry, string[] ddlgeoregion_arry, string[] ddlsupplier_arry, string[] ddlsupervisor_arry, string[] ddlunion_arry, string[] ddlpublicprivate_arry, string[] ddltenure_arry, string[] ddlsalaried_arry, string[] ddlmanageval_arry, string[] ddlmarketval_arry, int compareaccount, string[] compareyear)
        {
            int accountid1 = compareaccount;
            string yearidprime = string.Join(",", yearid);
            string yearid1 = string.Join(",", compareyear);
            string datalabel1 = string.Join(",", datalabel);
            //if (accountyear != "0")
            //{
            //    string[] val = accountyear.Split('-');
            //    accountid1 = Convert.ToInt32(val[0].ToString());
            //    yearid1 = Convert.ToInt32(val[1].ToString());
            //}
            //string ddlindustry_arry_result = string.Join(",", ddlindustry_arry);
            string ddlindustry_arry_result = string.Join(",", ddlindustry_arry);
            string ddlcmpysize_arry_result = string.Join(",", ddlcmpysize_arry);
            string ddlrevenue_arry_result = string.Join(",", ddlrevenue_arry);
            string ddlagebrands_arry_result = string.Join(",", ddlagebrands_arry);
            string ddlgender_arry_result = string.Join(",", ddlgender_arry);
            string ddlrace_arry_result = string.Join(",", ddlrace_arry);
            string ddlgeoregion_arry_result = string.Join(",", ddlgeoregion_arry);
            string ddlsupplier_arry_result = string.Join(",", ddlsupplier_arry);
            string ddlsupervisor_arry_result = string.Join(",", ddlsupervisor_arry);
            string ddlunion_arry_result = string.Join(",", ddlunion_arry);
            string ddlpublicprivate_arry_result = string.Join(",", ddlpublicprivate_arry);
            string ddltenure_arry_result = string.Join(",", ddltenure_arry);
            string ddlsalaried_arry_result = string.Join(",", ddlsalaried_arry);
            string ddlmanageval_arry_result = string.Join(",", ddlmanageval_arry);
            string ddlmarketval_arry_result = string.Join(",", ddlmarketval_arry);


            DataTable dt = _reportAccess.GetNewDashboardReportByFilter(catname, datalabel1, accountid, yearidprime, ddlindustry_arry_result, ddlcmpysize_arry_result, ddlrevenue_arry_result, ddlagebrands_arry_result, ddlgender_arry_result, ddlrace_arry_result, ddlgeoregion_arry_result, ddlsupplier_arry_result, ddlsupervisor_arry_result, ddlunion_arry_result, ddlpublicprivate_arry_result, ddltenure_arry_result, ddlsalaried_arry_result, ddlmanageval_arry_result, ddlmarketval_arry_result, accountid1, yearid1);

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetNewDashboardOverallByFilter(int catname, int datalabel, int accountid, int yearid, string[] ddlindustry_arry, string[] ddlcmpysize_arry, string[] ddlrevenue_arry, string[] ddlagebrands_arry, string[] ddlgender_arry, string[] ddlrace_arry, string[] ddlgeoregion_arry, string[] ddlsupplier_arry, string[] ddlsupervisor_arry, string[] ddlunion_arry, string[] ddlpublicprivate_arry, string[] ddltenure_arry, string[] ddlsalaried_arry, string[] ddlmanageval_arry, string[] ddlmarketval_arry, int compareaccount, int compareyear)
        {
            int accountid1 = compareaccount;
            int yearid1 = compareyear;

            //if (accountyear != "0")
            //{
            //    string[] val = accountyear.Split('-');
            //    accountid1 = Convert.ToInt32(val[0].ToString());
            //    yearid1 = Convert.ToInt32(val[1].ToString());
            //}

           // string ddlindustry_arry_result = string.Join(",", ddlindustry_arry);
            string ddlindustry_arry_result = string.Join(",", ddlindustry_arry);
            string ddlcmpysize_arry_result = string.Join(",", ddlcmpysize_arry);
            string ddlrevenue_arry_result = string.Join(",", ddlrevenue_arry);
            string ddlagebrands_arry_result = string.Join(",", ddlagebrands_arry);
            string ddlgender_arry_result = string.Join(",", ddlgender_arry);
            string ddlrace_arry_result = string.Join(",", ddlrace_arry);
            string ddlgeoregion_arry_result = string.Join(",", ddlgeoregion_arry);
            string ddlsupplier_arry_result = string.Join(",", ddlsupplier_arry);
            string ddlsupervisor_arry_result = string.Join(",", ddlsupervisor_arry);
            string ddlunion_arry_result = string.Join(",", ddlunion_arry);
            string ddlpublicprivate_arry_result = string.Join(",", ddlpublicprivate_arry);
            string ddltenure_arry_result = string.Join(",", ddltenure_arry);
            string ddlsalaried_arry_result = string.Join(",", ddlsalaried_arry);
            string ddlmanageval_arry_result = string.Join(",", ddlmanageval_arry);
            string ddlmarketval_arry_result = string.Join(",", ddlmarketval_arry);

            DataTable dt = _reportAccess.GetNewDashboardOverallByFilter(catname, datalabel, accountid, yearid, ddlindustry_arry_result, ddlcmpysize_arry_result, ddlrevenue_arry_result, ddlagebrands_arry_result, ddlgender_arry_result, ddlrace_arry_result, ddlgeoregion_arry_result, ddlsupplier_arry_result, ddlsupervisor_arry_result, ddlunion_arry_result, ddlpublicprivate_arry_result, ddltenure_arry_result, ddlsalaried_arry_result, ddlmanageval_arry_result, ddlmarketval_arry_result, accountid1, yearid1);

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }


        public JsonResult AddFilterSettings(int Filtid, int catname, string[] datalabel, int accountid, string[] yearid, string[] ddlindustry_arry, string[] ddlcmpysize_arry, string[] ddlrevenue_arry, string[] ddlagebrands_arry, string[] ddlgender_arry, string[] ddlrace_arry, string[] ddlgeoregion_arry, string[] ddlsupplier_arry, string[] ddlsupervisor_arry, string[] ddlunion_arry, string[] ddlpublicprivate_arry, string[] ddltenure_arry, string[] ddlsalaried_arry, string[] ddlmanageval_arry, string[] ddlmarketval_arry, int accountyear, string usernum, string reportname, int funtype, string[] compareyear1)
        {            
            int accountid1 = accountyear;
            string yearidprime = string.Join(",", yearid);
            string yearid1 = string.Join(",", compareyear1);
            string datalabel1 = string.Join(",", datalabel);
            //if (accountyear != "0")
            //{
            //    string[] val = accountyear.Split('-');
            //    accountid1 = Convert.ToInt32(val[0].ToString());
            //    yearid1 = Convert.ToInt32(val[1].ToString());
            //}
            // string ddlindustry_arry_result = string.Join(",", ddlindustry_arry);
            string ddlindustry_arry_result = string.Join(",", ddlindustry_arry);
            string ddlcmpysize_arry_result = string.Join(",", ddlcmpysize_arry);
            string ddlrevenue_arry_result = string.Join(",", ddlrevenue_arry);
            string ddlagebrands_arry_result = string.Join(",", ddlagebrands_arry);
            string ddlgender_arry_result = string.Join(",", ddlgender_arry);
            string ddlrace_arry_result = string.Join(",", ddlrace_arry);
            string ddlgeoregion_arry_result = string.Join(",", ddlgeoregion_arry);
            string ddlsupplier_arry_result = string.Join(",", ddlsupplier_arry);
            string ddlsupervisor_arry_result = string.Join(",", ddlsupervisor_arry);
            string ddlunion_arry_result = string.Join(",", ddlunion_arry);
            string ddlpublicprivate_arry_result = string.Join(",", ddlpublicprivate_arry);
            string ddltenure_arry_result = string.Join(",", ddltenure_arry);
            string ddlsalaried_arry_result = string.Join(",", ddlsalaried_arry);
            string ddlmanageval_arry_result = string.Join(",", ddlmanageval_arry);
            string ddlmarketval_arry_result = string.Join(",", ddlmarketval_arry);

            int Result = _reportAccess.AddFilterSettings(Filtid, int.Parse(usernum), catname, datalabel1, accountid, yearidprime, reportname, int.Parse(usernum), ddlindustry_arry_result, ddlcmpysize_arry_result, ddlrevenue_arry_result, ddlagebrands_arry_result, ddlgender_arry_result, ddlrace_arry_result, ddlgeoregion_arry_result, ddlsupplier_arry_result, ddlsupervisor_arry_result, ddlunion_arry_result, ddlpublicprivate_arry_result, ddltenure_arry_result, ddlsalaried_arry_result, ddlmanageval_arry_result, ddlmarketval_arry_result, accountid1, funtype, yearid1);
            
            return Json(Result.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddBuilderFilterSettings(int Filtid, int catname, string[] datalabel, int accountid, string[] yearid, string[] ddlindustry_arry, string[] ddlcmpysize_arry, string[] ddlrevenue_arry, string[] ddlagebrands_arry, string[] ddlgender_arry, string[] ddlrace_arry, string[] ddlgeoregion_arry, string[] ddlsupplier_arry, string[] ddlsupervisor_arry, string[] ddlunion_arry, string[] ddlpublicprivate_arry, string[] ddltenure_arry, string[] ddlsalaried_arry, string[] ddlmanageval_arry, string[] ddlmarketval_arry, int accountyear, string usernum, string reportname, int funtype, string[] compareyear1,int  accountid2, string[] yearid2, string Folderpath)
        {
            int accountid1 = accountyear;
            string yearidprime = string.Join(",", yearid);
            string yearid1 = string.Join(",", compareyear1);
            string yearcomp = string.Join(",", yearid2);
            string datalabel1 = string.Join(",", datalabel);
            //if (accountyear != "0")
            //{
            //    string[] val = accountyear.Split('-');
            //    accountid1 = Convert.ToInt32(val[0].ToString());
            //    yearid1 = Convert.ToInt32(val[1].ToString());
            //}
            // string ddlindustry_arry_result = string.Join(",", ddlindustry_arry);
            string ddlindustry_arry_result = string.Join(",", ddlindustry_arry);
            string ddlcmpysize_arry_result = string.Join(",", ddlcmpysize_arry);
            string ddlrevenue_arry_result = string.Join(",", ddlrevenue_arry);
            string ddlagebrands_arry_result = string.Join(",", ddlagebrands_arry);
            string ddlgender_arry_result = string.Join(",", ddlgender_arry);
            string ddlrace_arry_result = string.Join(",", ddlrace_arry);
            string ddlgeoregion_arry_result = string.Join(",", ddlgeoregion_arry);
            string ddlsupplier_arry_result = string.Join(",", ddlsupplier_arry);
            string ddlsupervisor_arry_result = string.Join(",", ddlsupervisor_arry);
            string ddlunion_arry_result = string.Join(",", ddlunion_arry);
            string ddlpublicprivate_arry_result = string.Join(",", ddlpublicprivate_arry);
            string ddltenure_arry_result = string.Join(",", ddltenure_arry);
            string ddlsalaried_arry_result = string.Join(",", ddlsalaried_arry);
            string ddlmanageval_arry_result = string.Join(",", ddlmanageval_arry);
            string ddlmarketval_arry_result = string.Join(",", ddlmarketval_arry);

            int Result = _reportAccess.AddBuilderFilterSettings(Filtid, int.Parse(usernum), catname, datalabel1, accountid, yearidprime, reportname, int.Parse(usernum), ddlindustry_arry_result, ddlcmpysize_arry_result, ddlrevenue_arry_result, ddlagebrands_arry_result, ddlgender_arry_result, ddlrace_arry_result, ddlgeoregion_arry_result, ddlsupplier_arry_result, ddlsupervisor_arry_result, ddlunion_arry_result, ddlpublicprivate_arry_result, ddltenure_arry_result, ddlsalaried_arry_result, ddlmanageval_arry_result, ddlmarketval_arry_result, accountid1, funtype, yearid1, accountid2, yearcomp, Folderpath);

            return Json(Result.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteUser(int Userid)
        {
            int result = _userDataAccess.Deleteuser(Userid);
            return Json(result.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult checkmailid(string Email)
        {
            int result = _userDataAccess.checkmailid(Email);
            return Json(result.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult checkaccount(string companyname)
        {
            int result = _userDataAccess.checkaccount(companyname);
            return Json(result.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Deleteaccount(int accid)
        {
            int result = _userDataAccess.Deleteaccount(accid);
            return Json(result.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteRole(int RoleId)
        {
            int result = _userDataAccess.DeleteRole(RoleId);
            return Json(result.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetReportBuilderNewReport(int catname, string[] datalabel, int accountid, string[] yearid, string[] ddlindustry_arry, string[] ddlcmpysize_arry, string[] ddlrevenue_arry, string[] ddlagebrands_arry, string[] ddlgender_arry, string[] ddlrace_arry, string[] ddlgeoregion_arry, string[] ddlsupplier_arry, string[] ddlsupervisor_arry, string[] ddlunion_arry, string[] ddlpublicprivate_arry, string[] ddltenure_arry, string[] ddlsalaried_arry, string[] ddlmanageval_arry, string[] ddlmarketval_arry, int compareaccount, string[] compareyear, int accountid2, string[] yearid2)
        {
            int accountid1 = compareaccount;
            string yearidprime = string.Join(",", yearid);
            string yearid1 = string.Join(",", compareyear);
            string yearidexist = string.Join(",", yearid2);
            string datalabel1 = string.Join(",", datalabel);
            //if (accountyear != "0")
            //{
            //    string[] val = accountyear.Split('-');
            //    accountid1 = Convert.ToInt32(val[0].ToString());
            //    yearid1 = Convert.ToInt32(val[1].ToString());
            //}
            //string ddlindustry_arry_result = string.Join(",", ddlindustry_arry);
            string ddlindustry_arry_result = string.Join(",", ddlindustry_arry);
            string ddlcmpysize_arry_result = string.Join(",", ddlcmpysize_arry);
            string ddlrevenue_arry_result = string.Join(",", ddlrevenue_arry);
            string ddlagebrands_arry_result = string.Join(",", ddlagebrands_arry);
            string ddlgender_arry_result = string.Join(",", ddlgender_arry);
            string ddlrace_arry_result = string.Join(",", ddlrace_arry);
            string ddlgeoregion_arry_result = string.Join(",", ddlgeoregion_arry);
            string ddlsupplier_arry_result = string.Join(",", ddlsupplier_arry);
            string ddlsupervisor_arry_result = string.Join(",", ddlsupervisor_arry);
            string ddlunion_arry_result = string.Join(",", ddlunion_arry);
            string ddlpublicprivate_arry_result = string.Join(",", ddlpublicprivate_arry);
            string ddltenure_arry_result = string.Join(",", ddltenure_arry);
            string ddlsalaried_arry_result = string.Join(",", ddlsalaried_arry);
            string ddlmanageval_arry_result = string.Join(",", ddlmanageval_arry);
            string ddlmarketval_arry_result = string.Join(",", ddlmarketval_arry);


            DataTable dt = _reportAccess.GetReportBuilderNewReport(catname, datalabel1, accountid, yearidprime, ddlindustry_arry_result, ddlcmpysize_arry_result, ddlrevenue_arry_result, ddlagebrands_arry_result, ddlgender_arry_result, ddlrace_arry_result, ddlgeoregion_arry_result, ddlsupplier_arry_result, ddlsupervisor_arry_result, ddlunion_arry_result, ddlpublicprivate_arry_result, ddltenure_arry_result, ddlsalaried_arry_result, ddlmanageval_arry_result, ddlmarketval_arry_result, accountid1, yearid1, accountid2, yearidexist);

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCategoryList(int accountid, string[] yearid,int accountid1, string[] yearid1)
        {
            string yearid_arry = string.Join(",", yearid);
            string yearid1_arry = string.Join(",", yearid1);
            DataTable dt = _reportAccess.GetCategoryList(accountid, yearid_arry, accountid1, yearid1_arry);

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCategoryList1()
        {
            DataTable dt = _reportAccess.GetCategoryList1();

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetECIFilter(int catname, string[] datalabel, int accountid, string[] yearid, int compareaccount, string[] compareyear)
        {
            int Yearcount = 0;
            if (compareyear[0].ToString() == "0")
            {
                Yearcount = yearid.Length;
            }
            else
            {
                Yearcount = yearid.Length + compareyear.Length;
            }
            int accountid1 = compareaccount;
            string yearidprime = string.Join(",", yearid);
            string yearid1 = string.Join(",", compareyear);
            string datalabel1 = string.Join(",", datalabel);
           // _reportAccess.GetNewDashboardReport(catname, datalabel1, accountid, yearidprime, accountid1, yearid1);
            DataTable dt = _reportAccess.GetECIFilter(catname, datalabel1, accountid, yearidprime, accountid1, yearid1, Yearcount);
            

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetECIFilterReportBuilderNew(int catname, string[] datalabel, int accountid, string[] yearid, int compareaccount, string[] compareyear, int accountid2, string[] yearid2)
        {
            int Yearcount = 0;
            if (compareyear[0].ToString() == "0")
            {
                Yearcount = yearid.Length + yearid2.Length;
            }
            else
            {
                Yearcount = yearid.Length + compareyear.Length + yearid2.Length;
            }
            //int Yearcount = yearid.Length + compareyear.Length + yearid2.Length;
            int accountid1 = compareaccount;
            string yearidprime = string.Join(",", yearid);
            string yearid1 = string.Join(",", compareyear);
            string yearidexist = string.Join(",", yearid2);
            string datalabel1 = string.Join(",", datalabel);
            // _reportAccess.GetNewDashboardReport(catname, datalabel1, accountid, yearidprime, accountid1, yearid1);
            DataTable dt = _reportAccess.GetECIFilterReportBuilderNew(catname, datalabel1, accountid, yearidprime, accountid1, yearid1, Yearcount, accountid2, yearidexist);


            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDataLabelListByCatID(string catid,string[] secid)
        {
            if (catid == "")
            {
                catid = "0";
            }
            string secidVal = string.Join(",", secid);
            DataTable dt = _reportAccess.GetDataLabelListByCatID(Convert.ToInt32(catid), secidVal);

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCatSectionListByCatID(string catid)
        {
            if (catid == "")
            {
                catid = "0";
            }
            DataTable dt = _reportAccess.GetCatSectionListByCatID(Convert.ToInt32(catid));

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCompanyLists(int Usernum)
        {
            DataTable dt = _reportAccess.GetCompanyLists(Usernum);

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetYears(int companyid)
        {
            //DataTable dt = new DataTable();
            //dt.Columns.Add("yearid");
            //dt.Columns.Add("yearname");

            //for(int i = DateTime.Now.Year; i >= 2000; i--)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr[0] = i.ToString();
            //    dr[1] = i.ToString();
            //    dt.Rows.Add(dr);
            //    dt.AcceptChanges();
            //}

            DataTable dt = _reportAccess.GetYear(companyid);
            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);
            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetComparecompany()
        {
            //DataTable dt = new DataTable();
            //dt.Columns.Add("yearid");
            //dt.Columns.Add("yearname");

            //for(int i = DateTime.Now.Year; i >= 2000; i--)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr[0] = i.ToString();
            //    dr[1] = i.ToString();
            //    dt.Rows.Add(dr);
            //    dt.AcceptChanges();
            //}

            DataTable dt = _reportAccess.GetCompanyCompare();
            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);
            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetYears1()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("yearid");
            dt.Columns.Add("yearname");

            for (int i = DateTime.Now.Year; i >= 2000; i--)
            {
                DataRow dr = dt.NewRow();
                dr[0] = i.ToString();
                dr[1] = i.ToString();
                dt.Rows.Add(dr);
                dt.AcceptChanges();
            }


            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);
            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InsertUserPreference(string category, string year, string company, string label, string groupby, string matrics, string ReportName,string SaveStatus,string X,string Y)
        {
          //Save Status , 1=Save ,2=TempSave,3=save and run
          

            string[] categoryarr = category.Remove(0,10).Split(',');
            string[] yeararr = year.Remove(0, 10).Split(',');
            string[] companyarr = company.Remove(0, 10).Split(',');
            string[] labelarr = label.Remove(0, 10).Split(',');
            string[] groupbyarr = groupby.Remove(0, 10).Split(',');
            string[] matricsarr = matrics.Remove(0, 10).Split(',');


            string xmlparmval = "<Parm>";
            foreach (string val in categoryarr)
            {
          //      id =\"" + dr.id + "\"
                xmlparmval = xmlparmval + "<Parmid  RptCat =\" Category \" " + " RptPlace =\" Filter \" " + " ParmValue =\"" + val + "\" ></Parmid> ";
            }
            foreach (string val in yeararr)
            {
                xmlparmval = xmlparmval + "<Parmid  RptCat =\" SurveyYear \" " + " RptPlace =\" Filter \" " + " ParmValue =\"" + val + "\" ></Parmid> ";
            }
            foreach (string val in companyarr)
            {
                xmlparmval = xmlparmval + "<Parmid  RptCat =\" Company \" " + " RptPlace =\" Filter \" " + " ParmValue =\"" + val + "\" ></Parmid> ";
            }
            foreach (string val in labelarr)
            {
                xmlparmval = xmlparmval + "<Parmid  RptCat =\" DataLabel \" " + " RptPlace =\" Filter \" " + " ParmValue =\"" + val + "\" ></Parmid> ";
            }
            foreach (string val in groupbyarr)
            {

                xmlparmval = xmlparmval + "<Parmid  RptCat =\" GroupBy \" " + " RptPlace =\" GroupBy \" " + " ParmValue =\"" + val + "\" ></Parmid> ";
            }
            xmlparmval = xmlparmval + "</Parm>";

            string xmlmatricsval = "<Mats>";

            foreach (string val in matricsarr)
            {
                xmlmatricsval = xmlmatricsval + "<Matid  Mat =\"" + val + "\" ></Matid> ";
            }
            xmlmatricsval = xmlmatricsval + "</Mats>";

            int creartedby = 0;
            string Result = "";
            if (Session["SessionUser"] != null)
            {
                SessionUser SessionUser = (SessionUser)Session["SessionUser"];
                creartedby = SessionUser.UserNum;

                Result = _reportAccess.AddUserPref(xmlparmval, xmlmatricsval, creartedby, ReportName, creartedby,Convert.ToInt32(SaveStatus),X,Y);
            }

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult WIP()
        {           
            return View();
        }

        public ActionResult WIPROLE()
        {
            return View();
        }
        public ActionResult SetColor()
        {
            return View();
        }
        public JsonResult FetchColors()
        {
            var count = 0;
            var sb = new StringBuilder();
            sb.Append("<table><tbody><tr>");

            foreach (var color in Enum.GetNames(typeof(KnownColor)))
            {
                var colorValue = ColorTranslator.FromHtml(color);
                var html = string.Format("#{0:X2}{1:X2}{2:X2}",
                                    colorValue.R, colorValue.G, colorValue.B);
                sb.AppendFormat("<td bgcolor=\"{0}\">&nbsp;</td>", html);
                if (count < 20)
                {
                    count++;
                }
                else
                {
                    sb.Append("</tr><tr>");
                    count = 0;
                }
            }
            sb.Append("</tbody></table>");
            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Accounts()
        {
            AccountModel accountModel = new AccountModel();
            accountModel.id = 0;
           // accountModel.Employees = 1;
           // accountModel.Annual_Revenue = 1;
            return View(accountModel);
        }

        public ActionResult Contacts()
        {
            ProfileModel profileModel = new ProfileModel();
            profileModel.CompanyList = new SelectList(_userDataAccess.GetCompanyListsAll(),"id","companyName");
            profileModel.primeid = new int[] {0};
            profileModel.compid = new int[] {0};
            return View(profileModel);
        }

        public ActionResult User()
        {
            return View();
        }

        public ActionResult SADashboardOld()
        {
            return View("SADashboard-old");
        }

        [HttpPost]
        public ActionResult Accounts(AccountModel accountModel,string UserNum)
        {
            if (ModelState.IsValid)
            {
                if (accountModel.id != 0)
                {
                    //update
                    //if (Session["SessionUser"] != null)
                    //{
                       // SessionUser SessionUser = (SessionUser)Session["SessionUser"];
                    accountModel.createdby = Convert.ToInt32(UserNum);//SessionUser.UserNum;
                        int Result = _accountDataAccess.UpdateAccount(accountModel);

                        if (Result == 1)
                        {
                            return RedirectToAction("SAECIAccounts");
                        }
                    //}
                    return View("Accounts", accountModel);
                }
                else //if (Session["SessionUser"] != null)
                {
                    //SessionUser SessionUser = (SessionUser)Session["SessionUser"];
                    //accountModel.createdby = SessionUser.UserNum;
                    accountModel.createdby = Convert.ToInt32(UserNum);
                    int Result = _accountDataAccess.AddAccount(accountModel);

                    if (Result >= 1)
                    {
                        return RedirectToAction("SAECIAccounts");
                    }
                }
            }
            return View("Accounts", accountModel);
        }
        [HttpPost]
        public ActionResult Contacts(ProfileModel profileModel)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(profileModel.id) != true)
                {
                    //update
                    //if (Session["SessionUser"] != null)
                    //{
                    //    SessionUser SessionUser = (SessionUser)Session["SessionUser"];
                    //    profileModel.createdby = SessionUser.UserNum;
                        
                    //}

                    int Result = _userDataAccess.UpdateProfile(profileModel);

                    if (Result > 0)
                    {
                        if(profileModel.FromScreen == "userlist")
                        {
                            return RedirectToAction("SAECIUser");
                        }
                        else
                        {
                            if (profileModel.LoggedInUserType == "ECI Client User")
                            {
                                return RedirectToAction("DataAnalysisNew");
                            }
                            else
                            {
                                return RedirectToAction("AlterDashboard");
                            }
                        }
                    }


                    profileModel.CompanyList = new SelectList(_userDataAccess.GetCompanyListsAll(), "id", "companyName");
                    profileModel.primeid = new int[] { 0 };
                    profileModel.compid = new int[] { 0 };
                    return View("Contacts", profileModel);
                }
                else
                {
                    var urlBuilder =
  new System.UriBuilder(Request.Url.AbsoluteUri)
  {
      Path = Url.Action("ResetPass", "Home"),
      Query = null,
  };

                    Uri uri = urlBuilder.Uri;
                    string url = urlBuilder.ToString();

                    profileModel.AppURL = url;

                    int Result = _userDataAccess.AddProfile(profileModel);

                    if (Result >= 1)
                    {
                        return RedirectToAction("SAECIUser");
                    }
                }
              //else  if (Session["SessionUser"] != null)
              //  {
              //      SessionUser SessionUser = (SessionUser)Session["SessionUser"];
              //      profileModel.createdby = SessionUser.UserNum;

                    
              //  }
               
            }
            profileModel.CompanyList = new SelectList(_userDataAccess.GetCompanyListsAll(), "id", "companyName");
            profileModel.primeid = new int[] { 0 };
            profileModel.compid = new int[] { 0 };
            return View(profileModel);
        }

        public ActionResult EditAccount(int accid)
        {

            DataTable dt = new DataTable();
            dt = _accountDataAccess.GetAccountbyid(accid);
            AccountModel accountModel = new AccountModel();
            foreach (DataRow dr in dt.Rows)
            {
                accountModel.id = accid;
                accountModel.companyName = dr["companyname"].ToString();
                accountModel.AccOwner = dr["AccOwner"].ToString();
                accountModel.Industry = dr["Industry"].ToString();
                if (dr["Employees"].ToString() != "")
                {
                    accountModel.Employees = Convert.ToInt32(dr["Employees"]);
                }

                if (dr["Annual_Revenue"].ToString() != "")
                {
                    accountModel.Annual_Revenue = Convert.ToInt32(dr["Annual_Revenue"]);
                }
                accountModel.Telephone = dr["Telephone"].ToString();
                accountModel.Mailing_Street = dr["Mailing_Street"].ToString();
                accountModel.Mailing_City = dr["Mailing_City"].ToString();
                accountModel.Mailing_State = dr["Mailing_State"].ToString();
                accountModel.Mailing_Country = dr["Mailing_Country"].ToString();
                accountModel.Mailing_Postal_Code = dr["Mailing_Postal_Code"].ToString();
                accountModel.Website = dr["Website"].ToString(); ;
                accountModel.Description = dr["Description"].ToString();
                accountModel.AliasName = dr["AliasName"].ToString();
            }

            return View("Accounts", accountModel);
        }

        public ActionResult EditUser(int userid, string FromScreen)
        {
            DataTable dt = new DataTable();
            ProfileModel profileModel = new ProfileModel();
            profileModel.FromScreen = FromScreen;
            profileModel.CompanyList = new SelectList(_userDataAccess.GetCompanyListsAll(), "id", "companyName");
            dt = _userDataAccess.GetProfilebyuserid(userid);
            foreach (DataRow dr in dt.Rows)
            {
                profileModel.id = dr["id"].ToString();
                profileModel.firstname = SecureLogic.Decrypt(dr["firstname"].ToString());
                profileModel.lastname = SecureLogic.Decrypt(dr["lastname"].ToString());
                profileModel.email = SecureLogic.Decrypt(dr["email"].ToString());
                profileModel.Middle_Name = dr["Middle_Name"].ToString() != "" ? SecureLogic.Decrypt(dr["Middle_Name"].ToString()) : dr["Middle_Name"].ToString();
                profileModel.Tittle = dr["Tittle"].ToString() != "" ? SecureLogic.Decrypt(dr["Tittle"].ToString()) : dr["Tittle"].ToString();
                profileModel.Division = dr["Division"].ToString() != "" ? SecureLogic.Decrypt(dr["Division"].ToString()) : dr["Division"].ToString();
                //string birthdayval = SecureLogic.Decrypt(dr["Brithday"].ToString());
                //birthdayval = Convert.ToDateTime(birthdayval).ToString("yyyy-MM-dd");
                profileModel.Brithday = dr["Brithday"].ToString() != "" ? SecureLogic.Decrypt(dr["Brithday"].ToString()) : dr["Brithday"].ToString();


                profileModel.Mobile = dr["Mobile"].ToString() != "" ? SecureLogic.Decrypt(dr["Mobile"].ToString()) : dr["Mobile"].ToString();
                profileModel.Contact_Owner =dr["Contact_Owner"].ToString() != "" ? SecureLogic.Decrypt(dr["Contact_Owner"].ToString()) : dr["Contact_Owner"].ToString();
                profileModel.Telephone = dr["Telephone"].ToString() !="" ? SecureLogic.Decrypt(dr["Telephone"].ToString()) : dr["Telephone"].ToString();
                profileModel.Mailing_Street =dr["Mailing_Street"].ToString() != "" ? SecureLogic.Decrypt(dr["Mailing_Street"].ToString()) :dr["Mailing_Street"].ToString();
                profileModel.Mailing_City = dr["Mailing_City"].ToString() != "" ? SecureLogic.Decrypt(dr["Mailing_City"].ToString()) : dr["Mailing_City"].ToString();
                profileModel.Mailing_State = dr["Mailing_State"].ToString() != "" ? SecureLogic.Decrypt(dr["Mailing_State"].ToString()) : dr["Mailing_State"].ToString();
                profileModel.Mailing_Postal_Code = dr["Mailing_Postal_Code"].ToString() != "" ? SecureLogic.Decrypt(dr["Mailing_Postal_Code"].ToString()) : dr["Mailing_Postal_Code"].ToString();
                profileModel.Mailing_Country = dr["Mailing_Country"].ToString() != "" ? SecureLogic.Decrypt(dr["Mailing_Country"].ToString()) : dr["Mailing_Country"].ToString();
                profileModel.Website = dr["Website"].ToString() != "" ? SecureLogic.Decrypt(dr["Website"].ToString()) : dr["Website"].ToString();
                profileModel.Description = dr["Description"].ToString() != "" ? SecureLogic.Decrypt(dr["Description"].ToString()) : dr["Description"].ToString();
                
                profileModel.ExpiryDate = dr["ExpiryDate"].ToString() != "" ? Convert.ToDateTime(dr["ExpiryDate"]).ToString("dd/MM/yyyy") : dr["ExpiryDate"].ToString();
               
               // profileModel.ExpiryDate = Convert.ToDateTime(dr["ExpiryDate"]).ToString("yyyy-MM-dd");
                profileModel.companyid = Convert.ToInt32(dr["accountid"].ToString());
                profileModel.roleid = Convert.ToInt32(dr["UserType"]);
                profileModel.primeid = dr["PrimaryID"].ToString().Split(',').Select(n => Convert.ToInt32(n)).ToArray();
                profileModel.compid =dr["ComparisionID"].ToString().Split(',').Select(n => Convert.ToInt32(n)).ToArray();
                    
            }

            return View("Contacts", profileModel);
        }
               
        public ActionResult SAECIRoles()
        {
            ViewBag.PageTitle = "Roles";
            ViewBag.TitleClass = "dripicons-Contact-group";
            List<RoleModel> list = new List<RoleModel>();
            list = _RoleDataAccess.GetAllRoles().ToList();
            return View(list);

        }

        public ActionResult AddRole()
        {
            ViewBag.PageTitle = "Add Role";
            List<TreeViewNode> nodes = new List<TreeViewNode>();

            DataTable dt = new DataTable();
            dt = _RoleDataAccess.GetAllMenus();

            foreach (DataRow dr in dt.Select("Parentid = 0", "OrderID asc"))
            {
                if (dr["IsHasChild"].ToString().ToLower() == "true")
                {
                    nodes.Add(new TreeViewNode { id = dr["Id"].ToString(), parent = "#", text = dr["menuname"].ToString() });

                    foreach (DataRow drchild in dt.Select("Parentid = " + dr["id"].ToString(), "OrderID asc"))
                    {
                        nodes.Add(new TreeViewNode { id = drchild["Id"].ToString(), parent = dr["id"].ToString(), text = drchild["menuname"].ToString() });
                    }                   
                }
                else
                {
                    nodes.Add(new TreeViewNode { id = dr["Id"].ToString(), parent = "#", text = dr["menuname"].ToString() });
                }
            }
           

            //Serialize to JSON string.
            ViewBag.Json = (new JavaScriptSerializer()).Serialize(nodes);
            return View();
        }

        public ActionResult EditRole(int Roleid)
        {
            ViewBag.PageTitle = "Edit Role";
            List<TreeViewNode> nodes = new List<TreeViewNode>();

            DataTable dt = new DataTable();
            dt = _RoleDataAccess.GetAllMenus();

            foreach (DataRow dr in dt.Select("Parentid = 0", "OrderID asc"))
            {
                if (dr["IsHasChild"].ToString().ToLower() == "true")
                {
                    nodes.Add(new TreeViewNode { id = dr["Id"].ToString(), parent = "#", text = dr["menuname"].ToString() });

                    foreach (DataRow drchild in dt.Select("Parentid = " + dr["id"].ToString(), "OrderID asc"))
                    {
                        nodes.Add(new TreeViewNode { id = drchild["Id"].ToString(), parent = dr["id"].ToString(), text = drchild["menuname"].ToString() });
                    }
                }
                else
                {
                    nodes.Add(new TreeViewNode { id = dr["Id"].ToString(), parent = "#", text = dr["menuname"].ToString() });
                }
            }


            //Serialize to JSON string.
            ViewBag.Json = (new JavaScriptSerializer()).Serialize(nodes);
            ViewBag.Message = Roleid.ToString();
          //  ViewBag.Json1 = (new JavaScriptSerializer()).Serialize(nodes);
            return View();
        }

        [HttpPost]
        public ActionResult EditRole(int RoleId,string selectedItems, string RoleName, int createdby)
        {
            List<TreeViewNode> items = (new JavaScriptSerializer()).Deserialize<List<TreeViewNode>>(selectedItems);
            string xmlval = "<roles>";
            //<roles><roleid id=''1''></roleid><roleid id=''2''></roleid></roles>
            foreach (TreeViewNode dr in items)
            {
                xmlval = xmlval + "<roleid id=\"" + dr.id + "\" ></roleid> ";
            }
            xmlval = xmlval + "</roles>";
            //int creartedby = 0;
            //if (Session["SessionUser"] != null)
            //{
            //    SessionUser SessionUser = (SessionUser)Session["SessionUser"];
            //    creartedby = SessionUser.UserNum;



            //}

            int Result = _RoleDataAccess.UpdateRoleloop(RoleId, xmlval, RoleName, createdby);

            if (Result == 1)
            {
                return RedirectToAction("SAECIRoles");
            }

            return View("SAECIRoles");

        }

        [HttpPost]
        public ActionResult AddRole(string selectedItems, string RoleName, int createdby)
        {
            List<TreeViewNode> items = (new JavaScriptSerializer()).Deserialize<List<TreeViewNode>>(selectedItems);
            string xmlval = "<roles>";
            //<roles><roleid id=''1''></roleid><roleid id=''2''></roleid></roles>
            foreach (TreeViewNode dr in items)
            {
                xmlval = xmlval + "<roleid id=\""+dr.id+ "\" ></roleid> ";
            }
            xmlval = xmlval + "</roles>";
            //int creartedby = 0;
            //if (Session["SessionUser"] != null)
            //{
            //    SessionUser SessionUser = (SessionUser)Session["SessionUser"];
            //    creartedby = SessionUser.UserNum;



            //}

            int Result = _RoleDataAccess.AddRoleloop(xmlval, RoleName, createdby);

            if (Result == 1)
            {
                return RedirectToAction("SAECIRoles");
            }

            return View("SAECIRoles");

        }

        public ActionResult LinkUserRole(string msg = null)
        {
            ViewBag.PageTitle = "Link UserRole";
            List<TreeViewNode> nodes = new List<TreeViewNode>();

            var rolelist = _RoleDataAccess.GetAllRoles().ToList();

            //SelState state = new SelState { selected = "false" };

            foreach (RoleModel RModel in rolelist)
            {
                // nodes.Add(new TreeViewNode { id = RModel.RoleID.ToString(), parent = "#", text = RModel.RoleName.ToString(), state = state });
                nodes.Add(new TreeViewNode { id = RModel.RoleID.ToString(), parent = "#", text = RModel.RoleName.ToString()});
            }

            //Serialize to JSON string.
            ViewBag.Json = (new JavaScriptSerializer()).Serialize(nodes);

            //string jsonval = ViewBag.Json;

            //jsonval = jsonval.Replace("_val", "");

            //ViewBag.Json = jsonval;

            LinkUserRoleModel profileModel = new LinkUserRoleModel();
            profileModel.UserList = new SelectList(_userDataAccess.GetUsers(), "UserNum", "Emailid");

            ViewBag.Message = msg;

            return View(profileModel);
        }

        [HttpPost]
        public ActionResult LinkUserRole(string selectedItems, LinkUserRoleModel linkUserRoleModel, int createdby)
        {
            List<TreeViewNode> items = (new JavaScriptSerializer()).Deserialize<List<TreeViewNode>>(selectedItems);
            string xmlval = "<roles>";
            //<roles><roleid id=''1''></roleid><roleid id=''2''></roleid></roles>
            foreach (TreeViewNode dr in items)
            {
                xmlval = xmlval + "<roleid id=\"" + dr.id + "\" ></roleid> ";
            }
            xmlval = xmlval + "</roles>";
            int creartedby = 0;
            //if (Session["SessionUser"] != null)
            //{
            //    SessionUser SessionUser = (SessionUser)Session["SessionUser"];
            creartedby = createdby;

                int Result = _RoleDataAccess.AddUserRole(xmlval, linkUserRoleModel.UserNum, creartedby);

                if (Result == 1)
                {
                    return RedirectToAction("LinkUserRole", new { msg = "Roles are linked to selected user." });
                }
            //}


            return RedirectToAction("LinkUserRole", new { msg = "Unable to link" });

        }

        public ActionResult UserPreference()
        {
            ViewBag.PageTitle = "Report Generator";
            ViewBag.TitleClass = "dripicons-Contact-group";
            List<UserRefModel> list = new List<UserRefModel>();
            if (Session["SessionUser"] != null)
            {
                SessionUser SessionUser = (SessionUser)Session["SessionUser"];
                list = _reportAccess.GetUserReports(SessionUser.UserNum).ToList();
            }
            return View(list);
        }
        [HttpPost]
        public ActionResult UserPreference(string selectedItems, string RoleName)
        {            


            return View("UserPreference");

        }

        //public ActionResult UserReferencePost()
        //{

        //    return View();
        //}
        //[HttpPost]
        //public ActionResult UserReferencePost(string selectedItems, string ReportName)
        //{
        //    List<TreeViewNode> items = (new JavaScriptSerializer()).Deserialize<List<TreeViewNode>>(selectedItems);
        //    string xmlval = "<Parm>";
        //    //<roles><roleid id=''1''></roleid><roleid id=''2''></roleid></roles>
        //    foreach (TreeViewNode dr in items)
        //    {
        //        xmlval = xmlval + "<Parmid  RptCat=\"" + dr.id + "\", RptPlace=\"" + dr.id + "\", ParmValue=\"" + dr.id + "\" ></Parmid> ";
        //    }
        //    xmlval = xmlval + "</Parm>";
        //    int creartedby = 0;
        //    if (Session["SessionUser"] != null)
        //    {
        //        SessionUser SessionUser = (SessionUser)Session["SessionUser"];
        //        creartedby = SessionUser.UserNum;

        //        //int Result = _RoleDataAccess.AddRouserleloop(xmlval, RoleName, creartedby);
        //        UserRefModel userRefModel = new UserRefModel();

        //        int Result = _reportAccess.AddUserPref(userRefModel);

        //        if (Result == 1)
        //        {
        //            return RedirectToAction("SAECIRoles");
        //        }

        //    }


        //    return View("SAECIRoles");

        //}

        public ActionResult ExportData(int reportid, string reportname)
        {
            DataTable tableDataSet = _reportAccess.Getreportdata(reportid).Tables[0];

            string data = ""; //will store file data
            bool flag = true;
            string headings = ""; //to store column headings
            foreach (DataRow row in tableDataSet.Rows)
            {
                data += "\n"; // New line for each column
                foreach (DataColumn column in tableDataSet.Columns)
                {
                    if (flag)
                        headings += column + ",";
                    data += row[column] + ",";
                }
                flag = false; //once we get heading setting to false;
            }
            data = headings + data;

            Response.Clear();
            Response.ContentType = "application/CSV";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + reportname + ".csv\"");
            Response.Write(data);
            Response.End();
            return RedirectToAction("UserPreference");
        }
       
        public ActionResult CustomReport(int Reportid, string Reportname)
        {
            ViewBag.ReportName = Reportname;
            ViewBag.PageTitle = Reportname;
            ViewBag.TitleClass = "dripicons-Contact-group";

            DataSet tableDataSet = _reportAccess.Getreportdata(Reportid);

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(tableDataSet.Tables[1]);
            //ViewBag.DataValues = jsonlist;

            ViewBag.DataValues = tableDataSet.Tables[0];

            UserRefModel userRefModel = new UserRefModel
            {
                Reportid = Reportid,
                ReportName = Reportname,
                fulldescription = "",
                canvasdata = "",
                chartdata = JsonConvert.SerializeObject(jsonlist)
            };

            return View(userRefModel);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CustomReport(UserRefModel userRefModel)
        {
            string path = @"D:\";
            string fileNameWitPath = path + DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "") + ".jpeg";
            using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    byte[] data = Convert.FromBase64String(userRefModel.canvasdata);
                    bw.Write(data);
                    bw.Close();

                }
            }

            return RedirectToAction("CustomReport");
        }

        public class donutDataCls
        {
            public string label { get; set; }
            public string value { get; set; }
        }
        protected override void OnException(ExceptionContext exceptionContext)
        {
            if (!exceptionContext.ExceptionHandled)
            {
                string controllerName = (string)exceptionContext.RouteData.Values["controller"];
                string actionName = (string)exceptionContext.RouteData.Values["action"];

                Exception custException = new Exception("There is some error");

                //Log.Error(exceptionContext.Exception.Message + " in " + controllerName);

                var model = new HandleErrorInfo(custException, controllerName, actionName);

                exceptionContext.Result = new ViewResult
                {
                    ViewName = "~/Views/Shared/Error.cshtml",
                    ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
                    TempData = exceptionContext.Controller.TempData
                };

                exceptionContext.ExceptionHandled = true;

            }
        }
        public JsonResult GetUserSelectedRole(string Userid)
        {
            
            DataTable dt = new DataTable();
           dt = _RoleDataAccess.GetSelectedUserRole(Userid);
            //dt.Columns.Add("roleid");

            //DataRow dr = dt.NewRow();
            //dr[0] = "1";
            //dt.Rows.Add(dr);

            //DataRow dr1 = dt.NewRow();
            //dr1[0] = "15";
            //dt.Rows.Add(dr1);

            //dt.AcceptChanges();

            List <Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMenubyRoleid(int RoleId)
        {

            DataTable dt = new DataTable();
            dt = _RoleDataAccess.GetMenubyRoleid(RoleId);
            //dt.Columns.Add("roleid");

            //DataRow dr = dt.NewRow();
            //dr[0] = "1";
            //dt.Rows.Add(dr);

            //DataRow dr1 = dt.NewRow();
            //dr1[0] = "15";
            //dt.Rows.Add(dr1);

            //dt.AcceptChanges();

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public ActionResult NewReportDashboard()
        {
            ViewBag.PageTitle = "Analysis Generator";

            //if (Session["SessionUser"] != null)
            //{
            //    SessionUser SessionUser = (SessionUser)Session["SessionUser"];
            //    ViewBag.UserNum = SessionUser.UserNum;
            //}

            return View();

        }
        public ActionResult DataAnalysis()
        {
            ViewBag.PageTitle = "Data Analysis";

            if (Session["SessionUser"] != null)
            {
                SessionUser SessionUser = (SessionUser)Session["SessionUser"];
                ViewBag.UserNum = SessionUser.UserNum;
            }

            return View();

        }
        public JsonResult UserLogout(int Usernum)
        {
            _userDataAccess.UpdateUserHistory(Usernum);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult AlterDashboard()
        {
            ViewBag.PageTitle = "Administration";
            return View();

        }
        public JsonResult GetTypeCounts(int UserNum)
        {
            DataTable dt = new DataTable();
            //if (Session["SessionUser"] != null)
            //{
            //    SessionUser SessionUser = (SessionUser)Session["SessionUser"];

            //    //list = _reportAccess.GetUserReports(SessionUser.UserNum).ToList();          

            //}
            dt = _reportAccess.GetTypeCounts(UserNum);
            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);
            

            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetWidgetsReport()
        {
            DataTable dt = new DataTable();
            if (Session["SessionUser"] != null)
            {
                SessionUser SessionUser = (SessionUser)Session["SessionUser"];
                dt = _reportAccess.GetWidgetsReports(SessionUser.UserNum);
               // list = _reportAccess.GetUserReports(SessionUser.UserNum).ToList();          

            }
            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);


            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFilterSettingsByUserNum(int usernum)
        {
            DataTable dt = _reportAccess.GetFilterSettingsByUserNum(usernum);
            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);
            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetFilterSettingsByFiltReportid(int FiltReportid)
        {
            DataTable dt = _reportAccess.GetFilterSettingsByFiltReportid(FiltReportid);
            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);
            return Json(jsonlist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUsersReport()
        {
            DataSet ds = new DataSet();

            ds = _reportAccess.GetUsersReport();

            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(ds.Tables[0]);

            List<Dictionary<string, object>> jsonlist1 = _reportAccess.GetTableRows(ds.Tables[1]);

            List<object> jsonlist2 = new List<object>();

            jsonlist2.Add(jsonlist);
            jsonlist2.Add(jsonlist1);

            return Json(jsonlist2, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReportBuilderNew()
        {
            ViewBag.PageTitle = "Report Generator";

            //if (Session["SessionUser"] != null)
            //{
            //    SessionUser SessionUser = (SessionUser)Session["SessionUser"];
            //    ViewBag.UserNum = SessionUser.UserNum;
            //}

            return View();

        }
        public ActionResult ChooseFolder(int ? usernum)
        {
            
            List<TreeViewNode> nodes = new List<TreeViewNode>();
            DataTable dt = new DataTable();
            dt = _userDataAccess.GetUserFolders((int)usernum);

            foreach (DataRow dr in dt.Rows)
            {
                if (dr["rootfolderid"].ToString() == "0")
                {
                    nodes.Add(new TreeViewNode { id = dr["folderid"].ToString(), parent = "#", text =SecureLogic.Decrypt(dr["foldername"].ToString()) });
                }
                    
                else
                {
                    nodes.Add(new TreeViewNode { id = dr["folderid"].ToString(), parent = dr["parentfolderid"].ToString(), text = dr["foldername"].ToString() });
                }

            }

            List<TreeViewNode> nodes1 = new List<TreeViewNode>();
            List<TreeViewNode> nodes2 = new List<TreeViewNode>();

            DataTable dt1 = new DataTable();
            dt1 = _userDataAccess.GetMultiCompanyDataset((int)usernum);
            string TypeVal = "Primary";
            //foreach (DataRow dr in dt.Select("Parentid = 0", "OrderID asc"))
           // ("[student Name] ='" + stringname4 + "'")
            foreach (DataRow dr in dt1.Select("DatasetType = '" + TypeVal + "'"))
            {
              nodes1.Add(new TreeViewNode { id = dr["id"].ToString(), parent = dr["Parent"].ToString(), text = dr["Text"].ToString() });
                
            }
            TypeVal = "Compare";
            int count = 0;
            foreach (DataRow dr in dt1.Select("DatasetType = '" + TypeVal + "'"))
            {
                if(count == 0)
                {
                    nodes2.Add(new TreeViewNode { id = "-1", parent = "#", text = "None" });
                }
                count++;
                nodes2.Add(new TreeViewNode { id = dr["id"].ToString(), parent = dr["Parent"].ToString(), text = dr["Text"].ToString() });

            }
            //Serialize to JSON string.
            ViewBag.Json = (new JavaScriptSerializer()).Serialize(nodes);
            ViewBag.JsonPrimary = (new JavaScriptSerializer()).Serialize(nodes1);
            ViewBag.JsonComparison = (new JavaScriptSerializer()).Serialize(nodes2);
            //return View();
            return PartialView("_ChooseFolder");
        }
        public JsonResult SaveFolderOnDB(string foldername,  string[] parentfolderid,int UserNum)
        {
            int Result = 0;
            //if (Session["SessionUser"] != null)
            //{
            //    SessionUser SessionUser = (SessionUser)Session["SessionUser"];
                Result = _userDataAccess.InsertUserFolder(foldername, UserNum, Convert.ToInt32(parentfolderid[0].ToString()));
           // }           

            return Json(new { result = Result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DataUpload()
        {
            ViewBag.PageTitle = "Data Upload";

            if (Session["SessionUser"] != null)
            {
                SessionUser SessionUser = (SessionUser)Session["SessionUser"];
                ViewBag.UserNum = SessionUser.UserNum;
            }

            return View();

        }
        [HttpPost]
        public ActionResult DataUpload(HttpPostedFileBase file)  
        {

            if (file.ContentLength > 0)
            {
                string _FileName = Path.GetFileName(file.FileName);
                string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
                file.SaveAs(_path);
                string Result = "";

                Result = _reportAccess.BulkDataUpload(_path);

            }
            else
            {
                ViewBag.Message = "Choose File Before Uploading....";
                return View();
            }
               
                ViewBag.Message = "File Uploaded Successfully!!";  
                return View();
           
        }
        public JsonResult IsValidSession(int usernum, int resetsession)
        {
            string val = _userDataAccess.IsValidSession(usernum, resetsession);
            
            object msg = new { Message = val };

            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult InsertChartImageData(string chartname, string imgdata, string storedpath, int usernum, int ChartVersion)
        {
            //chartname = chartname + "_" + Guid.NewGuid().ToString().Substring(0, 6);
            chartname = chartname + "_" + DateTime.Now.ToString("dd_MM_yyyy") + "_" + ChartVersion;
           // string imgdata1 = string.Join(",", imgdata);
            int Result = _reportAccess.InsertChartImageData(chartname,imgdata,storedpath,usernum);

            return Json(Result.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetChartversionname(int usernum, string chartname)
        {
            string Result = _reportAccess.GetChartversionname(usernum,chartname);

            return Json(Result.ToString(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateChartImageData(int ReportSaveGetId, string imgdata, string storedpath, int usernum)
        {
           // chartname = chartname + "_" + Guid.NewGuid().ToString().Substring(0, 6);
            // string imgdata1 = string.Join(",", imgdata);
            int Result = _reportAccess.UpdateChartImageData(ReportSaveGetId, imgdata, storedpath, usernum);

            return Json(Result.ToString(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DataAnalysisNew()
        {
            ViewBag.PageTitle = "Data Analysis";
            return View();
        }
        public JsonResult PopulateSavedFolders(int? usernum)
        {
            //int usernum = 0;
            //if (Session["SessionUser"] != null)
            //{
            //    SessionUser SessionUser = (SessionUser)Session["SessionUser"];
            //    usernum = SessionUser.UserNum;
            //}
            List<TreeViewNode> nodes = new List<TreeViewNode>();

            DataTable dt = new DataTable();
            dt = _userDataAccess.GetUserFolders((int)usernum);

            foreach (DataRow dr in dt.Rows)
            {
                if (dr["rootfolderid"].ToString() == "0")
                {
                    nodes.Add(new TreeViewNode { id = dr["folderid"].ToString(), parent = "#", text = SecureLogic.Decrypt(dr["foldername"].ToString()) });
                }
                else
                {
                    nodes.Add(new TreeViewNode { id = dr["folderid"].ToString(), parent = dr["parentfolderid"].ToString(), text = dr["foldername"].ToString() });
                }

            }
            //Serialize to JSON string.
            var jsonobj = (new JavaScriptSerializer()).Serialize(nodes);

            object msg = new { Message = jsonobj };

            return Json(msg, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetSavedcharts(int? usernum, string storedpath)
        {
           
            DataTable dt = new DataTable();
            dt = _reportAccess.GetSavedcharts((int)usernum, storedpath);

           
            List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

            //Serialize to JSON string.
            var jsonobj = (new JavaScriptSerializer()).Serialize(jsonlist);

            object msg = new { Message = jsonobj };

            return Json(msg, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ReportBuilder()
        {
            ViewBag.PageTitle = "Report Generator";


            return View();

        }
    }
}
