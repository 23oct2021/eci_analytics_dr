﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ECIAPP.Models;
using ECIModel;
using ECIDataAccess;
using ECIBusinessLogic;
using System.Web.SessionState;
using ECIAPP.Helpers;
using System.Data;
using System.Web.Script.Serialization;

namespace ECIAPP.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly IUserDataAccess _userDataAccess;

        public HomeController()
        {
            this._userDataAccess = new UserDataAccess();
        }

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult TestOtp(int usernum, string enotp)
        {
            LoginVM login = new LoginVM();
            login.Usernum = usernum;
            login.otp = enotp;

            return View(login);
        }
        [HttpPost]
        public ActionResult TestOtp(LoginVM loginVM)
        {
            string otp = SecureLogic.Decrypt(loginVM.otp);

            if (loginVM.Password != otp)
            {
                ViewBag.Message = "Wrong OTP";
                return View(loginVM);
            }
            else
            {
                return RedirectToAction("AlterDashboard", "SuperAdmin");
            }
        }

        public ActionResult CheckOTP(int usernum, string enotp)
        {
            LoginVM login = new LoginVM();
            login.Usernum = usernum;
            login.otp = enotp;

            return View(login);
        }
        [HttpPost]
        public ActionResult CheckOTP(LoginVM loginVM )
        {
            string otp = SecureLogic.Decrypt(loginVM.otp);

            if(loginVM.Password != otp)
            {
                ViewBag.Message = "Wrong OTP";
                return View(loginVM);
            }
            else
            {
                UserModel userModel = new UserModel();
                userModel = _userDataAccess.ValidateUser("", "", loginVM.Usernum);

                //Create Session
                SessionUser sessionUser = new SessionUser();
                sessionUser.UserNum = userModel.UserNum;
                sessionUser.FirstName = userModel.FirstName;
                sessionUser.LastName = userModel.LastName;
                sessionUser.RoleID = 0;
                sessionUser.UserType = userModel.UserType;
                sessionUser.Accountid = userModel.Accountid;
                sessionUser.IsAdmin = false;
                HSSession.set("SessionUser", sessionUser);

                Session["MenuMaster"] = _userDataAccess.GetMenusByUser(userModel.UserNum);

                //Call UserLogin History
                userModel.SessionId = Session.SessionID;

                _userDataAccess.InsertUserHistory(userModel);

                return RedirectToAction("AlterDashboard", "SuperAdmin");
            }
        }

        public ActionResult Login()
        {          

            ViewBag.Message = "";
            LoginVM loginvm = new LoginVM();
            //loginvm.UserName = "Deepa";
            //loginvm.Usernum = 1;
            return View(loginvm);
        }

        [HttpPost]
        public ActionResult Login(LoginVM loginvm)
        {
            //Random r1 = new Random();
            //var otp1 = r1.Next(0, 999999).ToString("D6");
            SessionUser sessionUser = null;

            if (loginvm.UserName == "SuperAdmin")
            {
                return RedirectToAction("NewReportDashboard", "SuperAdmin");
            }

            UserModel userModel = new UserModel();
            userModel = _userDataAccess.ValidateUser(loginvm.UserName, loginvm.Password, 0);

            if (userModel != null)
            {
                if(userModel.UserNum == 0)
                {
                    ViewBag.Message = "Invalid user / password";
                    return View();
                }
                else if (userModel.UserNum == -1)
                {
                    ViewBag.Message = "Your Account Has Been Expired.Please Contact Admin";
                    return View();
                }
                else
                {
                    string ip = loginvm.IpAddress;

                    if( string.IsNullOrEmpty(userModel.IpAddress) == false )
                    {
                        if (userModel.IpAddress.ToUpper() != ip.ToUpper())
                        {
                            userModel.IpAddress = ip;

                            //string otp = Guid.NewGuid().ToString().Substring(0, 6);
                            Random r = new Random();
                            var otp = 313562;// r.Next(0, 999999).ToString("D6");

                            userModel.otp = otp.ToString();

                            userModel.sysconfig = loginvm.sysconfig;

                            _userDataAccess.UpdateIPAddress(userModel);

                            string enotp = SecureLogic.Encrypt(otp.ToString());

                            return RedirectToAction("CheckOTP", new { usernum = userModel.UserNum, Enotp = enotp });
                        }
                        else
                        {
                            //Create Session
                            sessionUser = new SessionUser();
                            sessionUser.UserNum = userModel.UserNum;
                            sessionUser.FirstName = userModel.FirstName;
                            sessionUser.LastName = userModel.LastName;
                            sessionUser.RoleID = 0;
                            sessionUser.UserType = userModel.UserType;
                            sessionUser.Accountid = userModel.Accountid;
                            sessionUser.IsAdmin = false;
                            HSSession.set("SessionUser", sessionUser);

                            Session["MenuMaster"] = _userDataAccess.GetMenusByUser(userModel.UserNum);

                            //Call UserLogin History
                            userModel.SessionId = Session.SessionID;

                            _userDataAccess.InsertUserHistory(userModel);

                            return RedirectToAction("AlterDashboard", "SuperAdmin");
                        }
                    }
                    else
                    {

                        userModel.IpAddress = ip;

                        //string otp = Guid.NewGuid().ToString().Substring(0, 6);
                        Random r = new Random();
                        var otp = r.Next(0, 999999).ToString("D6");

                        userModel.otp = otp.ToString();

                        userModel.sysconfig = loginvm.sysconfig;

                        _userDataAccess.UpdateIPAddress(userModel);

                        string enotp = SecureLogic.Encrypt(otp.ToString());

                        return RedirectToAction("CheckOTP", new { usernum = userModel.UserNum, Enotp = enotp });
                    }
                }
            }

            
            
//            string ip1 = Request.ServerVariables["remote_host"];

            return View();
        }

        public ActionResult ResetPass(int UserNum,string status)
        {
            ViewBag.Message = "";
            PasswordVM passwordVM = new PasswordVM();
            passwordVM.Usernum = UserNum;
            passwordVM.Status = status;
            //loginvm.UserName = "Deepa";
            //loginvm.Usernum = 1;
            return View(passwordVM);
        }

        [HttpPost]
        public ActionResult ResetPass(PasswordVM passwordVM)
        {          

            if (ModelState.IsValid)
            {
                UserModel userModel = new UserModel();
                userModel.UserNum = passwordVM.Usernum;
                userModel.Password = passwordVM.Password;
                userModel.IpAddress = passwordVM.IpAddress;
                _userDataAccess.UpdatePwd(userModel);
                if (passwordVM.Status  == "Yes")
                {
                    _userDataAccess.UpdateIPAddress(userModel);
                }
                
                return RedirectToAction("Login");
               
            }
            return View();
        }

        public ActionResult ForgetPass()
        {
            ForgetVM forgetVM = new ForgetVM();
            return View(forgetVM);
        }
        [HttpPost]
        public ActionResult ForgetPass(ForgetVM forgetVM)
        {

            if (ModelState.IsValid)
            {
                int usernum = _userDataAccess.GetUserByEmailId(forgetVM.EmailId);

                var urlBuilder =
 new System.UriBuilder(Request.Url.AbsoluteUri)
 {
     Path = Url.Action("ResetPass", "Home"),
     Query = "UserNum="+ usernum+"&status=No",
 };

                Uri uri = urlBuilder.Uri;
                string url = urlBuilder.ToString();

                string htmlstring = @"Login Id : " + forgetVM.EmailId + "<br></br> URL :<a href='" + url + "'>Click Me</a> <br></br> Regards,  <br></br>ECI Team";

                int ret = EmailLogic.SendEmail("ECI Reset Password", forgetVM.EmailId, "<html><body style='font-family:Calibri'>" + htmlstring + "</body></html>");

                ViewBag.Message = "Reset link has been sent to your email-id";

            }
            return View();
        }

        public JsonResult ValidateSession(string emailid, string Pass, string IpAddress)
        {
            string pass_de = System.Uri.UnescapeDataString(Pass);
            int cnt = _userDataAccess.IsSameUserLoggedIn(emailid, pass_de, IpAddress);

            var ok = "false";

            if (cnt > 0)
            {
                ok = "true";
            }
            else if (cnt == -1)
            {
                ok = "Invalid";
            }
            
            object msg = new { Ok = ok };

            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            if (Session["SessionUser"] != null)
            {
                SessionUser SessionUser = (SessionUser)Session["SessionUser"];
                _userDataAccess.UpdateUserHistory(SessionUser.UserNum);
            }
            return RedirectToAction("Login");
        }

        public ActionResult SessionLogout()
        {
            Session.Abandon();
            return View();
        }
        public ActionResult NextTab()
        {
            return View();
        }

        public JsonResult ValidateUser(string UserName, string Password, string IpAddress, string sysconfig)
        {
            //SessionUser sessionUser = null;

            string message ="";

            object msg = new { Message = message, UserNum = 0, Otp = "" };

            try
            {
                UserModel userModel = new UserModel();
                userModel = _userDataAccess.ValidateUser(UserName, Password, 0);

                if (userModel != null)
                {
                    if (userModel.UserNum == 0)
                    {
                        message = "Invalid user / password";

                         msg = new { Message = message, UserNum = 0, Otp ="" };
                    }
                    else if (userModel.UserNum == -1)
                    {
                        message = "Your Account Has Been Expired.Please Contact Admin";

                        msg = new { Message = message, UserNum = -1, Otp = "" };
                    }
                    else
                    {
                        string ip = IpAddress;

                        if (string.IsNullOrEmpty(userModel.IpAddress) == false)
                        {
                            if (userModel.IpAddress.ToUpper() != ip.ToUpper())
                            {
                                userModel.IpAddress = ip;

                                //string otp = Guid.NewGuid().ToString().Substring(0, 6);
                                Random r = new Random();
                                var otp = r.Next(0, 999999).ToString("D6");

                                userModel.otp = otp.ToString();

                                userModel.sysconfig = sysconfig;

                                _userDataAccess.UpdateIPAddress(userModel);

                                string enotp = SecureLogic.Encrypt(otp.ToString());

                                message = "CheckOTP";

                                msg = new { Message = message, UserNum = userModel.UserNum, Otp = enotp };
                            }
                            else
                            {
                                //Create Session
                                //sessionUser = new SessionUser();
                                //sessionUser.UserNum = userModel.UserNum;
                                //sessionUser.FirstName = userModel.FirstName;
                                //sessionUser.LastName = userModel.LastName;
                                //sessionUser.RoleID = 0;
                                //sessionUser.UserType = userModel.UserType;
                                //sessionUser.Accountid = userModel.Accountid;
                                //sessionUser.IsAdmin = false;
                                //HSSession.set("SessionUser", sessionUser);

                                DataTable dt = _userDataAccess.GetMenusByUser(userModel.UserNum);

                                ReportAccess _reportAccess = new ReportAccess();

                                List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

                                var jobj = (new JavaScriptSerializer()).Serialize(jsonlist);

                                //Call UserLogin History
                                userModel.SessionId = Guid.NewGuid().ToString();

                                _userDataAccess.InsertUserHistory(userModel);                                

                                msg = new { Message = "Success", UserNum = userModel.UserNum, Otp = "", FName = userModel.FirstName, LName = userModel.LastName, AccountId = userModel.Accountid, MenuList = jobj,UserType = userModel.UserTypeVal };
                            }
                        }
                        else
                        {

                            userModel.IpAddress = ip;

                            //string otp = Guid.NewGuid().ToString().Substring(0, 6);
                            Random r = new Random();
                            var otp = r.Next(0, 999999).ToString("D6");

                            userModel.otp = otp.ToString();

                            userModel.sysconfig = sysconfig;

                            _userDataAccess.UpdateIPAddress(userModel);

                            string enotp = SecureLogic.Encrypt(otp.ToString());

                            message = "CheckOTP";

                            msg = new { Message = message, UserNum = userModel.UserNum, Otp = enotp };
                        }
                    }
                }

                

                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                msg = new { Message = "Error", UserNum = 0 };

                return Json(msg, JsonRequestBehavior.AllowGet);
            }

            return Json("", JsonRequestBehavior.AllowGet);

        }

        public JsonResult CheckOTPJS(int Usernum, string enotp, string typedotp)
        {
            string message = "";

            object msg = new { Message = message, UserNum = 0, Otp = "" };

            string otp = SecureLogic.Decrypt(enotp);

            if (typedotp != otp)
            {
                message = "Wrong OTP";

                msg = new { Message = message, UserNum = 0, Otp = "" };
            }
            else
            {
                try
                {
                    UserModel userModel = new UserModel();
                    userModel = _userDataAccess.ValidateUser("", "", Usernum);

                    DataTable dt = _userDataAccess.GetMenusByUser(userModel.UserNum);

                    ReportAccess _reportAccess = new ReportAccess();

                    List<Dictionary<string, object>> jsonlist = _reportAccess.GetTableRows(dt);

                    var jobj = (new JavaScriptSerializer()).Serialize(jsonlist);

                    //Call UserLogin History
                    userModel.SessionId = Guid.NewGuid().ToString();

                    _userDataAccess.InsertUserHistory(userModel);

                    msg = new { Message = "Success", UserNum = userModel.UserNum, Otp = "", FName = userModel.FirstName, LName = userModel.LastName, AccountId = userModel.Accountid, MenuList = jobj, UserType = userModel.UserTypeVal};
                }
                catch(Exception ex)
                {
                    message = "Error";

                    msg = new { Message = message, UserNum = 0, Otp = "" };

                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
    }
}
