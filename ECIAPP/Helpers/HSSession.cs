﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ECIModel;

namespace ECIAPP.Helpers
{
    //Gets Data from Session
    public static class HSSession
    {

        public static Object getSession()
        {
            return HttpContext.Current.Session;
        }
        public static void clearSession()
        {
            HttpContext ctx = HttpContext.Current;
            if (ctx != null)
            {
                ctx.Session.Clear();
            }
        }
        public static SessionUser getSessionUser()
        {
            SessionUser SessionUser = null;
            if (HttpContext.Current.Session != null)
            {
                SessionUser = (SessionUser)get("SessionUser");
            }
            return SessionUser;
        }

        public static void set(string key, Object value)
        {
            if (HttpContext.Current.Session != null)
                HttpContext.Current.Session[key] = value;
        }
        public static String getUserType()
        {
            String userType = "";
            SessionUser sessionUser = getSessionUser();
            if (sessionUser != null)
            {
                /*CompanyUserVM companyUser = sessionUser.CompanyUser;
                if (companyUser != null)
                {
                    userType = companyUser.UserType;
                }*/
            }
            return userType;
        }

        public static Object get(string key)
        {
            //Logger.log("key", key.ToString(), "hsessionuser", "test");  
            HttpContext ctx = HttpContext.Current;
            if (ctx.Session != null)
            {
                /* // check if a new session id was generated
                 if (ctx.Session.IsNewSession)
                 {

                     // If it says it is a new session, but an existing cookie exists, then it must
                     // have timed out
                     string sessionCookie = ctx.Request.Headers["Cookie"];
                     if ((null != sessionCookie) && (sessionCookie.IndexOf("ASP.NET_SessionId") >= 0))
                     {
                         ctx.Session.Clear();

                         //ctx.Response.Redirect("/Account/TimeoutRedirect"); //Disabled By Deepa Marimuthu
                         return null;
                     }
                 }
                 */

                return HttpContext.Current.Session[key];
            }
            else
            {
                //Logger.log("else null", key.ToString(), "hsessionuser", "test");  
                return null;
            }
        }

    }
}