﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECIAPP.Models
{
    public class LoginVM
    {
        public int Usernum { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string IpAddress { get; set; }
        public string otp { get; set; }
        public string sysconfig { get; set; }
    }
    public class PasswordVM
    {
        public int Usernum { get; set; }
        [Required]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$", ErrorMessage = "Password must be at least 8 characters, no more than 32 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit.")]
        public string Password { get; set; }
        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public string IpAddress { get; set; }
        public string Status { get; set; }
    }
    public class ForgetVM
    {
        [Required]
        public string EmailId { get; set; }
    }
}