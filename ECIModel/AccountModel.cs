﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECIModel
{
    public class AccountModel
    {
        public int id { get; set; }
        [Required(ErrorMessage = "* Information Required")] 
        public string companyName { get; set; }
        public DateTime createdon { get; set; }
        public int createdby { get; set; }
        public DateTime modifiedon { get; set; }
        public int modifiedby { get; set; }
        public string AccOwner { get; set; }
        public string Industry { get; set; }
        public int Employees { get; set; }
        public float Annual_Revenue { get; set; }
        public string Telephone { get; set; }
        public string Mailing_Street { get; set; }
        public string Mailing_City { get; set; }
        public string Mailing_State { get; set; }
        public string Mailing_Postal_Code { get; set; }
        public string Mailing_Country { get; set; }
        public string Website { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "* Information Required")] 
        public string AliasName { get; set; }
        
    }
}
