﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ECIModel
{
    public class ProfileModel
    {
        public string id { get; set; }
        [Required(ErrorMessage = "* Information Required")] 
        public string firstname { get; set; }
        [Required(ErrorMessage = "* Information Required")] 
        public string lastname { get; set; }
        [Required(ErrorMessage = "* Information Required")] 
        public string email { get; set; }
        public string createdon { get; set; }
        public int createdby { get; set; }
        public string modifiedon { get; set; }
        public string modifiedby { get; set; }
        public string Middle_Name { get; set; }
        public string Tittle { get; set; }
        public string Division { get; set; }
        public string Brithday { get; set; }
        public string Mobile { get; set; }
        public string Contact_Owner { get; set; }
        public string Telephone { get; set; }
        public string Mailing_Street { get; set; }
        public string Mailing_City { get; set; }
        public string Mailing_State { get; set; }
        public string Mailing_Postal_Code { get; set; }
        public string Mailing_Country { get; set; }
        public string Website { get; set; }
        public string Description { get; set; }
        //[Required(ErrorMessage = "* Information Required")] 
        public string ExpiryDate { get; set; }

        //[Display(Name = "Country")]
        [Required(ErrorMessage = "* Information Required")] 
        public int companyid { get; set; }

        public SelectList CompanyList { get; set; }

        [Required(ErrorMessage = "* Information Required")] 
        public int roleid { get; set; }    
        
        public string AppURL { get; set; }

        [Required(ErrorMessage = "* Information Required")]
       // public int PrimaryID { get; set; }
        public List<string> PrimaryID { get; set; }

        [Required(ErrorMessage = "* Information Required")]
       // public int ComparisionID { get; set; }
        public List<string> ComparisionID { get; set; }

        public int[]  primeid { get; set; }
        public int[] compid { get; set; }
        public string LoggedInUserType { get; set; }
        public string FromScreen { get; set; }
    }
}
