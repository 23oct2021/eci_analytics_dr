﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ECIModel
{
    [Serializable]
    public class SessionUser
    {
        ///User Num - Identity field
        public int UserNum { get; set; }

        ///First Name of the User
        public string FirstName { get; set; }

        ///Last Name of the User
        public string LastName { get; set; }

        ///Email of User
        public string Emailid { get; set; }

        ///Password of User
        public string Password { get; set; }

        ///User Status
        ///0 - Added
        ///1 - Verified
        ///2 - Active
        ///3 - Deactive
        public int UserStatus { get; set; }

        //UserType of User
        public string UserTypeVal { get; set; }

        //Role of User
        public int RoleID { get; set; }

        public bool IsAdmin { get; set; }

        public int UserType { get; set; }

        public int Accountid { get; set; }
    }

    public class UserModel
    {
        ///User Num - Identity field        
        public int UserNum { get; set; }

        ///First Name of the User
        [Required]
        public string FirstName { get; set; }

        ///Last Name of the User
        [Required]
        public string LastName { get; set; }

        ///Email of User
        [Required]
        [EmailAddress]
        public string Emailid { get; set; }

        ///Password of User        
        [DataType(DataType.Password)]
        public string Password { get; set; }

        ///User Status
        ///0 - Added
        ///1 - Verified
        ///2 - Active
        ///3 - Deactive
        public int UserStatus { get; set; }

        ///User Type
        ///1 - Super Admin
        ///2 - ECIUser
        ///3 - ECIClientUser
        public int UserType { get; set; }

        /// <summary>
        /// User Creation
        /// </summary>
        public DateTime CreatedOn { get; set; }

        ///User Update
        public DateTime UpdatedOn { get; set; }

        public string UserTypeVal { get; set; }

        public string UserStatusVal { get; set; }

        public string IpAddress { get; set; }

        public string otp { get; set; }

        public string sysconfig { get; set; }

        public int Accountid { get; set; }
        public string Accountname { get; set; }

        public string SessionId { get; set; }
    }
}
