﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ECIModel
{
    public class TreeViewNode
    {
        public string id { get; set; }
        public string parent { get; set; }
        public string text { get; set; }
    }

   
    public class RoleModel
    {
        public int RoleID { get; set; }
        [Required(ErrorMessage = "* Information Required")] 
        public string RoleName { get; set; }
        public string Description { get; set; }
    }
    public class LinkUserRoleModel
    {
        public int UserNum { get; set; }

        public int RoleId { get; set; }

        public SelectList UserList { get; set; }
    }
}
