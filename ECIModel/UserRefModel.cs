﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECIModel
{
    public class UserRefModel
    {
        public string xmlparamval { get; set; }
        public string xmlmatricsval { get; set; }
        public int Usernum { get; set; }
        [Required]
        public string ReportName { get; set; }
        public int CreatedBy { get; set; }
        public int Reportid { get; set; }
        public string fulldescription { get; set; }
        public string canvasdata { get; set; }
        public string chartdata { get; set; }
    }
}
